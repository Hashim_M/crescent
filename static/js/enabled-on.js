$(function(){

  // we shall use this function to disable/enable the input based
  // on the current value (v) of the target_field (t)
  var evaluateTarget = function(t, v, input, related, initial){
    if(initial && input.attr('data-display-checked') == 'true') {
      return;
    }
    input.attr('disabled', 'disabled');

    if(t.attr('type') == 'checkbox' && related.is(':checked')){
      input.removeAttr('disabled');
    }
    else if(t.attr('type') == 'radio' && t.is(':checked') && t.val() == v){
      input.removeAttr('disabled');
      if(initial){
        input.attr('data-display-checked', 'true');
      }
    }
    else if(t.val() == v){
      input.removeAttr('disabled');
    }
  }

  var devaluateTarget = function(t, v, input, related, initial){
    if(initial && input.attr('data-display-checked') == 'true') {
      return;
    }
    input.removeAttr('disabled');

    if(t.attr('type') == 'checkbox' && related.is(':checked')){
      input.attr('disabled', 'disabled');
    }
    else if(t.attr('type') == 'radio' && t.is(':checked') && t.val() == v){
      input.attr('disabled', 'disabled');
      if(initial){
        input.attr('data-display-checked', 'true');
      }
    }
    else if(t.val() == v){
      input.attr('disabled', 'disabled');
    }
  }

  $('.enabled-on').each(function(){
    var input = $(this),
      target_fields = input.attr('data-enabled-on').split(','),
      target_value = input.attr('data-enabled-value'),
      targets = "";

    for(var i = 0; i < target_fields.length; i++){
      targets += "input[name=" + target_fields[i] + "],select[name=" +target_fields[i] + "],";
    }
    targets = $(targets.slice(0, -1));

    // let's do an initial check to see if we should disable the input
    targets.each(function(){
      var target = $(this);

      evaluateTarget(target, target_value, input, targets, true);

      // add an event handler when the value changes
      target.change(function(){
        evaluateTarget($(this), target_value, input, targets, false);
      });
    });
  });

  // this will be enabled when the target value **isn't** enabled
  $('.enabled-off').each(function(){
    var input = $(this),
      target_fields = input.attr('data-enabled-off').split(','),
      target_value = input.attr('data-enabled-value'),
      targets = "";;

    for(var i = 0; i < target_fields.length; i++){
      targets += "input[name=" + target_fields[i] + "],select[name=" +target_fields[i] + "],";
    }
    targets = $(targets.slice(0, -1));

    // let's do an initial check to see if we should disable the input
    targets.each(function(){
      var target = $(this);

      devaluateTarget(target, target_value, input, targets, true);

      // add an event handler when the value changes
      target.change(function(){
        devaluateTarget($(this), target_value, input, targets, false);
      });
    });
  });
});
