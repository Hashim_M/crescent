$(function(){

  // we shall use this function to hide/show the div based
  // on the current value (v) of the target_field (t)
  var evaluateTarget = function(t, v, div){
    if(div.attr('data-display-checked') == 'true') {
      return;
    }

    if(t.attr('type') == 'checkbox' && t.is(':checked')){
      div.show();
    }
    else if(t.attr('type') == 'radio' && t.is(':checked') && t.val() == v){
      div.show();
      div.attr('data-display-checked', 'true');
    }
    else if(t.val() == v){
      div.show();
    }
    else {
      div.hide();
    }
  }

  $('.display-on').each(function(){
    var div = $(this),
      target_field = div.attr('data-display-on'),
      target_value = div.attr('data-display-value');

    // let's do an initial check to see if we should disable the input
    $('input[name=' + target_field + '],select[name=' + target_field + ']').each(function(){
      var target = $(this);

      evaluateTarget(target, target_value, div);

      // add an event handler when the value changes
      target.change(function(){
        evaluateTarget($(this), target_value, div);
      });
    });
  });
});
