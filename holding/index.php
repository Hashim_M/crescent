<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Queens Cresent</title>
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<meta name="description" content="Queens Crescent Surgery is moving on a temporary basis to the James Wigg Practice at Kentish Town Health Centre as from Monday 12 October 2009 for a 6 month period. This move is while the redevelopment of the Queens Crescent site takes place" />
		<meta name="keywords" content="Queens Crescent, Surgery, James Wigg, GP, Doctor" />
		<link rel="canonical" href="http://www.queenscrescent.co.uk" />
		<link rel="stylesheet" href="/static/css/style.css" type="text/css" media="screen" />
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<h1 id="queenscresent-logo">Queens Crescent Practise</h1>
				</div>
				<div id="contact-panel">
					<p>
						<strong>Contact</strong><br />
						76 Queens Crescent<br />London NW5 4EB
					</p>
					<p>
						<strong>Telephone:</strong> 020 7485 6104<br />
						<strong>Email:</strong>
						<a href="mailto:queenscrescentsurgery@nhs.net">queenscrescentsurgery@nhs.net</a>
					</p>
				</div>
			</div>
			<div id="content">
				<h1>New Website <span class="light-green">coming soon</span></h1>
				<h3>
					The Queens Crescent Surgery moved back to our newly built premises
					on 7th December 2010
				</h3>
				<p class="content-text">
					Your Doctors Surgery has moved to larger, more modern premises. The
					Surgery will provide additional services to patients.<br />
					With the new building, the Practice has a new name...
					The Queens Crescent Practice
				</p>
				<!--
				<p class="content-text">
					The new premises are located in the same place as before.
				</p>
				-->
				<h2>New registrations are welcome</h2>
			</div>
		</div>
	</body>
</html>
