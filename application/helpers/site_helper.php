<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function display_sidenav($sections, $admin = false)
{
	$CI =& get_instance(); 

	// admin control panel
	if ($admin)
	{
		$prefix = '/admin';
		$level1_href = ($CI->uri->segment(2) ? $CI->uri->segment(2) : '');
		$level2_href = ($CI->uri->segment(3) ? $CI->uri->segment(3) : '');
		$level3_href = ($CI->uri->segment(4) ? $CI->uri->segment(4) : '');
		$level4_href = ($CI->uri->segment(5) ? $CI->uri->segment(5) : '');
	}
	else
	{
		// public site
		$prefix = '';
		$level1_href = ($CI->uri->segment(1) ? $CI->uri->segment(1) : '');
		$level2_href = ($CI->uri->segment(2) ? $CI->uri->segment(2) : '');
		$level3_href = ($CI->uri->segment(3) ? $CI->uri->segment(3) : '');
		$level4_href = ($CI->uri->segment(4) ? $CI->uri->segment(4) : '');
	}

	foreach($sections as $section) : ?>
		<li><a href="<?=$prefix?>/<?=$level1_href?>/<?=$section['href']?>"<?php if ($section['href'] == $level2_href) : ?> class="active"<?php endif; ?>><?=$section['name']?></a>
		<?php if (isset($section['level3']) && !empty($section['level3']) && $section['href'] == $level2_href) : ?>
		<ul>
			<?php foreach($section['level3'] as $level3) : ?>
			<li><a href="<?=$prefix?>/<?=$level1_href?>/<?=$section['href']?>/<?=$level3['href']?>"<?php if ($level3['href'] == $level3_href) : ?> class="active"<?php endif; ?>><?=$level3['name']?></a>
			<?php if (isset( $level3['level4']) && !empty($level3['level4']) && $level3['href'] == $level3_href) : ?>
				<ul>
				<?php foreach($level3['level4'] as $level4) : ?>
					<li><a href="<?=$prefix?>/<?=$level1_href?>/<?=$section['href']?>/<?=$level3['href']?>/<?=$level4['href']?>"<?php if ($level4['href'] == $level4_href) : ?> class="active"<?php endif; ?>><?=$level4['name']?></a></li>
				<?php endforeach; ?>	
				</ul>
			<?php endif; ?></li>
			<?php endforeach; ?>	
		</ul>
		<?php endif; ?></li>
	<?php
	endforeach;
}