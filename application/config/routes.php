<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved 
| routes must come before any wildcard or regular expression routes.
|
*/

$route['default_controller'] = "home";
$route['scaffolding_trigger'] = "";

$route['/'] = "home";

$route['news/:any/:any/:any/:any'] = 'news/item';
$route['news/:any/:any'] = 'news/month';
$route['news/:any'] = 'news';

$route['admin'] = 'admin/home';

$route['admin/ppg/links/create'] = 'admin/ppg/links_create';
$route['admin/ppg/links/update'] = 'admin/ppg/links_update';
$route['admin/ppg/links/update/:any'] = 'admin/ppg/links_update';
$route['admin/ppg/links/delete/:any'] = 'admin/ppg/links_delete';

$route['admin/ppg/minutes'] = 'admin/ppg/currently_doing';
$route['admin/ppg/minutes/create'] = 'admin/ppg/currently_doing_create';
$route['admin/ppg/minutes/update'] = 'admin/ppg/currently_doing_update';
$route['admin/ppg/minutes/update/:any'] = 'admin/ppg/currently_doing_update';
$route['admin/ppg/minutes/delete/:any'] = 'admin/ppg/currently_doing_delete';

$route['ppg/currently_doing/:any/:any/:any/:any'] = 'ppg/currently_doing_item';


$route['patient_services/register'] = 'patient_services/register';
$route['patient_services/:any'] = 'patient_services';

$route['staff/:any'] = 'staff';

$route['practice_info/:any']	= 'practice_info';
$route['contact/:any']			= 'contact';
$route['ppg/:any']			= 'ppg';

/* End of file routes.php */
/* Location: ./system/application/config/routes.php */