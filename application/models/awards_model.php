<?php

class Awards_model extends Model{

	function __construct()
	{
		parent::__construct();
	}
	
	function get_awards_content(){
		return $this->db->get('kthc_awards')->row();
	}
	
	function update($body, $sidebar){
	
		$data = array(
			'body' 			=> ascii_to_entities($body),
			'sidebar' 		=> ascii_to_entities($sidebar),
		);
		
		$this->db->update('kthc_awards', $data);
		
	}
	
}