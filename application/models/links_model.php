<?php

class Links_model extends Model {

	function __constructor(){
		parent::Model();
	}
	
	function get_links(){
		return $this->db->get('kthc_links');
	}
	
	function create($title, $description, $hyperlink, $thumbnail){
		
		$data = array(
			'title' 		=> ascii_to_entities($title),
			'description' 	=> ascii_to_entities($description),
			'hyperlink'     => ascii_to_entities(prep_url($hyperlink)),
			'thumbnail'		=> $thumbnail,			
		);
		
		$this->db->insert('kthc_links', $data);
	
	}
	
	function update($id, $title, $description, $hyperlink, $thumbnail) {
		
		$data = array(
			'title' 		=> ascii_to_entities($title),
			'description' 	=> ascii_to_entities($description),
			'hyperlink'     => ascii_to_entities(prep_url($hyperlink)),
			'thumbnail'		=> $thumbnail,
		);

		$this->db->where('id', $id);
		$this->db->update('kthc_links', $data);
		
	}
	
	function get_item_by_id($id){
		
		$this->db->where('id', $id);
		$result = $this->db->get('kthc_links')->row();
		
		if (empty($result)){
			show_404();
		}
		
		return $result;
	
	}
	
	function delete($id){
		
		$this->db->where('id', $id);
		$this->db->delete('kthc_links');
		
	}
	
}