<?php

class News_model extends Model{

	function __constructor(){
		parent::Model();
	}
	
	function get_item_by_id($id){
		
		$this->db->where('id', $id);
		$result = $this->db->get('kthc_news')->row();
		
		if (empty($result)){
			show_404();
		}
		
		return $result;
		
	}
	
	function get_latest_items($limit){
		
		$this->db->limit($limit);
		$this->db->where('post_status', 'publish');
		$this->db->select("*, DATE_FORMAT(published,'%Y/%m/%d') AS url_date", FALSE);
		$this->db->order_by('position');
		
		return $this->db->get('kthc_news');
	}
	
	function get_item_by_url_title($news_title_url, $isodate){
		
		$this->db->where('post_status', 'publish');
		$this->db->like('published', $isodate, 'after');
		$this->db->where('url_title', $news_title_url);
		$this->db->limit(1);
		
		$result = $this->db->get('kthc_news')->row();
		
		if (empty($result)){
			show_404();
			exit;
		}
		
		return $result;
	}
	
	function get_news_by_month($news_archive_month){
		
		$this->db->like('published', $news_archive_month, 'after');
		$this->db->where('post_status', 'publish');
		$this->db->select("*, MONTHNAME(published) AS monthname", FALSE);
		$this->db->order_by('published', 'desc'); 
		$result =  $this->db->get('kthc_news');
		
		if (empty($result) || ($result->num_rows() == 0)){
			show_404();
			exit;
		}
		
		return $result;
	}
	
	function get_news_items(){
		$this->db->order_by("position"); 
		return $this->db->get('kthc_news');

	}

	function get_draft_news_items(){
		$this->db->where('post_status', 'draft');
		$this->db->order_by('position');
		return $this->db->get('kthc_news');
	}
	
	function get_publish_news_items(){
		$this->db->where('post_status', 'publish');
		$this->db->order_by('position');
		return $this->db->get('kthc_news');
	}
	
	function get_pending_news_items(){
		$this->db->where('post_status', 'pending');
		$this->db->order_by('position');
		return $this->db->get('kthc_news');
	}
	
	function create($title, $url_title, $summary, $body, $post_status){
		
		if (empty($url_title)){
			$url_title 		=  $title;
		}
		
		if($post_status == 'publish'){
			$published = date('Y-m-d');
		}else{
			$published = '0000-00-00';
		}
		
		$data = array(
			'title' 		=> ascii_to_entities($title),
			'url_title' 	=> strtolower(url_title($url_title, 'underscore')),
			'summary' 		=> ascii_to_entities($summary),
			'body' 			=> ascii_to_entities($body),
			'published'     => $published,
			'created' 		=> date('Y-m-d H:i:s'),
			'modified' 		=> date('0000-00-00 00:00:00'),
			'post_status'   => $post_status
		);
		
		$this->db->insert('kthc_news', $data);
			
	}
	
	function update($id, $title, $url_title, $summary, $body, $post_status){
		
		if (empty($url_title))
			$url_title 		=  $title;
			
		$data = array(
			'title' 			=> ascii_to_entities($title),
			'url_title' 		=> strtolower(url_title($url_title, 'underscore')),
			'body' 				=> ascii_to_entities($body),
			'summary' 			=> ascii_to_entities($summary),
			'modified' 			=> date('Y-m-d H:i:s'),
			'post_status' 		=> $post_status
		);
		
		$this->db->where('id', $id);
		$this->db->update('kthc_news', $data);
		
	}
	
	function delete($id){
		
		$this->db->where('id', $id);
		$this->db->delete('kthc_news');
		
	}
	
	function get_nav() {
	
		$this->db->select("DISTINCT(DATE_FORMAT(published,'%Y-%m')) AS isomonth, MONTHNAME(published) AS monthname", FALSE);
		$this->db->select("YEAR(published) AS year, DATE_FORMAT(published,'%m') AS month" , FALSE);
	
		// published
		$this->db->where('post_status', 'publish');
		
		// no future posts
		$this->db->where('published <=', date('Y-m-d H:i:s'));
		
		$this->db->order_by('published', 'desc');
		
		return $this->db->get('kthc_news');	
	}
	
	
	
	function update_position($id,$position) {
		$data['position'] = $position;
		
		$this->db->where('id', $id);
		$this->db->update('kthc_news', $data);
	}

}