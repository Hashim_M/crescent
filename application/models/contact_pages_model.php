<?php

class Contact_pages_model extends Model {

	function __construct() {
		parent::__construct();
	}

	function get_pages() {
		$this->db->order_by('position');
		return $this->db->get('kthc_contact_pages');
	}
	
	function get_sidenav() {
		$this->db->select('url_title, title');
		$this->db->order_by('position');
		return $this->db->get('kthc_contact_pages');
	}
	
		
	function get_page_by_url_title($url_title) {
		$this->db->where('url_title', $url_title);
		$result = $this->db->get('kthc_contact_pages')->row();
		
		if (empty($result)) {
			show_404();
		}
		
		return $result;
	}

	function get_index_page() {
		$this->db->order_by('position', 'asc');
		$result = $this->db->get('kthc_contact_pages')->row();
		
		if (empty($result)) {
			show_404();
		}
		
		return $result;
	}
	
	function get_by_id($id) {
		$this->db->where('id', $id);
		$result = $this->db->get('kthc_contact_pages')->row();
		
		if (empty($result)) {
			show_404();
		}
		
		return $result;
	}
	
	function create($title, $url_title, $body, $position, $display_sidebar, $banners) {
		if (empty($url_title))
			$url_title 		= $title;
			
		if (empty($banners))
			$banners	= array();	
	
		$data = array(
			'title' 			=> ascii_to_entities($title),
			'url_title' 		=> strtolower(url_title($url_title, 'underscore')),
			'body' 				=> ascii_to_entities($body),
			'position' 			=> (int)$position,
			'created' 			=> date('Y-m-d H:i:s'),
			'modified' 			=> date('Y-m-d H:i:s'),
			'display_sidebar' 	=> (int)$display_sidebar,
			'banners'			=> join(',', $banners)
		);
		
		$this->db->insert('kthc_contact_pages', $data);	
	}
	
	function update($id, $title, $url_title, $body, $position, $display_sidebar, $banners) {
		if (empty($url_title))
			$url_title 		= $title;
			
		if (empty($banners))
			$banners	= array();
			
		$data = array(
			'title' 			=> ascii_to_entities($title),
			'url_title' 		=> strtolower(url_title($url_title, 'underscore')),
			'body' 				=> ascii_to_entities($body),
			'position' 			=> (int)$position,
			'modified' 			=> date('Y-m-d H:i:s'),
			'display_sidebar' 	=> (int)$display_sidebar,
			'banners'			=> join(',', $banners)
		);
		
		$this->db->where('id', $id);
		$this->db->update('kthc_contact_pages', $data);	
	}	
	
	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('kthc_contact_pages');
	}
	
	function update_position($id,$position) {
		$data['position'] = $position;
		
		$this->db->where('id', $id);
		$this->db->update('kthc_contact_pages', $data);
	}
}