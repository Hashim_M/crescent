<?php

class Contact_model extends Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	
	function get_awards_content()
	{
		return $this->db->get('kthc_contact')->row();
	}
	
	function update($location, $contact, $contact_home)
	{
		$data = array(
			'location' 		=> ascii_to_entities($location),
			'contact' 		=> ascii_to_entities($contact),
			'contact_home' 		=> ascii_to_entities($contact_home)
		);
		
		$this->db->update('kthc_contact', $data);
		
	}
	
	function get_contact_home()
	{
		$this->db->select("contact_home", FALSE);
		
		return $this->db->get('kthc_contact')->row();	
	}
	
	function get_locations() {
		return $this->db->get('kthc_location_markers')->result();
	}
	
	function create_map_marker($title, $address, $contact, $website1, $website2, $lat, $lon)
	{
		$data = array(
			'title' => $title,
			'address' => $address,
			'contact' => $contact,
			'website1' => $website1,
			'website2' => $website2,
			'lat'	=> $lat,
			'lon' => $lon
		);
		
		$this->db->insert('kthc_location_markers', $data);
	}
	
	function update_map_marker($id, $title, $address, $contact, $website1, $website2, $lat, $lon)
	{
		$data = array(
			'title' => $title,
			'address' => $address,
			'contact' => $contact,
			'website1' => $website1,
			'website2' => $website2,
			'lat'	=> $lat,
			'lon' => $lon
		);
		$this->db->where('id', $id);
		$this->db->update('kthc_location_markers', $data);
	}

	function delete_map_marker($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('kthc_location_markers');
	}
	
	function get_map_item($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('kthc_location_markers')->row();
	}
	
}
