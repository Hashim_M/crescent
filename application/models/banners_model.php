<?php

class Banners_model extends Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_banners()
	{
		$this->db->order_by('title');
		return $this->db->get('kthc_banners');
	}

	function get_banners_by_ids($ids = array())
	{	
		$this->db->where_in('id', $ids);
		return $this->db->get('kthc_banners');
	}
}