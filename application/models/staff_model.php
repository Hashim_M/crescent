<?php

class Staff_model extends Model {

	function __construct() {
		parent::__construct();
	}

	function get_pages() {
		$this->db->order_by('position');
		return $this->db->get('kthc_staff');
	}
	
	function get_sidenav() {
		$this->db->select('url_title, title');
		$this->db->order_by('position');
		return $this->db->get('kthc_staff');
	}
	
		
	function get_page_by_url_title($url_title) {
		$this->db->where('url_title', $url_title);
		$result = $this->db->get('kthc_staff')->row();
		
		if (empty($result)) {
			show_404();
		}
		
		return $result;
	}

	function get_index_page() {
		$this->db->order_by('position', 'asc');
		$result = $this->db->get('kthc_staff')->row();
		
		if (empty($result)) {
			show_404();
		}
		
		return $result;
	}
	
	function get_by_id($id) {
		$this->db->where('id', $id);
		$result = $this->db->get('kthc_staff')->row();
		
		if (empty($result)) {
			show_404();
		}
		
		return $result;
	}
	
	function create($title, $url_title, $body, $position) {
		if (empty($url_title))
			$url_title 		= $title;
	
		$data = array(
			'title' 			=> ascii_to_entities($title),
			'url_title' 		=> strtolower(url_title($url_title, 'underscore')),
			'body' 				=> ascii_to_entities($body),
			'position' 			=> (int)$position,
			'created' 			=> date('Y-m-d H:i:s'),
			'modified' 			=> date('Y-m-d H:i:s')
		);
		
		$this->db->insert('kthc_staff', $data);	
	}
	
	function update($id, $title, $url_title, $body, $position) {
		if (empty($url_title))
			$url_title 		= $title;
			
		$data = array(
			'title' 			=> ascii_to_entities($title),
			'url_title' 		=> strtolower(url_title($url_title, 'underscore')),
			'body' 				=> ascii_to_entities($body),
			'position' 			=> (int)$position,
			'modified' 			=> date('Y-m-d H:i:s')
		);
		
		$this->db->where('id', $id);
		$this->db->update('kthc_staff', $data);	
	}	
	
	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('kthc_staff');
	}
	
	function update_position($id,$position) {
		$data['position'] = $position;
		
		$this->db->where('id', $id);
		$this->db->update('kthc_staff', $data);
	}
}