<?php
class Slider_model extends Model{
	public function get_slides() {
		return $this->db->get('kthc_slides')->result();
	}
	
	public function get_slide_by_id($id) {
		$this->db->where('slide_id', $id);
		return $this->db->get('kthc_slides')->row();
	}
	
	public function update_slide($id, $data) {
		$this->db->where('slide_id', $id);
		$this->db->update('kthc_slides', $data);		
	}
}