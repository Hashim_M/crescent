<?php

class Ppg_model extends Model {
	
	function __construct() {
		parent::__construct();
	}
	
	function get_home_page() {
		$this->db->where('id', 4);
		return $this->db->get('kthc_ppg')->row();
	}
	
	function get_members_page() {
		$this->db->where('id', 10);
		return $this->db->get('kthc_ppg')->row();
	}
	
	function update_home_page($content) {
	
		$data = array(
			'body' => $content
		);
	
		$this->db->where('id', 4);
		return $this->db->update('kthc_ppg', $data);
	}
	
	function get_links() {
		$this->db->order_by('position');
		return $this->db->get('kthc_ppg_links')->result();
	}
	
	function get_item_by_url_title($news_title_url, $isodate){
		
		$this->db->where('post_status', 'publish');
		$this->db->like('published', $isodate, 'after');
		$this->db->where('url_title', $news_title_url);
		$this->db->limit(1);
		
		$result = $this->db->get('kthc_currently_doing')->row();
		
		if (empty($result)){
			show_404();
			exit;
		}
		
		return $result;
	}
	
	function create_links($title, $description, $hyperlink) {
		
		$data = array (
			'title' => $title,
			'description' => $description,
			'hyperlink' => $hyperlink,
			'modified' => date('y-m-d h:m:s')
		);
		
		return $this->db->insert('kthc_ppg_links', $data);
		
	}
	
	function get_link_by_id($id) {
		$this->db->where('id', $id);
		return $this->db->get('kthc_ppg_links')->row();
	}

	function update_link($id, $title, $description, $hyperlink) {
		
		$data = array (
			'title' => $title,
			'description' => $description,
			'hyperlink' => $hyperlink,
			'modified' => date('y-m-d h:m:s')
		);
		
		$this->db->where('id', $id);
		return $this->db->update('kthc_ppg_links', $data);
	}
	
	function delete_link($id) {
		$this->db->where('id', $id);
		return $this->db->delete('kthc_ppg_links');
	}
	
	function get_draft_currently_doing_items(){
		$this->db->where('post_status', 'draft');
		$this->db->order_by('position');
		return $this->db->get('kthc_currently_doing');
	}
	
	function get_publish_currently_doing_items(){
		$this->db->where('post_status', 'publish');
		return $this->db->get('kthc_currently_doing');
	}
	
	function get_pending_currently_doing_items(){
		$this->db->where('post_status', 'pending');
		$this->db->order_by('position');
		return $this->db->get('kthc_currently_doing');
	}
	
	
	function create_currently_doing_create($title, $url_title, $summary, $body, $post_status){
		
		if (empty($url_title)){
			$url_title 		=  $title;
		}
		
		if($post_status == 'publish'){
			$published = date('Y-m-d');
		}else{
			$published = '0000-00-00';
		}
		
		$data = array(
			'title' 		=> ascii_to_entities($title),
			'url_title' 	=> strtolower(url_title($url_title, 'underscore')),
			'summary' 		=> ascii_to_entities($summary),
			'body' 			=> ascii_to_entities($body),
			'published'     => $published,
			'created' 		=> date('Y-m-d H:i:s'),
			'modified' 		=> date('0000-00-00 00:00:00'),
			'post_status'   => $post_status
		);
		
		return $this->db->insert('kthc_currently_doing', $data);
			
	}
	
	function get_currently_doing_by_id($id) {
		$this->db->where('id', $id);
		return $this->db->get('kthc_currently_doing')->row();
	}
	
	function currently_doing_update($id, $title, $url_title, $summary, $body, $post_status){
		
		if (empty($url_title))
			$url_title 		=  $title;
			
		$data = array(
			'title' 			=> ascii_to_entities($title),
			'url_title' 		=> strtolower(url_title($url_title, 'underscore')),
			'body' 				=> ascii_to_entities($body),
			'summary' 			=> ascii_to_entities($summary),
			'modified' 			=> date('Y-m-d H:i:s'),
			'post_status' 		=> $post_status
		);
		
		$this->db->where('id', $id);
		return $this->db->update('kthc_currently_doing', $data);
		
	}
	
	function delete_currently_doing($id) {
		$this->db->where('id', $id);
		return $this->db->delete('kthc_currently_doing');
	}
	
	function get_password() {
		return $this->db->get('kthc_ppg_password')->row();
	}
	
	function update_memmbers_page($content, $password) {
		
		$data = array(
			'password' => $password
		);
		
		$this->db->where('id', 1);
		$this->db->update('kthc_ppg_password', $data);
		
		$data = array(
			'body' => $content
		);
	
		$this->db->where('id', 10);
		return $this->db->update('kthc_ppg', $data);
	}
	
	function get_currently_doing($limit = 10) {
		$this->db->order_by('position');
		return $this->db->get('kthc_currently_doing', 10)->result();
	}

	function update_links_position($id,$position) {
		$data['position'] = $position;
		
		$this->db->where('id', $id);
		$this->db->update('kthc_ppg_links', $data);
	}

	function update_currently_position($id,$position) {
		$data['position'] = $position;
		
		$this->db->where('id', $id);
		$this->db->update('kthc_currently_doing', $data);
	}
	
}