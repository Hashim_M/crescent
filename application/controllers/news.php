<?php

class News extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->Model('news_model');	
	}
		
	function index()
	{	
		$data['extra_head_content'] = '<script src="/static/js/twitter.js" type="text/javascript"></script>';
		
		//Get the latest 10 news items for the index?
		$data['news_items'] = $this->news_model->get_latest_items(10);
		
		$data['nav_items'] = $this->news_model->get_nav();
		
		$this->load->view('news/index', $data);
	}
	
	function month()
	{
			
		$news_archive_month = $this->uri->segment(3);
		$news_archive_year = $this->uri->segment(2);
		
		$news_archive_date = $news_archive_year . '-' . $news_archive_month;
		
		$data['news_items'] = $this->news_model->get_news_by_month($news_archive_date);
		$data['nav_items'] = $this->news_model->get_nav();
		$titles = $data['nav_items']->result();
		
		$data['archive_title'] = $this->get_archive_title($news_archive_month, $news_archive_year); 
		
		$data['year'] = $news_archive_year;
		$data['month'] = $news_archive_month;
			
		$this->load->view('news/month', $data);
	}
	
	function item()
	{
		$news_item_url_title = $this->uri->segment(5);
		$year = $this->uri->segment(2);
		$month = $this->uri->segment(3);
		$day = $this->uri->segment(4);
				
		if (!checkdate($month, $day, $year)){
			show_404();
		}
		
		$isodate = sprintf('%s-%s-%s', $year, $month, $day);
		
		$data['news_item'] = $this->news_model->get_item_by_url_title($news_item_url_title, $isodate);
		
		$data['nav_items'] = $this->news_model->get_nav();
		
		$this->load->view('news/item', $data);
	}
	
	function get_archive_title($news_archive_month, $news_archive_year){
		
		$months = array('01' => 'January', 	
						'02' => 'February',
						'03' => 'March',
						'04' => 'April',
						'05' => 'May',
						'06' => 'June',
						'07' => 'July',
						'08' => 'August',
						'09' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December');
						
		return $months[$news_archive_month] . ' ' . $news_archive_year;
		
	}
	
}