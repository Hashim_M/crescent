<?php

class Search extends Controller {

	function __construct()
	{
		parent::__construct();	
	}
	
	function index()
	{
		$this->load->view('search/index');
	}
	
	function results()
	{
		$this->load->view('search/results');
	}
	
}