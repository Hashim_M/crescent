<?php

class Contact extends Controller {

	public $data;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('contact_pages_model');	
	}
	
	function index() {
	
		$url_title = $this->uri->segment(2);
		if (!empty($url_title)) {			
			$data['content'] = $this->contact_pages_model->get_page_by_url_title($url_title);
		} else {
			$data['content'] = $this->contact_pages_model->get_index_page();
		}
		
		
		$data['sidenav'] = $this->contact_pages_model->get_sidenav();
		$data['page_name'] = $data['content']->title;
					
		$this->load->view('contact_pages/page', $data);
	}
	
	// Not used currently?
	function feedback()
	{
		$this->load->view('contact/feedback');
	}
	
}