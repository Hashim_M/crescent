<?php

class Ppg extends controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model('ppg_model');
		$this->load->model('ppg_pages_model');	
	}
	
	function index() {
	
		
		$url_title = $this->uri->segment(2);

		if (!empty($url_title)){

			switch ($url_title) {
				case 'links':
					$this->links();
					break;
				case 'minutes':
					$this->currently_doing();
					break;
				case 'members':
					$this->members();
					break;
				
				default:
					$data['content'] = $this->ppg_pages_model->get_page_by_url_title($url_title);

					$data['sidenav'] = $this->ppg_pages_model->get_sidenav();
					$data['page_name'] = $data['content']->title;
		
					$this->load->view('ppg_pages/page', $data);
					break;
			}

		} else {
			$data['content'] = $this->ppg_model->get_home_page();
			$data['sidenav'] = $this->ppg_pages_model->get_sidenav();
	
			$this->load->view('ppg/index', $data);
		}
		
		
		
	}
	
	function links() {
		$data['links'] = $this->ppg_model->get_links();
		$data['sidenav'] = $this->ppg_pages_model->get_sidenav();
		$this->load->view('ppg/links', $data);
	}
	
	function currently_doing() {
		
		$data['currently_doing'] = $this->ppg_model->get_currently_doing(10);
		$data['sidenav'] = $this->ppg_pages_model->get_sidenav();
		$this->load->view('ppg/currently_doing', $data);
	}
	
	function members() {
		
		$data['logged_in'] = FALSE;
		
		$data['content'] = $this->ppg_model->get_members_page();
		$data['sidenav'] = $this->ppg_pages_model->get_sidenav();
	
		if($this->input->post('access_code')) {
			
			$password = $this->ppg_model->get_password();
	
			if($this->input->post('access_code') == $password->password) {
				
				$data['logged_in'] = TRUE;
				
			} else {
			
				$data['error'] = 'Invalid Access Code';
			
			}
			
		}
	
		$this->load->view('ppg/members', $data);
	}
	
	function currently_doing_item() {
		
		$news_item_url_title = $this->uri->segment(6);
		$year = $this->uri->segment(3);
		$month = $this->uri->segment(4);
		$day = $this->uri->segment(5);
				
		if (!checkdate($month, $day, $year)){
			show_404();
		}
		
		$isodate = sprintf('%s-%s-%s', $year, $month, $day);
		$data['sidenav'] = $this->ppg_pages_model->get_sidenav();
		$data['news_item'] = $this->ppg_model->get_item_by_url_title($news_item_url_title, $isodate);	
		
		$this->load->view('ppg/item', $data);
	}
	
}