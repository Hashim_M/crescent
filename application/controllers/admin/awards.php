<?php

class Awards extends Controller 
{

	function __construct(){
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		
		$this->load->model('awards_model');
		$this->load->plugin('xinha');
	}
	
	function index(){
	
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/awards','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body', 'body_right'));
	
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {
				
				$current_content = $this->awards_model->get_awards_content();
				  
				$this->validation->set_default_values('body', 		$current_content->body);
				$this->validation->set_default_values('body_right', 	$current_content->sidebar);
			}

			$this->load->view('admin/awards/index', $data);
			
		} else {
			// save
			$this->awards_model->update(
				$this->input->post('body'),
				$this->input->post('body_right')
			);
			
			redirect('admin/awards/index');
	
			$this->load->view('admin/awards/index');
		}
		
	}
}