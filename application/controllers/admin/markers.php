<?php

class Markers extends Controller {
	
	function __construct() {
		parent::__construct();
	
		$this->load->model('contact_model');
	}
	
	function index() {
		
		$data['extra_head_content'] = '<script src="/static/js/confirmdelete.js" type="text/javascript"></script>';

		$data['items'] = $this->contact_model->get_locations();
		$this->load->view('admin/contact/markers', $data);
	}
	
	function create() {
		
		$this->load->library('validation');
		$this->load->view('validation/admin/markers','',FALSE);
			
		// Validate
		if ($this->validation->run() == FALSE) {		
			$this->load->view('admin/contact/markers_create');
		} else {				
			// Save
			$this->contact_model->create_map_marker(
				$this->input->post('title'), 
				$this->input->post('address'), 
				$this->input->post('contact'), 
				$this->input->post('website1'), 
				$this->input->post('website2'), 	
				$this->input->post('lat'), 
				$this->input->post('lon')							
				);
			redirect('/admin/markers');
		}
	}
	
	function update()
	{
		// gid
		if ((int) $this->uri->segment(4)) 
		{
			$data['id'] = (int) $this->uri->segment(4);
		} 
		else 
		{
			$data['id'] = (int) $this->input->post('id');
		}
	
		$this->load->library('validation');
		$this->load->view('validation/admin/markers','',FALSE);
			
		// Validate
		if ($this->validation->run() == FALSE)
		{		
			$data['item'] = $this->contact_model->get_map_item($data['id']);
			
			$this->validation->set_default_values('title',		$data['item']->title);
			$this->validation->set_default_values('address', 	$data['item']->address);
			$this->validation->set_default_values('contact', 	$data['item']->contact);
			$this->validation->set_default_values('website1', 	$data['item']->website1);
			$this->validation->set_default_values('website2', 	$data['item']->website2);
			$this->validation->set_default_values('lat', 		$data['item']->lat);
			$this->validation->set_default_values('lon', 		$data['item']->lon);
			
			$this->load->view('admin/contact/markers_update', $data);
		}
		else
		{				
			// Save
			$this->contact_model->update_map_marker(
				$this->input->post('id'), 
				$this->input->post('title'), 
				$this->input->post('address'), 
				$this->input->post('contact'), 
				$this->input->post('website1'), 
				$this->input->post('website2'), 	
				$this->input->post('lat'), 
				$this->input->post('lon')							
				);
			redirect('/admin/markers/');
		}
	}
	
	function delete()
	{
		$id = (int) $this->uri->segment(4);
		$this->contact_model->delete_map_marker($id);
		redirect('/admin/markers/');	
	}
	
}