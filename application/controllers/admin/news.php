<?php 

class News extends Controller {

	function __construct()
	{
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		
		$this->load->model('news_model');
		$this->load->plugin('xinha');
	}
	
	function index()
	{
		$data['draft_news'] = $this->news_model->get_draft_news_items();
		$data['publish_news'] = $this->news_model->get_publish_news_items();
		$data['pending_news'] = $this->news_model->get_pending_news_items();
		$this->load->view('admin/news/index', $data);
	}
	
	function create(){
		
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/news','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {  
				$this->validation->set_default_values('title', 		'');
				$this->validation->set_default_values('summary', 	'');
				$this->validation->set_default_values('body', 		'');
				$this->validation->set_default_values('post_status','publish');
				$this->validation->set_default_values('url_title', 	'');
			}

			$this->load->view('admin/news/create', $data);
			
		} else {
			// save
			$this->news_model->create(
				$this->input->post('title'),
				$this->input->post('url_title'),
				$this->input->post('summary'),
				$this->input->post('body'),	
				$this->input->post('post_status')
			);
			
			$this->generate_feed();
						
			redirect('admin/news');
		}	
		
	}
	
	function update()
	{
		$this->load->library('validation');		
		$this->load->view('validation/admin/news','',FALSE);		
	
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		if ((int) $this->uri->segment(4)) {
			$data['id'] = (int) $this->uri->segment(4);
		} 
		else {
			$data['id'] = (int) $this->input->post('id');		
		}
		
		// get current item
		$data['item'] = $this->news_model->get_item_by_id($data['id']);		
		
		// Validate
		if ($this->validation->run() == FALSE){		
			// For initial load set default values
			if (count($_POST) == 0){
				$this->validation->set_default_values('title', 		$data['item']->title);
				$this->validation->set_default_values('summary', 	$data['item']->summary);
				$this->validation->set_default_values('body', 		$data['item']->body);
				$this->validation->set_default_values('post_status',$data['item']->post_status);
				$this->validation->set_default_values('url_title', 	$data['item']->url_title);
			}
			$this->load->view('admin/news/update', $data);
		}
		else
		{
			// save
			$this->news_model->update(
				$this->input->post('id'),
				$this->input->post('title'),
				$this->input->post('url_title'),
				$this->input->post('summary'),	
				$this->input->post('body'),
				$this->input->post('post_status')
			);
						
			$this->generate_feed();			
						
			redirect('admin/news');
		}		
	}	
	
	function delete($id)
	{		
		$this->news_model->delete($id);			
		
		$this->generate_feed();
		
		redirect('admin/news');
	}
	
	function generate_feed(){
		// Load helpers
		$this->load->helper('file');
		$this->load->helper('xml');
	
		// Get items
		$data['items'] =  $this->news_model->get_latest_items(10);
	
		// Build file
		$file = $this->load->view('admin/news/feed', $data, TRUE);
		
		// Write file
		write_file($_SERVER['DOCUMENT_ROOT'] . '/rss/news.xml', $file);
	}
	
	
	
	function order() {
		$this->load->helper('ajax');
		
		if (!isAjax()) 
			return;

		$items = $this->input->post('items');
		$position = 1;
		
		foreach($items as $item) {
			$id = (int)$item;
			$this->news_model->update_position($id, $position);
			$position++;
		}
	}
}