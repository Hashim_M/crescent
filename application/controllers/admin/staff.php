<?php

class Staff extends Controller {

	function __construct() {
	
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { 
			redirect('admin/login'); 
			exit; 
		}
		
		$this->load->model('staff_model');
		$this->load->plugin('xinha');
	}
	
	function index() {
		$data['pages'] = $this->staff_model->get_pages();
		$this->load->view('admin/staff/index', $data);
	}

	function create() {
	
		$this->load->library('validation');
		$this->load->view('validation/admin/staff','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		// Validate
		if ($this->validation->run() == FALSE) {		
			// For initial load set default values
			if (count($_POST) == 0) {  
				$this->validation->set_default_values('title', 				'');
				$this->validation->set_default_values('body', 				'');
				$this->validation->set_default_values('url_title', 			'');
				$this->validation->set_default_values('position', 			'0');
			}

			$this->load->view('admin/staff/create', $data);
		} else {
			// save
			$this->staff_model->create(
				$this->input->post('title'),
				$this->input->post('url_title'),	
				$this->input->post('body'),
				$this->input->post('position')
			);
			redirect('admin/staff');
		}		
	}
	
	function update() {
		
		$this->load->library('validation');		
		$this->load->view('validation/admin/staff','',FALSE);		
	
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		if ((int) $this->uri->segment(4)) {
			$data['id'] = (int) $this->uri->segment(4);
		} else {
			$data['id'] = (int) $this->input->post('id');		
		}
		
		// get current item
		$data['item'] = $this->staff_model->get_by_id($data['id']);		
	
		// Validate
		if ($this->validation->run() == FALSE) {		
			// For initial load set default values
			if (count($_POST) == 0) {  
				$this->validation->set_default_values('title', 				$data['item']->title);
				$this->validation->set_default_values('body', 				$data['item']->body);
				$this->validation->set_default_values('url_title', 			$data['item']->url_title);
				$this->validation->set_default_values('position', 			$data['item']->position);
			}
			$this->load->view('admin/staff/update', $data);
		} else {
			// save
			$this->staff_model->update(
				$this->input->post('id'),
				$this->input->post('title'),
				$this->input->post('url_title'),	
				$this->input->post('body'),
				$this->input->post('position')
			);
			redirect('admin/staff');
		}		
	}	
	
	function delete($id) {		
		$this->staff_model->delete($id);			
		redirect('admin/staff');
	}
	
	function order() {
		$this->load->helper('ajax');
		
		if (!isAjax()) 
			return;

		$items = $this->input->post('items');
		$position = 1;
		
		foreach($items as $item) {
			$id = (int)$item;
			$this->staff_model->update_position($id, $position);
			$position++;
		}
	}
}