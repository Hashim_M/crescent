<?php

class Contact extends Controller 
{

	function __construct(){
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		
		$this->load->model('contact_model');
		$this->load->plugin('xinha');
	}
	
	function index(){
	
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/contact','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('location', 'contact', 'contact_home'));
	
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {
				
				$current_content = $this->contact_model->get_awards_content();
				  
				$this->validation->set_default_values('location', 	$current_content->location);
				$this->validation->set_default_values('contact', 	$current_content->contact);
				$this->validation->set_default_values('contact_home', 	$current_content->contact_home);
			}

			$this->load->view('admin/contact/index', $data);
			
		} else {
			// save
			$this->contact_model->update(
				$this->input->post('location'),
				$this->input->post('contact'),
				$this->input->post('contact_home')
			);
			
			redirect('admin/contact/index');
	
			$this->load->view('admin/contact/index');
		}
		
	}
}