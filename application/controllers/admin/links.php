<?php 

class Links extends Controller {

	function __construct() {
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		
		$this->load->model('links_model');
	}
	
	function index() {
		$data['links'] = $this->links_model->get_links();
		$this->load->view('admin/links/index', $data);
	}
	
	function create(){
		
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/links','',FALSE);
		
		// Load the css and js needed for the thickbox
		$data['extra_head_content'] = $this->_thickbox();
		
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {  
				$this->validation->set_default_values('title', 		'');
				$this->validation->set_default_values('description', 	'');
				$this->validation->set_default_values('hyperlink', 		'http://');
			}

			$this->load->view('admin/links/create', $data);
			
		} else {
		
		
			$thumbnail = $this->input->post('thumbnail_file');
			
			if (!empty($thumbnail))
			{
				$root = $_SERVER['DOCUMENT_ROOT'];
				
				if (!rename($root . '/tmp/' . $thumbnail, $root . '/storage/links/' . $thumbnail))
				{
					$thumbnail = "";
				}
			}		
		
		
			// save
			$this->links_model->create(
				$this->input->post('title'),
				$this->input->post('description'),
				$this->input->post('hyperlink'), 
				$thumbnail
			);
			
			redirect('admin/links');
		}	
		
	}
	
	function update() {
	
		$this->load->library('validation');		
		$this->load->view('validation/admin/links','',FALSE);		
	
		if ((int) $this->uri->segment(4)) {
			$data['id'] = (int) $this->uri->segment(4);
		} 
		else {
			$data['id'] = (int) $this->input->post('id');		
		}
		
		// get current item
		$data['item'] = $this->links_model->get_item_by_id($data['id']);		

		// Load the css and js needed for the thickbox
		$data['extra_head_content'] = $this->_thickbox();	
	
		// Validate
		if ($this->validation->run() == FALSE) {		
			// For initial load set default values
			if (count($_POST) == 0){
				$this->validation->set_default_values('title', 		    $data['item']->title);
				$this->validation->set_default_values('description', 	$data['item']->description);
				$this->validation->set_default_values('hyperlink', 		$data['item']->hyperlink);
				
				if (!empty($data['item']->thumbnail))
				{
					$this->validation->set_default_values('thumbnail_file', 		$data['item']->thumbnail);	
					$this->validation->set_default_values('thumbnail_filepath', 	'/storage/links/' . $data['item']->thumbnail);	
				}
				
			}
			$this->load->view('admin/links/update', $data);
		}
		else 
		{
		
			$thumbnail = $this->input->post('thumbnail_file');			
						
			if (!empty($data['item']->thumbnail))
			{
				
				
				// check if different
				if ($data['item']->thumbnail != $thumbnail)
				{
					
					$root = $_SERVER['DOCUMENT_ROOT'];
				
				
					// remove old
					@unlink($root . '/storage/links/' .$data['item']->thumbnail);

										
					// new
					if (!empty($thumbnail))
					{
						
						
						if (!@rename($root . '/tmp/' . $thumbnail, $root . '/storage/links/' . $thumbnail))
						{
							$thumbnail = "";
						}
					}
				
				}			
			}
			else
			{
				// new
				if (!empty($thumbnail))
				{
					$root = $_SERVER['DOCUMENT_ROOT'];
					
					if (!@rename($root . '/tmp/' . $thumbnail, $root . '/storage/links/' . $thumbnail))
					{
						$thumbnail = "";
					}
				}				
			}
				
		
			// save
			$this->links_model->update(
				$this->input->post('id'),
				$this->input->post('title'),
				$this->input->post('description'),
				$this->input->post('hyperlink'),
				$thumbnail
			);
									
			redirect('admin/links');
		}		
	}	
	
	function delete($id) {		
		
		$this->links_model->delete($id);			
		
		redirect('admin/links');
	}
	
	function _thickbox()
	{
		ob_start();
	?>
			<link type="text/css" rel="stylesheet" href="/thickbox/thickbox.css" media="screen" />
			<script type="text/javascript" src="/thickbox/jquery.js"></script>
			<script type="text/javascript" src="/thickbox/thickbox.js"></script>
			<script>
			     jQuery.noConflict();
			</script>
	<?php
		$output = ob_get_contents(); 	
		ob_end_clean();
		return $output;
	}
	
	
	function upload()
	{
		$error = array('error' => '');			
		$this->load->view('admin/links/upload_form', $error);
	}
	
	function do_upload()
	{	
		// store the new upload in a tmp location
		$config['upload_path'] = './tmp/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload())
		{
			// failed - back to form
			$error = array('error' => $this->upload->display_errors());			
			$this->load->view('admin/links/upload_form', $error);
		}	
		else
		{
			// uploaded
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('admin/links/crop_form', $data);
		}
	}
	
	
	function crop()
	{
		$imagepath = $this->input->post('imagepath');
		
		if (!$imagepath)
		{
			echo 'no img!';
			exit;
		}
		
		// coordinates
    	$sx = $this->input->post('x1');
    	$sy = $this->input->post('y1');
    	$ex = $this->input->post('x2');
    	$ey = $this->input->post('y2');	
    	
    	
    	// load library
    	$this->load->library('cropcanvas');
    	
    	// load img
    	$this->cropcanvas->load($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $imagepath);
    	
    	// crop
    	$this->cropcanvas->crop($sx,$sy,$ex,$ey);
    	
    	// scale
    	$this->cropcanvas->scaleTo(253,168);
    	
    	// save
    	if (!$this->cropcanvas->save($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $imagepath))
    	{
    		echo 'The file could not be saved. ' . $this->cropcanvas->error;
    		exit;
    	}	
    	
    	$data['imagepath'] = $imagepath;
    		
    	$this->load->view('admin/links/crop_success', $data);

	}	
	
}