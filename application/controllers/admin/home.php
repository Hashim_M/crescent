<?php

class Home extends Controller {

	function __construct()
	{
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		$this->load->Model('slider_model');
	}
	
	function index()
	{
		$data['slides'] = $this->slider_model->get_slides();
		$this->load->view('admin/home/index', $data);
	}
	
	function edit_slider($slide_id) {
		if(!$data['slide'] = $this->slider_model->get_slide_by_id($slide_id)){
			echo 'Invalid slide id';
			die();
		}
		$this->load->helper('form_helper');
		$this->load->plugin('xinha');
		$this->load->library('validation');
		$data['extra_head_content'] = create_xinha(array('slide_content'));
		$this->load->view('validation/admin/slider','',FALSE);
		// Validate
		
		if($this->validation->run() != FALSE && !empty($_FILES) && $_FILES['slide_image']['size']>0) {
			$config['upload_path'] = './static/img/sections/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';
			$config['max_width']  = '901';
			$config['max_height']  = '277';
			
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('slide_image'))
			{
				$data['error'] = '<div class="error">' . $this->upload->display_errors() . '</div>';
			} else {
				$upload = $this->upload->data();
			}
		}
		
		if ($this->validation->run() == FALSE || !empty($data['error'])) {
			if (count($_POST) == 0) {
				$this->validation->set_default_values('slide_title', 	$data['slide']->slide_title);
				$this->validation->set_default_values('slide_content', 	$data['slide']->slide_content);
				$this->validation->set_default_values('slide_image', 	$data['slide']->slide_image);
				$this->validation->set_default_values('slide_position', $data['slide']->slide_position);
			}
		} else {
			$save['slide_title'] = $_POST['slide_title'];
			$save['slide_content'] = $_POST['slide_content'];
			$save['slide_position'] = $_POST['slide_position'];
			
			if(isset($upload['file_name']))
				$save['slide_image'] = $upload['file_name'];
			
			$this->slider_model->update_slide($slide_id, $save);
			redirect('/admin/home/edit_slider/' . $slide_id .'/updated');
		}

		
		$this->load->view('admin/home/slider', $data);				
	}	
}