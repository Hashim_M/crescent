<?php

class Practice_info extends Controller {

	function __construct()
	{
		parent::__construct();	
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		
		$this->load->model('practice_info_model');
		$this->load->model('banners_model');
		$this->load->plugin('xinha');
	}
	
	function index()
	{
		$data['pages'] = $this->practice_info_model->get_pages();
		$this->load->view('admin/practice_info/index', $data);
	}


	function create()
	{
		$this->load->library('validation');
		$this->load->view('validation/admin/practice_info','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
		
		// banners
		$data['banners'] = $this->banners_model->get_banners();
	
		// Validate
		if ($this->validation->run() == FALSE)
		{		
			// For initial load set default values
			if (count($_POST) == 0)
			{  
				$this->validation->set_default_values('title', 				'');
				$this->validation->set_default_values('body', 				'');
				$this->validation->set_default_values('url_title', 			'');
				$this->validation->set_default_values('position', 			'0');
				$this->validation->set_default_values('display_sidebar', 	'1');
			}

			$this->load->view('admin/practice_info/create', $data);
		}
		else
		{
		
			// save
			$this->practice_info_model->create(
				$this->input->post('title'),
				$this->input->post('url_title'),	
				$this->input->post('body'),
				$this->input->post('position'),
				$this->input->post('display_sidebar'),
				$this->input->post('banners')
			);
						
			redirect('admin/practice_info');
		}		
	}
	
	function update()
	{
		$this->load->library('validation');		
		$this->load->view('validation/admin/practice_info','',FALSE);		
	
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		if ((int) $this->uri->segment(4)) 
		{
			$data['id'] = (int) $this->uri->segment(4);
		} 
		else 
		{
			$data['id'] = (int) $this->input->post('id');		
		}
		
		// get current item
		$data['item'] = $this->practice_info_model->get_by_id($data['id']);		
		
		// banners
		$data['banners'] = $this->banners_model->get_banners();
	
		// Validate
		if ($this->validation->run() == FALSE)
		{		
			// For initial load set default values
			if (count($_POST) == 0)
			{  
				$this->validation->set_default_values('title', 				$data['item']->title);
				$this->validation->set_default_values('body', 				$data['item']->body);
				$this->validation->set_default_values('url_title', 			$data['item']->url_title);
				$this->validation->set_default_values('position', 			$data['item']->position);
				$this->validation->set_default_values('display_sidebar', 	$data['item']->display_sidebar);
				$this->validation->set_default_values('banners', 			explode(',', $data['item']->banners));
			}

			$this->load->view('admin/practice_info/update', $data);
		}
		else
		{
			// save
			$this->practice_info_model->update(
				$this->input->post('id'),
				$this->input->post('title'),
				$this->input->post('url_title'),	
				$this->input->post('body'),
				$this->input->post('position'),
				$this->input->post('display_sidebar'),
				$this->input->post('banners')
			);
						
			redirect('admin/practice_info');
		}		
	}	
	
	function delete($id)
	{		
		$this->practice_info_model->delete($id);			
		redirect('admin/practice_info');
	}
	
	function order()
	{
		$this->load->helper('ajax');
		
		if (!isAjax()) 
			return;

		$items = $this->input->post('items');
		$position = 1;
		
		foreach($items as $item)
		{
			$id = (int)$item;
			$this->practice_info_model->update_position($id,$position);
			$position++;
		}
	}
}