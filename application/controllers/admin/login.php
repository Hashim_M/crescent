<?php

class Login extends Controller {

	function __construct()
	{
		parent::__construct();	
	}
	
	function index()
	{
		$this->load->library('validation');
			
		$rules['username']	= "required";
		$rules['password']	= "required";
		
		$this->validation->set_rules($rules);
		
		$fields['username'] = 'Username';
		$fields['password'] = 'Password';
		
		
		$this->validation->set_fields($fields);		
		$this->validation->set_error_delimiters('<div class="error">', '</div>');	
		
			
		if ($this->validation->run() == FALSE)
		{		
		
			// For initial load set default values
			if (count($_POST) == 0)
			{  
				$this->validation->set_default_values('username', 'admin');				
			}
		
			// fail on validation
			$data['error'] = '';
			$this->load->view('admin/login/index', $data);
		}
		else
		{			
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			
			// try login			
			if ($username != 'admin' || $password != 'noodle1')
			{
				// fail on entering info
				$data['error'] = '<div class="error">Either the username or password has been entered incorrectly or the account doesn\'t exist. Please check, and try again.</div>';
				$this->load->view('admin/login/index', $data);
			}
			else
			{		
				// store id of logged in
				$this->session->set_userdata('logged_in', 'true');
	
				// redirect to profile
				redirect('admin');
			}
		}
	}
}

