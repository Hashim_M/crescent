<?php

class Ppg extends Controller {
	
	function __construct() {
		parent::__construct();
		
		if (!$logged_in = $this->session->userdata('logged_in')) { redirect('admin/login'); exit; }
		
		$this->load->model('ppg_model');
		$this->load->plugin('xinha');

		
	}
	
	function index() {
	
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/ppg','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('ppg_home'));
	
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {
				
				$current_content = $this->ppg_model->get_home_page();
				  
				$this->validation->set_default_values('ppg_home', 	$current_content->body);
			}

			$this->load->view('admin/ppg/index', $data);
			
		} else {
			// save
			$this->ppg_model->update_home_page(
				$this->input->post('ppg_home')
			);
			
			redirect('admin/ppg/index');
	
			$this->load->view('admin/ppg/index');
		}
		
	}
	
	function links() {
	
		$data['links'] = $this->ppg_model->get_links();
		
		$this->load->view('admin/ppg/links/index', $data);
	}
	
	function links_create() {
		
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/links','',FALSE);
		
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {  
				$this->validation->set_default_values('title', 		'');
				$this->validation->set_default_values('description', 	'');
				$this->validation->set_default_values('hyperlink', 		'http://');
			}

			$this->load->view('admin/ppg/links/create');
			
		} else {
		
			// save
			$this->ppg_model->create_links(
				$this->input->post('title'),
				$this->input->post('description'),
				$this->input->post('hyperlink')
			);
			
			redirect('admin/ppg/links');
		}	
		
	}
	
	function links_update() {
		
		$this->load->library('validation');		
		$this->load->view('validation/admin/links','',FALSE);		
	
		if ((int) $this->uri->segment(5)) {
			$data['id'] = (int) $this->uri->segment(5);
		} else {
			$data['id'] = (int) $this->input->post('id');		
		}
		
		// get current item
		$data['item'] = $this->ppg_model->get_link_by_id($data['id']);		

		// Validate
		if ($this->validation->run() == FALSE) {		
			// For initial load set default values
			if (count($_POST) == 0){
				$this->validation->set_default_values('title', 		    $data['item']->title);
				$this->validation->set_default_values('description', 	$data['item']->description);
				$this->validation->set_default_values('hyperlink', 		$data['item']->hyperlink);
			}
		
			$this->load->view('admin/ppg/links/update', $data);
		
		} else {
		
			// save
			$this->ppg_model->update_link(
				$this->input->post('id'),
				$this->input->post('title'),
				$this->input->post('description'),
				$this->input->post('hyperlink')
			);
									
			redirect('admin/ppg/links');
		}		
	}
	
	function links_delete() {
		
		if ((int) $this->uri->segment(5)) {
			$id = (int) $this->uri->segment(5);
		} else {
			return;		
		}

		$this->ppg_model->delete_link($id);			
		
		redirect('admin/ppg/links');
	}
	
	function currently_doing() {
		$data['draft_news'] = $this->ppg_model->get_draft_currently_doing_items();
		$data['publish_news'] = $this->ppg_model->get_publish_currently_doing_items();
		$data['pending_news'] = $this->ppg_model->get_pending_currently_doing_items();
		$this->load->view('admin/ppg/currently_doing/index', $data);
	}
	
	function currently_doing_create() {
		
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/ppg_currently_doing','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {  
				$this->validation->set_default_values('title', 		'');
				$this->validation->set_default_values('summary', 	'');
				$this->validation->set_default_values('body', 		'');
				$this->validation->set_default_values('post_status','publish');
				$this->validation->set_default_values('url_title', 	'');
			}

			$this->load->view('admin/ppg/currently_doing/create', $data);
			
		} else {
			
			// save
			$this->ppg_model->create_currently_doing_create(
				$this->input->post('title'),
				$this->input->post('url_title'),
				$this->input->post('summary'),
				$this->input->post('body'),	
				$this->input->post('post_status')
			);
						
			redirect('admin/ppg/minutes');
		}	
	}
	
	function currently_doing_update() {
	
		$this->load->library('validation');		
		$this->load->view('validation/admin/news','',FALSE);		
	
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('body'));
	
		if ((int) $this->uri->segment(5)) {
			$data['id'] = (int) $this->uri->segment(5);
		} 
		else {
			$data['id'] = (int) $this->input->post('id');		
		}
		
		// get current item
		$data['item'] = $this->ppg_model->get_currently_doing_by_id($data['id']);		
		
		// Validate
		if ($this->validation->run() == FALSE){		
			// For initial load set default values
			if (count($_POST) == 0){
				$this->validation->set_default_values('title', 		$data['item']->title);
				$this->validation->set_default_values('summary', 	$data['item']->summary);
				$this->validation->set_default_values('body', 		$data['item']->body);
				$this->validation->set_default_values('post_status',$data['item']->post_status);
				$this->validation->set_default_values('url_title', 	$data['item']->url_title);
			}
			$this->load->view('admin/ppg/currently_doing/update', $data);
		} else {
			// save
			$this->ppg_model->currently_doing_update(
				$this->input->post('id'),
				$this->input->post('title'),
				$this->input->post('url_title'),
				$this->input->post('summary'),	
				$this->input->post('body'),
				$this->input->post('post_status')
			);
							
			redirect('admin/ppg/minutes');
		}	
	}
	
	function currently_doing_delete() {
		
		if ((int) $this->uri->segment(5)) {
			$id = (int) $this->uri->segment(5);
		} else {
			return;		
		}

		$this->ppg_model->delete_currently_doing($id);			
		
		redirect('admin/ppg/minutes');
	}
	
	function members_page() {
		
		$this->load->library('validation');
		# Load in the validation rules as a view
		$this->load->view('validation/admin/ppg_members_page','',FALSE);
		
		// add xinha js
		$data['extra_head_content'] = create_xinha(array('ppg_home'));
	
		// Validate
		if ($this->validation->run() == FALSE) {
				
			// For initial load set default values
			if (count($_POST) == 0) {
				
				$current_content = $this->ppg_model->get_members_page();
				$password = $this->ppg_model->get_password();
				  
				$this->validation->set_default_values('ppg_home', 		$current_content->body);
				$this->validation->set_default_values('ppg_password', 	$password->password);
			}

			$this->load->view('admin/ppg/members_page', $data);
			
		} else {
			// save
			$this->ppg_model->update_memmbers_page(
				$this->input->post('ppg_home'),
				$this->input->post('ppg_password')
			);
			
			redirect('admin/ppg/members_page');
	
			$this->load->view('admin/ppg/members_page');
		}

	}

	function order_links() {
		$this->load->helper('ajax');
		
		if (!isAjax()) 
			return;

		$items = $this->input->post('items');
		$position = 1;
		
		foreach($items as $item) {
			$id = (int)$item;
			$this->ppg_model->update_links_position($id,$position);
			$position++;
		}
	}

	function order_currently_doing() {
		$this->load->helper('ajax');
		
		if (!isAjax()) 
			return;

		if(isset($_POST['draft-items']))
			$items = $this->input->post('draft-items');

		if(isset($_POST['publish-items']))
			$items = $this->input->post('publish-items');

		if(isset($_POST['pending-items']))
			$items = $this->input->post('pending-items');
		
		$position = 1;
		

		foreach($items as $item) {
			$id = (int)$item;
			$this->ppg_model->update_currently_position($id,$position);
			$position++;
		}
	}

	
}