<?php

class Patient_services extends Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('patient_services_model');
		$this->load->model('banners_model');
	}

	function index() {

		$url_title = $this->uri->segment(2);
		if (!empty($url_title)) {
			$data['content'] = $this->patient_services_model->get_page_by_url_title($url_title);
		} else {
			$data['content'] = $this->patient_services_model->get_index_page();
		}

		$banner_ids 		= explode(',', $data['content']->banners);

    $data['banners']   = $this->banners_model->get_banners_by_ids($banner_ids);
    $data['sidenav']   = $this->patient_services_model->get_sidenav();
    $data['page_name'] = $data['content']->title;

		$this->load->view('patient_services/page', $data);
	}

	function register()
  {
		$data['submitted'] = FALSE;

		$this->load->library('validation');

		# Load in the validation rules as a view
		$this->load->view('validation/register', '', FALSE);

		if ($this->validation->run() == FALSE) {
			## Default values

			// For initial load set default values
			if (count($_POST) == 0) {
        $this->validation->set_default_values('prefix',                 '');
        $this->validation->set_default_values('surname',                '');
        $this->validation->set_default_values('firstname',              '');
        $this->validation->set_default_values('country_of_birth',       '');
        $this->validation->set_default_values('address',                '');
        $this->validation->set_default_values('postcode',               '');
        $this->validation->set_default_values('number',                 '');
        $this->validation->set_default_values('email',                  '');
        $this->validation->set_default_values('previousaddress',        '');
        $this->validation->set_default_values('previousdoctor',         '');
        $this->validation->set_default_values('previousdoctoraddress',  '');
        $this->validation->set_default_values('gender',                 '');
        $this->validation->set_default_values('dob_day',                '');
        $this->validation->set_default_values('dob_month',              '');
        $this->validation->set_default_values('dob_year',               '');
        $this->validation->set_default_values('datearrived_day',        '');
        $this->validation->set_default_values('datearrived_month',      '');
        $this->validation->set_default_values('datearrived_year',       '');
        $this->validation->set_default_values('abroad',                 '');
        $this->validation->set_default_values('next_kin_name',          '');
        $this->validation->set_default_values('next_kin_relationship',  '');
        $this->validation->set_default_values('next_kin_phone',         '');
        $this->validation->set_default_values('occupation',             '');
        $this->validation->set_default_values('is_carer',               '');
        $this->validation->set_default_values('has_carer',              '');
        $this->validation->set_default_values('ethnicity',              '');
        $this->validation->set_default_values('interpreter_required',   '');
        $this->validation->set_default_values('known_allergies',        '');
        $this->validation->set_default_values('current_smoker',         '');
        $this->validation->set_default_values('past_smoker',            '');
        $this->validation->set_default_values('alcohol_frequency',      '');
        $this->validation->set_default_values('alcohol_per_session',    '');
        $this->validation->set_default_values('alcohol_max_per_session','');
        $this->validation->set_default_values('family_heart_attack_or_angina_member', '');
        $this->validation->set_default_values('family_other_health_problems_member', '');
        $this->validation->set_default_values('hear_about',             '');
			}
      else {
        $data['validation_failed'] = true;
      }

		} else {

			if (!empty($_POST)) {
          $config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'smtp.sendgrid.net',
          'smtp_port' => 587,
          'smtp_user' => 'boilerhousedigital',
          'smtp_pass' => '@Boilerhous32016',
          'mailtype'  => 'html', 
          'charset'   => 'iso-8859-1'
      );
        $this->load->library('email',$config);

				## Generate the email
        $email_title = "Queens Crescent Practice registration submission";

				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // message
        $email_content  = '<strong>Patients Details</strong><br />';
        $email_content .= 'Prefix: ' . $_POST['prefix'] . '<br />';
        $email_content .= 'Surname: ' . $_POST['surname'] . '<br />';
        $email_content .= 'First Name: ' . $_POST['firstname'] . '<br />';
        $email_content .= 'Previous Surname: ' . $_POST['previoussurname'] . '<br />';
        $email_content .= 'Gender: ' . $_POST['gender'] . '<br />';
        $email_content .= 'DOB: ' . $_POST['dob_day'] . '/' . $_POST['dob_month'] . '/' . $_POST['dob_year'] . '<br />';
        $email_content .= 'Town and Country of Birth: ' . $_POST['country_of_birth'] . '<br />';

        $email_content .= '<br /><strong>Where you born in London: '. $_POST['born_in_london'] .'</strong><br />';
        if($_POST['born_in_london'] == 'Yes'){
          $email_content .= ' Yes: ' . $_POST['london_borough'] . '<br />';
        } else {
          $email_content .= ' No<br/>';
        }

        $email_content .= 'Address: ' . $_POST['address'] . '<br />';
        $email_content .= 'Post Code: ' . $_POST['postcode'] . '<br />';
        $email_content .= 'Number: ' . $_POST['number'] . '<br />';
        $email_content .= 'Email: ' . $_POST['email'] . '<br />';
        $email_content .= 'NHS Number: ' . $_POST['nhsnumber1'] . '-' . $_POST['nhsnumber2'] . '-' . $_POST['nhsnumber3'] . '<br />';

        $email_content .= '<br /><strong>Medical Records</strong><br />';
        $email_content .= 'Previous Address: ' . $_POST['previousaddress'] . '<br />';
        $email_content .= 'Previous Doctor: ' . $_POST['previousdoctor'] . '<br />';
        $email_content .= 'Previous Doctor Address: ' . $_POST['previousdoctoraddress'] . '<br />';

        $email_content .= '<br /><strong>Are you from abroad: '. $_POST['abroad'] .'</strong><br />';
        if($_POST['abroad'] == 'Yes'){
          $email_content .= 'Abroad First Address: ' . $_POST['abroadfirstaddress'] . '<br />';
          // $email_content .= 'Date Arrived: ' . $_POST['datearrived'] . '<br />';
          $email_content .= 'Date Arrived: ';
          if(!empty($_POST['datearrived_day']))
            $email_content .= $_POST['datearrived_day'] . '/' . $_POST['datearrived_month'] . '/' . $_POST['datearrived_year'] . '<br />';
        }

        $email_content .= '<br /><strong>If you are returning from the Armed Forces</strong><br/>';
        $email_content .= 'Address before enlisting: ' . $_POST['address_before_enlisting'] . '<br />';
        $email_content .= 'Service number: ' . $_POST['service_number'] . '<br />';
        // $email_content .= 'Enlistment Date: ' . $_POST['date_enlistment'] . '<br />';
        // $email_content .= 'Enlistment Date: ' . $_POST['leavingdate'] . '<br />';
        $email_content .= 'If previously resident in UK, date of leaving: ';
        if(!empty($_POST['leavingdate_day']))
          $email_content .= $_POST['leavingdate_day'] . '/' . $_POST['leavingdate_month'] . '/' . $_POST['leavingdate_year'];
        $email_content .= '<br/>Date you first came to live in UK: ';
        if(!empty($_POST['date_enlistment_day']))
          $email_content .= $_POST['date_enlistment_day'] . '/' . $_POST['date_enlistment_month'] . '/' . $_POST['date_enlistment_year'];

        $email_content .= '<br /><br /><strong>If you are registering a child under 5</strong><br />';
        if(isset($_POST['childregistered'])){
          $email_content .= 'Child Registered: ' . $_POST['childregistered'] . '<br />';
        }

        $email_content .= '<br /><strong>If you need doctor to dispense medicines and appliances</strong><br/>';
        $email_content .= 'I live more than 1 mile in a straight line from the nearest chemist: ';
        if(isset($_POST['chemist_more_than_mile'])){
          $email_content .= $_POST['chemist_more_than_mile'];
        }
        $email_content .= '<br/>I would have serious difficulty in getting them from a chemist: ';
        if(isset($_POST['chemist_difficulty'])){
          $email_content .= $_POST['chemist_difficulty'];
        }

        $email_content .= '<br/><br /><strong>NHS Organ Donor registration</strong><br/>';
        if(isset($_POST['organ_donor_any_organs'])){
          $email_content .= ' - Any of my organs and tissue<br />';
        }
        if(isset($_POST['organ_donor_kidneys'])){
          $email_content .= ' - Kidneys<br />';
        }
        if(isset($_POST['organ_donor_heart'])){
          $email_content .= ' - Heart<br />';
        }
        if(isset($_POST['organ_donor_liver'])){
          $email_content .= ' - Livers<br />';
        }
        if(isset($_POST['organ_donor_corneas'])){
          $email_content .= '- Corneas<br />';
        }
        if(isset($_POST['organ_donor_lungs'])){
          $email_content .= ' - Lungs<br />';
        }
        if(isset($_POST['organ_donor_pancreas'])){
          $email_content .= ' - Pancreas<br />';
        }
        if(isset($_POST['organ_donor_any_part'])){
          $email_content .= ' - Any part<br />';
        }

        $email_content .= '<br /><strong>NHS Blood Donor registration</strong><br/>';
        $email_content .= 'I would like to join the NHS Blood Donor Register as someone who may be contacted and would be prepared to donate blood: ';
        if(isset($_POST['blood_donor_confirm'])){
          $email_content .= $_POST['blood_donor_confirm'];
        }
        $email_content .= '<br/>Has given blood in the last 3 years: ';
        if(isset($_POST['blood_donor_three_years'])){
          $email_content .= 'Yes<br />';
        }
        else{
          $email_content .= 'No<br />';
        }

        $email_content .= '<br /><strong>Next of Kin</strong><br/>';
        $email_content .= 'Name of Next of Kin: ' . $_POST['next_kin_name'] . '<br />';
        $email_content .= 'Relationship to you: ' . $_POST['next_kin_relationship'] . '<br />';
        $email_content .= 'Phone Number: ' . $_POST['next_kin_phone'] . '<br />';

        $email_content .= '<br /><strong>Children Details</strong><br/>';
        for($i = 0; $i < 5; $i++){
          if(isset($_POST['child_name_' . $i]) && !empty($_POST['child_name_' . $i])) {
            $email_content .= '<br/>Name: ' . $_POST['child_name_'.$i];
            if(isset($_POST['child_gender_' . $i]) && !empty($_POST['child_gender_'.$i])){
              $email_content .= ' Sex: ' . $_POST['child_gender_'.$i];
            }

            if((isset($_POST['child_dob_day_' . $i]) && !empty($_POST['child_dob_day_'.$i]))
              && (isset($_POST['child_dob_month_' . $i]) && !empty($_POST['child_dob_month_'.$i]))
              && (isset($_POST['child_dob_year_' . $i]) && !empty($_POST['child_dob_year_'.$i])))
            {
              $email_content .= ' Date of Birth: ' . $_POST['child_dob_day_' . $i];
              $email_content .= '/' . $_POST['child_dob_month_'.$i];
              $email_content .= '/' . $_POST['child_dob_year_'.$i];
            }
          } else {
            $email_content .= '<br>-';
          }
        }
        /*
        if(isset($_POST['child_name']) && count($_POST['child_name'])){
          foreach($_POST['child_name'] as $key => $value){
            if(empty($_POST['child_name'][$key])){
              $email_content .= '-';
              continue;
            }
            $email_content .= 'Name: ' . $_POST['child_name'][$key];
            $email_content .= ' Sex: ' . $_POST['child_gender'][$key];
            // $email_content .= ' Date of Birth:' . $_POST['child_dob'][$key] . '<br/>';
            $email_content .= ' Date of Birth: ' . $_POST['child_dob_day'][$key] . '/' . $_POST['child_dob_month'][$key] . '/' . $_POST['child_dob_year'][$key] . '<br />';
          }
        }
        */

        $email_content .= '<br /><br/><strong>For Children under 16</strong><br/>';
        $email_content .= 'Guardian (Who looks after you?): ';
        if(isset($_POST['under16_guardian'])){
          $email_content .= $_POST['under16_guardian'];
        }
        $email_content .= '<br/>School: ' . $_POST['under16_school'] . '<br />';

        $email_content .= '<br /><strong>Type of Accommodation</strong><br/>';
        $email_content .= 'Accomodation: ' . $_POST['accommodation'] . '<br />';

        $email_content .= '<br /><strong>Occupation</strong><br/>';
        $email_content .= 'Occupation: ' . $_POST['occupation'] . '<br />';

        $email_content .= '<br /><strong>Height and Weight</strong><br/>';
        $email_content .= 'Height: ' . $_POST['height'] . ' ' . $_POST['height_units'] . '<br />';
        $email_content .= 'Weight: ' . $_POST['weight'] . ' ' . $_POST['weight_units'] . '<br />';

        $email_content .= '<br /><strong>Carer Details</strong><br/>';
        $email_content .= 'Are you a carer?: ' . $_POST['is_carer'] . '<br />';
        if(isset($_POST['is_carer']) && $_POST['is_carer'] == 'Yes'){
          $email_content .= 'If Yes, who do you care for?: ' . $_POST['carer_for'] . '<br />';
        }
        $email_content .= 'Do you have a carer?: ' . $_POST['has_carer'] . '<br />';
        if(isset($_POST['has_carer']) && $_POST['has_carer'] == 'Yes'){
          $email_content .= 'If Yes, Carer\'s Name: ' . $_POST['carer_name'] . '<br />';
        }

        $email_content .= '<br /><strong>Ethnic Group</strong><br/>';
        if($_POST['ethnicity'] != 'other'){
          $email_content .= 'Ethnicity: ' . $_POST['ethnicity'] . '<br />';
        }
        else{
          $email_content .= 'Ethnicity: ' . $_POST['ethnicity_other_description'] . '<br/>';
        }

        $email_content .= '<br /><strong>General Questions</strong><br/>';
        $email_content .= 'First or main language: ' . $_POST['first_language'] . '<br />';
        $email_content .= 'Do you ever need an interpreter: ' . $_POST['interpreter_required'] . '<br />';

        $email_content .= '<br /><strong>Medical History</strong><br/>';
        $email_content .= 'Have you had any other Long Term Condition, Illness, Accident or Operation: <br />';
        for($i = 0; $i < 4; $i++){
          if(isset($_POST['history_condition_' . $i]) && !empty($_POST['history_condition_' . $i])) {
            $email_content .= '<br>Condition/Operation: ' . $_POST['history_condition_'.$i];

            if((isset($_POST['history_date_day_' . $i]) && !empty($_POST['history_date_day_'.$i]))
              && (isset($_POST['history_date_month_' . $i]) && !empty($_POST['history_date_month_'.$i]))
              && (isset($_POST['history_date_year_' . $i]) && !empty($_POST['history_date_year_'.$i])))
            {
              $email_content .= ' Date/Year: ' . $_POST['history_date_day_'.$i];
              $email_content .= '/' . $_POST['history_date_month_'.$i];
              $email_content .= '/' . $_POST['history_date_year_'.$i];
            }

            if(isset($_POST['history_hospital_' . $i]) && !empty($_POST['history_hospital_'.$i])){
              $email_content .= ' Hospital:' . $_POST['history_hospital_'.$i];
            }
          } else {
            $email_content .= '<br>-';
          }
        }
        /*
        if(isset($_POST['history_condition']) && count($_POST['history_condition'])){
          foreach($_POST['history_condition'] as $key => $value){
            if(empty($_POST['history_condition'][$key])){
              $email_content .= '-';
              continue;
            }
            $email_content .= 'Condition/Operation: ' . $_POST['history_condition'][$key];
            // $email_content .= ' Date/Year: ' . $_POST['history_date'][$key];
            $email_content .= ' Date/Year: ' . $_POST['history_date_day'][$key] . '/' . $_POST['history_date_month'][$key] . '/' . $_POST['history_date_year'][$key];
            $email_content .= ' Hospital:' . $_POST['history_hospital'][$key] . '<br/>';
          }
        }
        */

        $email_content .= '<br /><br /><strong>Allergies</strong><br/>';
        $email_content .= 'Are you allergic to any medication(such as Penicillin or Aspirin): ' . $_POST['known_allergies'] . '<br />';
        if(isset($_POST['allergy_details']) && $_POST['allergy_details'] == 'Yes'){
          $email_content .= 'Allergy details: ' . $_POST['allergy_details'] . '<br />';
        }

        $email_content .= '<br /><strong>Smoking</strong><br/>';
        $email_content .= 'Do you smoke now: ' . $_POST['current_smoker'] . '<br />';
        if(isset($_POST['current_smoker']) && $_POST['current_smoker'] == 'Yes'){
          $email_content .= 'If Yes, how many per day and what kind?: ' . $_POST['current_smoker_details'] . '<br />';
        }
        $email_content .= 'Did you smoke in the past: ' . $_POST['past_smoker'] . '<br />';
        if(isset($_POST['past_smoker']) && $_POST['past_smoker'] == "Yes"){
          $email_content .= 'If Yes, how long?: ' . $_POST['past_smoker_details'] . '<br />';
        }

        $email_content .= '<br /><strong>Alcohol</strong><br/>';
        $email_content .= 'How often do you have an alcoholic drink?: ' . $_POST['alcohol_frequency'] . '<br />';
        if(isset($_POST['alcohol_frequency']) && $_POST['alcohol_frequency'] != 'Never'){
          $email_content .= 'How many drinks/units per session?: ' . $_POST['alcohol_per_session'] . '<br />';
          $email_content .= 'How often do you have 6 or more standard drinks on one occasions?:' . $_POST['alcohol_max_per_session'] . '<br/>';
        }

        $email_content .= '<br /><strong>Your Family\'s Health</strong><br/>';
        $email_content .= 'Has anyone in your family had a heart attack or angina: ';
        if(isset($_POST['family_heart_attack_or_angina'])) {
          $email_content .= $_POST['family_heart_attack_or_angina'];
        }
        $email_content .= '<br />Family Member (If Yes, Please Tick):<br/>';
        if(isset($_POST['family_heart_attack_or_angina_mother'])){
          $email_content .= ' - Mother<br/>';
        }
        if(isset($_POST['family_heart_attack_or_angina_father'])){
          $email_content .= ' - Father<br/>';
        }
        if(isset($_POST['family_heart_attack_or_angina_sister'])){
          $email_content .= ' - Sister<br/>';
        }
        if(isset($_POST['family_heart_attack_or_angina_brother'])){
          $email_content .= ' - Brother<br/>';
        }
        if(isset($_POST['family_heart_attack_or_angina_other'])){
          $email_content .= ' - Other: ';
          if(isset($_POST['family_heart_attack_or_angina_other_description'])){
            $email_content .= $_POST['family_heart_attack_or_angina_other_description'].'<br/>';
          }
        }
        $email_content .= '<br />Have members of your family suffered from any other health problems:';
        if(isset($_POST['family_other_health_problems'])) {
          $email_content .= $_POST['family_other_health_problems'];
        }
        if(isset($_POST['family_other_health_problems']) && $_POST['family_other_health_problems'] == 'Yes'){
          $email_content .= '<br/>If Yes, please specify:';
          if(isset($_POST['family_other_health_problems_member'])){
            $email_content .= $_POST['family_other_health_problems_member'];
          }
        }

        $email_content .= '<br/><br/><strong>Where did you hear about us</strong><br/>';
        $email_content .= $_POST['hear_about'] . '<br />';

        $email_content .= '<br /><strong>Final Declaration</strong><br/>';
        if(isset($_POST['confirm_accuracy'])){
          $email_content .= 'I acknowledge that all the information provided is accurate to the<br/>best of my knowledge. By ticking this option, I take responsibility<br/>and consent to the practice registering me as a regular patient<br />';
        }

        $email_content .= '<br/>';

        if(is_uploaded_file($_FILES['proof_of_address']['tmp_name']) &&
           is_uploaded_file($_FILES['proof_of_id']['tmp_name'])) {
          $email_content .= '<br/>Find attached proof of address and ID<br/>';
        }
        if(is_uploaded_file($_FILES['proof_of_address']['tmp_name'])){
          $file_name_1 = time() . $_FILES['proof_of_address']['name'];
          $email_content .= 'Proof of Address: ' . $file_name_1 . '<br/>';
        }
        if(is_uploaded_file($_FILES['proof_of_id']['tmp_name'])){
          $file_name_2 = time() . $_FILES['proof_of_id']['name'];
          $email_content .= 'Proof of ID: ' . $file_name_2 . '<br/>';
        }

        $email_content .= '<br/><br/>';
        $email_content .= '--';

        $this->email->from('no-reply@nhs.net');
        $this->email->to("queenscrescentsurgery@nhs.net");
        //$this->email->to('mohamed@boilerhouse.digital');

        $this->email->subject($email_title);
        $this->email->message($email_content);

        // attachments
        // move the files to a temporary location

        if(is_uploaded_file($_FILES['proof_of_address']['tmp_name'])){
          $file_name_1 = './storage/files/' . $file_name_1;
          move_uploaded_file($_FILES['proof_of_address']['tmp_name'], $file_name_1);
          $this->email->attach($file_name_1);
        }

        if(is_uploaded_file($_FILES['proof_of_id']['tmp_name'])){
          $file_name_2 = './storage/files/' . $file_name_2;
          move_uploaded_file($_FILES['proof_of_id']['tmp_name'], $file_name_2);
          $this->email->attach($file_name_2);
        }

        // echo $email_content;
        // exit;

        $this->email->send();

        // echo $this->email->print_debugger();
        // exit;

        if(isset($_FILES['proof_of_address'])){
          // remove the temporary files
          if(file_exists($file_name_1)){
            unlink($file_name_1);
          }
        }
        if(isset($_FILES['proof_of_id'])){
          if(file_exists($file_name_2)){
            unlink($file_name_2);
          }
        }

				$data['submitted'] = TRUE;
			}

		}

		$url_title = $this->uri->segment(2);

		if (!empty($url_title)) {
			$data['content'] = $this->patient_services_model->get_page_by_url_title($url_title);
		}

		$data['sidenav'] = $this->patient_services_model->get_sidenav();
		$data['page_name'] = $data['content']->title;

		$this->load->view('patient_services/register', $data);
	}

}
