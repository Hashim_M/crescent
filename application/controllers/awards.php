<?php

class Awards extends Controller {

	function __construct() {
		
		parent::__construct();
		
		$this->load->model('awards_model');	
	}
	
	function index() {
		
		$data['awards'] = $this->awards_model->get_awards_content();

		$this->load->view('awards/index', $data);
	}
}