<?php

class Staff extends Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('staff_model');	
	}
	
	function index() {
	
		$url_title = $this->uri->segment(2);
		
		if (!empty($url_title)) {			
			$data['content'] = $this->staff_model->get_page_by_url_title($url_title);
		} else {
			$data['content'] = $this->staff_model->get_index_page();
		}
		
		$data['sidenav'] = $this->staff_model->get_sidenav();
					
		$this->load->view('staff/page', $data);
	}
	
	/*function index()
	{
		redirect('staff/doctors');
	}
	
	function doctors()
	{
		$this->load->view('staff/doctors');
	}	

	function specialist_training_doctors()
	{
		$this->load->view('staff/specialist_training_doctors');
	}	
	
	function practice_nurses()
	{
		$this->load->view('staff/practice_nurses');
	}	
	
	function district_nurses()
	{
		$this->load->view('staff/district_nurses');
	}	

	function health_visitors()
	{
		$this->load->view('staff/health_visitors');
	}	

	function james_wigg_team()
	{
		$this->load->view('staff/james_wigg_team');
	}*/	
	

}