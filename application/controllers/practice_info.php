<?php

class Practice_info extends Controller {

	function __construct()
	{
		parent::__construct();	
		
		$this->load->model('practice_info_model');
		$this->load->model('banners_model');
	}
	
	function index() {
	
		$url_title = $this->uri->segment(2);
		
		if (!empty($url_title))
		{			
			$data['content'] = $this->practice_info_model->get_page_by_url_title($url_title);
		}
		else
		{
			$data['content'] = $this->practice_info_model->get_index_page();
		}
		
		$banner_ids 		= explode(',', $data['content']->banners);		
		$data['banners'] 	= $this->banners_model->get_banners_by_ids($banner_ids);		
		
		
		$data['sidenav'] 	= $this->practice_info_model->get_sidenav();
					
		$this->load->view('practice_info/page', $data);
	}

}