<?php

class Home extends Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->Model('news_model');
		$this->load->Model('slider_model');
		$this->load->Model('contact_model');	
		
		$this->load->helper('text');
	}
	
	function index()
	{
		$data['extra_head_content'] = '<script src="/static/js/twitter.js" type="text/javascript"></script>';
		//Get the latest 3 news items for the homepage?
		$data['news_items'] = $this->news_model->get_latest_items(2);
		
		$this->load->Model('slider_model');
		$data['slides'] = $this->slider_model->get_slides();
		
		$data['contact_home'] = $this->contact_model->get_contact_home();
		$this->load->view('home/index', $data);
	}
}