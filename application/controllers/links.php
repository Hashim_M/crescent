<?php

class Links extends Controller {

	function __construct() {
		parent::__construct();
		$this->load->Model('links_model');	
	}
	
	function index() {
		$data['links'] = $this->links_model->get_links();
		$this->load->view('links/index', $data);
	}
}