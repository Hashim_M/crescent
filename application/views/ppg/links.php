<?=$this->load->view('assets/header')?>
	<div id="content">	
		<div id="sidebar">		
			<?=$this->load->view('assets/sidemenu/ppg_pages')?>			
		</div>
		<div id="main">			
			<h1>Patient Participation Group Links</h1>
			
			<?php if($links) : ?>
			
				<?php foreach($links as $link) : ?>
			
					<h3><a href="<?=$link->hyperlink;?>" title="<?=$link->title;?>"><?=$link->title;?></a></h3>
					
					<p><?=$link->description;?></p>
				
				<?php endforeach; ?>
			
			<?php else : ?>
			
				<p>Sorry, no links to display</p>
				
			<?php endif; ?>
			
		</div>				
	</div>
<?=$this->load->view('assets/footer')?>