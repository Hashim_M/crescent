<?=$this->load->view('assets/header')?>
	<div id="content">	
		<div id="sidebar">		
			<?=$this->load->view('assets/sidemenu/ppg_pages')?>			
		</div>
		<div id="main">			
			<h1>Members</h1>	
			
			<?php if($logged_in) : ?>
			
				<p><?=$content->body;?></p>
			
			<?php else : ?>
				
				<?php if(isset($error)) : ?>
					<div id="error"><p><?=$error;?></p></div>
				<?php endif;?>
				
				<form action="/ppg/members" method="post">
					<p><label>Access Code: <br />
					<input type="text" class="text" name="access_code" value="" /></label></p>
					<p><input type="submit" value="Enter" /></p>
				</form>
				
			<?php endif; ?>
			
		</div>				
	</div>
<?=$this->load->view('assets/footer')?>