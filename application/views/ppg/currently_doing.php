<?=$this->load->view('assets/header')?>
	<div id="content">	
		<div id="sidebar">			
			<?=$this->load->view('assets/sidemenu/ppg_pages')?>			
		</div>
		<div id="main">			
			<h1>Patient Participation Group - Minutes</h1>	
			
			<?php if ($currently_doing) : ?>
				
				<?php foreach ($currently_doing as $news_item) : ?>
					<div class="item" style="width: 100%;">
						<div class="date">
							<h3><span class="day"><?=date("jS", strtotime($news_item->published));?></span><br /> <?=date("M", strtotime($news_item->published));?> '<?=date("y", strtotime($news_item->published));?></h3>
						</div>
						<div class="info" style="width: 550px;">
							<h3><a href="/ppg/minutes/<?=str_replace("-", "/", $news_item->published);?>/<?=$news_item->url_title;?>"><?=$news_item->title;?></a></h3>
							<p><?=$news_item->summary;?></p>
						</div>
					</div>
				<?php endforeach; ?>
				
			<?php else : ?>
				
				<p>Sorry, <em>no</em> items to show</p>
			
			<?php endif; ?>
    							
		</div>				
	</div>
<?=$this->load->view('assets/footer')?>