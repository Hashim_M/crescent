<?=$this->load->view('assets/header');?>
	<div id="content">	
		<div id="sidebar">		
			<?=$this->load->view('assets/sidemenu/ppg_pages')?>		
		</div>
		<div id="main">
				
			<h1><?=$news_item->title;?></h1>
			
				<div class="date">
					<h2><?=date("jS", strtotime($news_item->published));?> <?=date("M", strtotime($news_item->published));?> '<?=date("y", strtotime($news_item->published));?></h2>
				</div>
				
				<h3><?=$news_item->summary;?></h3>
				<p><?=$news_item->body;?></p>
				
		</div>
	</div>
<?=$this->load->view('assets/footer')?>