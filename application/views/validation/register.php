<?php
// Patient details
$rules['prefix']                        = "required";
$rules['surname']                       = "required";
$rules['firstname']                     = "required";
$rules['country_of_birth']              = "required";

$rules['born_in_london']                = "required";
if($this->input->post('born_in_london') && $this->input->post('born_in_london') == 'Yes'){
  $rules['london_borough']        = "required";
}

$rules['address']                       = "required";
$rules['postcode']                      = "required";
$rules['number']                        = "required";
$rules['email']                         = "required|valid_email";
$rules['previousaddress']               = "required";
$rules['previousdoctor']                = "required";
$rules['previousdoctoraddress']         = "required";
$rules['dob_day']                       = "required";
$rules['dob_month']                     = "required";
$rules['dob_year']                      = "required";
$rules['gender']                        = "required";
$rules['height']                        = "required";
$rules['weight']                        = "required";
$rules['abroad']                        = "required";
if($this->input->post('abroad') && $this->input->post('abroad') == 'Yes'){
  $rules['datearrived_day']               = "required";
  $rules['datearrived_month']             = "required";
  $rules['datearrived_year']              = "required";
}
$rules['next_kin_name']                 = "required";
$rules['next_kin_relationship']         = "required";
$rules['next_kin_phone']                = "required";
$rules['accommodation']                 = "required";
$rules['occupation']                    = "required";

$rules['is_carer']                      = "required";
if($this->input->post('is_carer') && $this->input->post('is_carer') == 'Yes'){
  $rules['carer_for']        = "required";
}

$rules['has_carer']                     = "required";
if($this->input->post('has_carer') && $this->input->post('has_carer') == 'Yes'){
  $rules['carer_name']        = "required";
}

$rules['ethnicity']                     = "required";
if($this->input->post('ethnicity') && $this->input->post('ethnicity') == 'other'){
  $rules['ethnicity_other_description']        = "required";
}

$rules['first_language']                = "required";
$rules['interpreter_required']          = "required";

$rules['known_allergies']               = "required";
if($this->input->post('known_allergies') && $this->input->post('known_allergies') == 'Yes'){
  $rules['allergy_details']        = "required";
}

$rules['current_smoker']                = "required";
if($this->input->post('current_smoker') && $this->input->post('current_smoker') == 'Yes'){
  $rules['current_smoker_details']        = "required";
}

$rules['past_smoker']                   = "required";
if($this->input->post('past_smoker') && $this->input->post('past_smoker') == 'Yes'){
  $rules['past_smoker_details']           = "required";
}

$rules['alcohol_frequency']             = "required";
if($this->input->post('alcohol_frequency') && $this->input->post('alcohol_frequency') != 'Never'){
  $rules['alcohol_per_session']           = "required";
  $rules['alcohol_max_per_session']       = "required";
}
$rules['confirm_accuracy']              = "required";

$rules['family_heart_attack_or_angina'] = "required";
if($this->input->post('family_heart_attack_or_angina')
  && $this->input->post('family_heart_attack_or_angina') == 'Yes'
  && !$this->input->post('family_heart_attack_or_angina_father')
  && !$this->input->post('family_heart_attack_or_angina_brother')
  && !$this->input->post('family_heart_attack_or_angina_sister')
  && !$this->input->post('family_heart_attack_or_angina_other')){
  $rules['family_heart_attack_or_angina_mother']  = "required";
}
if($this->input->post('family_heart_attack_or_angina')
  && $this->input->post('family_heart_attack_or_angina') == 'Yes'
  && !$this->input->post('family_heart_attack_or_angina_mother')
  && !$this->input->post('family_heart_attack_or_angina_brother')
  && !$this->input->post('family_heart_attack_or_angina_sister')
  && !$this->input->post('family_heart_attack_or_angina_other')){
  $rules['family_heart_attack_or_angina_father']  = "required";
}
if($this->input->post('family_heart_attack_or_angina')
  && $this->input->post('family_heart_attack_or_angina') == 'Yes'
  && !$this->input->post('family_heart_attack_or_angina_father')
  && !$this->input->post('family_heart_attack_or_angina_mother')
  && !$this->input->post('family_heart_attack_or_angina_sister')
  && !$this->input->post('family_heart_attack_or_angina_other')){
  $rules['family_heart_attack_or_angina_brother']  = "required";
}
if($this->input->post('family_heart_attack_or_angina')
  && $this->input->post('family_heart_attack_or_angina') == 'Yes'
  && !$this->input->post('family_heart_attack_or_angina_father')
  && !$this->input->post('family_heart_attack_or_angina_brother')
  && !$this->input->post('family_heart_attack_or_angina_mother')
  && !$this->input->post('family_heart_attack_or_angina_other')){
  $rules['family_heart_attack_or_angina_sister']  = "required";
}
if($this->input->post('family_heart_attack_or_angina')
  && $this->input->post('family_heart_attack_or_angina') == 'Yes'
  && !$this->input->post('family_heart_attack_or_angina_father')
  && !$this->input->post('family_heart_attack_or_angina_brother')
  && !$this->input->post('family_heart_attack_or_angina_sister')
  && !$this->input->post('family_heart_attack_or_angina_mother')){
  $rules['family_heart_attack_or_angina_other']  = "required";
}
if($this->input->post('family_heart_attack_or_angina')
  && $this->input->post('family_heart_attack_or_angina') == 'Yes'
  && $this->input->post('family_heart_attack_or_angina_other')){
  $rules['family_heart_attack_or_angina_other_description'] = "required";
}

$rules['family_other_health_problems']  = "required";
if($this->input->post('family_other_health_problems') && $this->input->post('family_other_health_problems') == 'Yes'){
  $rules['family_other_health_problems_member'] = "required";
}

if (isset($_FILES['proof_of_address']) && empty($_FILES['proof_of_address']['name'])) {
  // $rules['proof_of_address'] = 'required';
}
if (isset($_FILES['proof_of_id']) && empty($_FILES['proof_of_id']['name'])) {
  // $rules['proof_of_id'] = 'required';
}

$this->validation->set_rules($rules);

$fields['prefix']                                          = 'Title';
$fields['surname']                                         = 'Surname';
$fields['firstname']                                       = 'First Names';
$fields['country_of_birth']                                = 'Country of birth';
$fields['born_in_london']                                  = 'Where you born in London';
$fields['london_borough']                                  = 'Borough';
$fields['address']                                         = 'Current home address';
$fields['postcode']                                        = "Postcode";
$fields['number']                                          = "Telephone Number";
$fields['email']                                           = "Email ID";
$fields['previousaddress']                                 = "Your previous address in UK";
$fields['previousdoctor']                                  = "Name of previous doctor while at that address";
$fields['previousdoctoraddress']                           = "Address of previous doctor";
$fields['gender']                                          = "Gender";
$fields['height']                                          = "How tall are you";
$fields['weight']                                          = "How much do you weigh";
$fields['dob_day']                                         = "Date of Birth day";
$fields['dob_month']                                       = "Date of Birth month";
$fields['dob_year']                                        = "Date of Birth year";
$fields['abroad']                                          = "From abroad";
$fields['datearrived_day']                                 = "Date you first came to live in UK day";
$fields['datearrived_month']                               = "Date you first came to live in UK month";
$fields['datearrived_year']                                = "Date you first came to live in UK year";
$fields['next_kin_name']                                   = "Name of Next of Kin";
$fields['next_kin_relationship']                           = "Relationship to you";
$fields['next_kin_phone']                                  = "Phone number";
$fields['accommodation']                                   = "Type of Accommodation";
$fields['occupation']                                      = "Occupation";
$fields['is_carer']                                        = "Are you a carer?";
$fields['carer_for']                                       = "'who do you care for?'";
$fields['has_carer']                                       = "Do you have a carer?";
$fields['carer_name']                                      = "'Carer's Name'";
$fields['ethnicity']                                       = "Ethnicity";
$fields['ethnicity_other_description']                     = "Ethnicity description";
$fields['first_language']                                  = "What is the first or main language that you speak";
$fields['interpreter_required']                            = "Interpreter required";
$fields['known_allergies']                                 = "Known Allergies";
$fields['allergy_details']                                 = "'please give details'";
$fields['current_smoker']                                  = "Do you smoke now";
$fields['current_smoker_details']                          = "'how many per day and what kind?'";
$fields['past_smoker']                                     = "Did you smoke in the past";
$fields['past_smoker_details']                             = "'How long'";
$fields['alcohol_frequency']                               = "How often do you have an alcoholic drink?";
$fields['alcohol_per_session']                             = "How many drinks/units per session?";
$fields['alcohol_max_per_session']                         = "How many drinks/units per session?";
// $fields['proof_of_address']                             = "Proof of Address";
// $fields['proof_of_id']                                  = "Proof of ID";
$fields['confirm_accuracy']                                = "final declaration";
$fields['family_heart_attack_or_angina']                   = "Has anyone in your family had a heart attack or angina";
$fields['family_heart_attack_or_angina_mother']            = "'Please Tick*'";
$fields['family_heart_attack_or_angina_father']            = "'Please Tick*'";
$fields['family_heart_attack_or_angina_sister']            = "'Please Tick*'";
$fields['family_heart_attack_or_angina_brother']           = "'Please Tick*'";
$fields['family_heart_attack_or_angina_other']             = "'Please Tick*'";
$fields['family_heart_attack_or_angina_other_description'] = "'please specify'";
$fields['family_other_health_problems']                    = "Have members of your family suffered from any other health problems";
$fields['family_other_health_problems_member']             = "'please specify'";

$this->validation->set_fields($fields);

$this->validation->set_error_delimiters('<div class="error">', '</div>');
