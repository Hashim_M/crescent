<?php

$rules['slide_title'] 		    = "required";	
$rules['slide_content'] 			= "required";
$rules['slide_image'] 		= "";
$rules['slide_position'] 		= "required";

$this->validation->set_rules($rules);	

$fields['slide_title'] 		= 'Slide title';
$fields['slide_content'] 			= 'Slide text';
$fields['slide_image'] 			= 'Slide image';
$fields['slide_position'] 		= "Slide position";

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');