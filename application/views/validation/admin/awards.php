<?php

$rules['body'] 		    = "required";	
$rules['body_right'] 		= "required";

$this->validation->set_rules($rules);	

$fields['body'] 		= 'Body';
$fields['body_right'] 	= 'body_right';

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');