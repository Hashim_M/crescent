<?php

$rules['ppg_home'] 			= "required";
$rules['ppg_password'] 		= "required";

$this->validation->set_rules($rules);	

$fields['ppg_home'] 		= 'PPG Home';
$fields['ppg_password'] 	= 'PPG Password';

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');