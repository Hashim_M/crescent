<?php

$rules['title'] 			= "required";	
$rules['body'] 				= "required";	
$rules['url_title'] 		= "alpha_dash";

$this->validation->set_rules($rules);	

$fields['title'] 			= 'Title';
$fields['body'] 			= 'Body';
$fields['url_title'] 		= "URL Title";

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');