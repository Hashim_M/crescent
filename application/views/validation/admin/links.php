<?php

$rules['title'] 				= "required";	
$rules['description'] 			= "required";
$rules['hyperlink']     		= "required";	

$rules['thumbnail_file'] 		= "";
$rules['thumbnail_filepath'] 	= "";


$this->validation->set_rules($rules);	

$fields['title'] 				= 'Title';
$fields['description'] 			= 'Summary';
$fields['hyperlink'] 			= 'Body';

$fields['thumbnail_file'] 		= "Thumbnail";
$fields['thumbnail_filepath'] 	= "Thumbnail";

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');