<?php

$rules['location'] 		    = "required";	
$rules['contact'] 			= "required";
$rules['contact_home'] 		= "required";

$this->validation->set_rules($rules);	

$fields['location'] 		= 'Location';
$fields['contact'] 			= 'Contact';
$fields['contact_home'] 			= 'Contact Home';

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');