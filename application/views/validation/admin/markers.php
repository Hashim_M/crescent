<?php

$rules['title'] 		= "required|min_length[2]|max_length[200]";	 

$rules['address'] 		= ""; 
$rules['contact'] 		= ""; 
$rules['website1'] 		= ""; 
$rules['website2'] 		= ""; 
$rules['lat']			= "numeric";
$rules['lon']			= "numeric";
$rules['body'] 			= ""; 

$this->validation->set_rules($rules);

$fields['title'] 		= 'Title';

$fields['address'] 		= "Address"; 
$fields['contact'] 		= "Contact"; 
$fields['website1'] 	= "Link to PCT home page"; 
$fields['website2'] 	= "Link to PCT Equality and Diversity page"; 
$fields['lat']			= "Latitude";
$fields['lon']			= "Longitude";

$this->validation->set_fields($fields);

$this->validation->set_error_delimiters('<div class="error">', '</div>');