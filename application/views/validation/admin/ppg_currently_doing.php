<?php

$rules['title'] 		= "required";	
$rules['summary'] 		= "required";
$rules['body'] 		= "required";	
$rules['url_title'] 	= "alpha_dash";
$rules['post_status'] 	= 'required';

$this->validation->set_rules($rules);	

$fields['title'] 		= 'Title';
$fields['summary'] 		= 'Summary';
$fields['body'] 		= 'Body';
$fields['post_status'] 	= 'Post Status';
$fields['url_title'] 	= "URL Title";

$this->validation->set_fields($fields);
$this->validation->set_error_delimiters('<div class="error">', '</div>');