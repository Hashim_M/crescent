<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
		
			<h1>Links</h1>
			
			<p>Create a new link to be displayed.</p>	
		
			<form action="/admin/links/create/" method="post">					
				<p><input type="submit" value="Create a new links" /></p>					
			</form>
				
			<?php if ($links->num_rows() > 0) : ?>
				
				<table width="100%">		
					<tr>
						<td>Title</td>
						<td>URL</td>

						<td>Remove</td>
					</tr>		
					<?php foreach ($links->result() as $link) : ?>
					
						<tr>
							
							<td><a href="/admin/links/update/<?=$link->id;?>"><?=$link->title;?></a></td>
							
							<td><?=$link->hyperlink;?></td>					
										
							<td><a href="/admin/links/delete/<?=$link->id;?>" title="Delete Link" onclick="return confirmDelete()">Delete</a></td>
						</tr>
					<?php endforeach; ?>
				</table>
				
			<?php else : ?>
				
				<p>No <em>links</em> found.</p>
				
			<?php endif; ?>
		
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>