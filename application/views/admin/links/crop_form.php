<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-us" />
<script src="/cropper/lib/prototype.js" type="text/javascript"></script>	
<script src="/cropper/lib/scriptaculous.js?load=builder,dragdrop" type="text/javascript"></script>
<script src="/cropper/cropper.js" type="text/javascript"></script>
<script type="text/javascript">
    
    function onEndCrop( coords, dimensions ) {
    	$( 'x1' ).value = coords.x1;
    	$( 'y1' ).value = coords.y1;
    	$( 'x2' ).value = coords.x2;
    	$( 'y2' ).value = coords.y2;
    	$( 'width' ).value = dimensions.width;
    	$( 'height' ).value = dimensions.height;
    }
    
    // example with a preview of crop results, must have minimumm dimensions
    Event.observe( 
    	window, 
    	'load', 
    	function() { 
    		new Cropper.ImgWithPreview( 
    			'testImage',
    			{ 
    				minWidth: 255, 
    				minHeight: 170,
    				ratioDim: { x: 255, y: 170 },
    				displayOnInit: true, 
    				onEndCrop: onEndCrop,
    				previewWrap: 'previewArea'
    			} 
    		) 
    	} 
    );

</script>
<style type="text/css">    
#testWrap {
	width: 645px;
	height: 500px;
	overflow: scroll;
	float: left;
	margin: 10px 0 0 10px;
}

#previewArea {
	margin: 10px; 0 0 10px;
	float: left;
}

</style>
</head>
<body>
	<div id="testWrap">
		<img src="/tmp/<?=$upload_data['file_name'] . '?' . date('U')?>" alt="test image" id="testImage" />
	</div>	
	<div id="previewArea"></div>	
	<div id="results">
		<form action="/admin/links/crop/" method="post">			
    	<input type="hidden" name="imagepath" value="<?=$upload_data['file_name']?>" />			
    	<input type="hidden" name="x1" id="x1" />
    	<input type="hidden" name="y1" id="y1" />
    	<input type="hidden" name="x2" id="x2" />
    	<input type="hidden" name="y2" id="y2" />
    	<input type="hidden" name="width" id="width" />
    	<input type="hidden" name="height" id="height" />			
    	<p><input type="submit" value="crop" class="cropbutton" /></p>
    	</form>
	</div>	
</body>
</html>