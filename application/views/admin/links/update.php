<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
			<h1>Update Link</h1>
			<form action="/admin/links/update/" method="post">	
				<?=$this->load->view('assets/admin/forms/links');?>							
				<input type="hidden" name="id" value="<?=$id?>" />
				<p><input type="submit" value="Save changes" /></p>					
			</form>	
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>