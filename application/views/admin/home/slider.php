<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>Editing slider</h1>
				<form action="/admin/home/edit_slider/<?=$slide->slide_id;?>" method="post" enctype="multipart/form-data">	
					<?=$this->load->view('assets/admin/forms/slider')?>							
					<p><input type="submit" value="Save" /></p>					
				</form>	
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>