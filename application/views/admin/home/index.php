<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>Home Page</h1>
				
				<h2>Home page slider</h2>
				
				<table style="width: 100%;">
					<thead>
						<tr>
							<th>
								Slide title
							</th>
							<th style="width: 150px;">
								Options
							</th>
						</tr>
					</thead>
					<?php foreach($slides as $slide):?>
						<tr>
							<td>
								<a href="/admin/home/edit_slider/<?=$slide->slide_id;?>"><?=$slide->slide_title;?>
							</td>
							<td>
								edit
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
				
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>