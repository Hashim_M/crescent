<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Queens Crescent Practice</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body {
	text-align: center;
	font-size: 13px;
	font-family: Arial, Sans-serif;
	background-color: #F9F9F9;
	color: #333;
}
#wrapper {
	width: 212px;
	background: #FFF;
	padding: 0 10px;
	border: 4px solid #EEE;
	margin: 70px auto 0;
	text-align: left;
}
label {
	color: #999;
}
.loginusername, .loginpassword {
	border: 2px solid #EEE;
	padding: 7px 5px;
	width: 200px;
	background: #F9F9F9;
	font-size: 13px;
	font-family: Arial, Sans-serif;	
}
input {
	font-size: 14px;
	font-family: Arial, Sans-serif;	
}
h2 {
	margin-top: 10px;
	font-size: 22px;
	font-family: Arial, Sans-serif;
	line-height: 26px;
	margin-bottom: 10px;
	font-weight: normal;
	color: #115B2F;
}

a {
	color: #666; /*#0075C0;*/
}

</style>
</head>
<body>
	<p><a href="/">&lt; Return to website</a></p>
	<div id="wrapper">
		<h2>CMS Login</h2>                   
		<?=$error?>
		<form action="/admin/login" method="post">
			<?=$this->validation->username_error?>
			<p><label>Username: <br /><input class="loginusername" type="text" name="username" value="<?=$this->validation->username?>" /></label></p>
			<?=$this->validation->password_error?>
			<p><label>Password: <br /><input class="loginpassword" type="passsword" name="password" value="" /></label></p>
			<p><input type="submit" value="Submit" /></p>
		</form>    
    </div>
</body>
</html>