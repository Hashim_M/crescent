<?='<?xml version="1.0" encoding="utf-8" ?>'?>
<rss version="2.0">
	<channel>
		<title>Queens Crescent Practice</title>
		<link><?=base_url();?></link>
		<description>News</description>
		<language>en</language>
		<copyright>Copyright <?=date('Y');?>. All rights reserved.</copyright>
		<pubDate><?=date('r');?></pubDate>
		<category>UK</category>
		<docs>http://blogs.law.harvard.edu/tech/rss</docs>
        <?php 
		if ($items->num_rows() > 0) :
			foreach($items->result() as $item) :
		?>
		<item>
			<title><?=xml_convert($item->title);?></title>
			<link><?=base_url()?>news/<?=$item->url_date;?>/<?=$item->url_title;?></link>
			<description><![CDATA[<?=$item->summary;?>]]></description>
			<pubDate></pubDate>
		</item>
		<?php
			endforeach;
		endif;
		?>
</channel>
</rss>