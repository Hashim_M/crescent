<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
		
			<h1>News</h1>
		
		<form action="/admin/news/create/" method="post">					
			<p><input type="submit" value="Create a new news article" /></p>					
		</form>
		
		<!-- Draft Items -->
		
		<h2>Draft Items</h2>
				
		<?php if ($draft_news->num_rows() > 0) : ?>
				
			<table width="100%">		
				<tr>
					<td>Title</td>
					<td>Published</td>
					<td>Modified</td>
					<td>Actions</td>
				</tr>
				<?php foreach ($draft_news->result() as $news_item) : ?>
					
					<tr>
						<td><a href="/admin/news/update/<?=$news_item->id;?>"><?=$news_item->title;?></a></td>
						<td><?=date('d F Y' , strtotime($news_item->published));?></td>
						<?php if($news_item->modified != '0000-00-00 00:00:00') : ?>
							<td><?=date('d F Y' , strtotime($news_item->modified));?></td>
						<?php else : ?>
							<td>N/A</td>
						<? endif; ?>
						<td><a href="/admin/news/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/news/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a></td>
						</tr>	
				<?php endforeach; ?>
			</table>
				
		<?php else : ?>
				
			<p>No <em>draft</em> articles found.</p>
				
		<?php endif; ?>
		
		<!-- Published Items -->	
		
		<h2>Published Items</h2>
				
		<?php if ($publish_news->num_rows() > 0) : ?>
				
			<ul id="items" class="sortable-list">
				<?php foreach ($publish_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1"><a href="/admin/staff/update/<?=$news_item->id?>"><?=$news_item->title?></a> - <?=date('d F Y' , strtotime($news_item->published));?></div>
							<div class="col2">
								<span class="manage"><a href="/admin/news/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a></span>
								<span class="del"><a href="/admin/news/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a></span>
							</div>
						</div>
						
					</li>
				<?php endforeach; ?>
			
			</ul>
		
				<script type="text/javascript">
				
				function updateOrder() {
					var options = {
						method : 'post',
						parameters : Sortable.serialize('items')
					};
					
					new Ajax.Request('/admin/news/order', options);
				}
								
				document.observe("dom:loaded", function() {
				
					Sortable.create('items', { onUpdate : updateOrder });
				});
										
				</script>
		<?php else : ?>
				
			<p>No <em>published</em> news articles found.</p>
				
		<?php endif; ?>
		
		<!-- Pending Items -->	
		
		<h2>Pending Items</h2>
				
		<?php if ($pending_news->num_rows() > 0) : ?>
				
			<table width="100%">		
				<tr>
					<td>Title</td>
					<td>Published</td>
					<td>Modified</td>
					<td>Actions</td>
				</tr>		
				<?php foreach ($pending_news->result() as $news_item) : ?>
					
					<tr>
						<td><a href="/admin/news/update/<?=$news_item->id;?>"><?=$news_item->title;?></a></td>
						<td><?=date('d F Y' , strtotime($news_item->published));?></td>
						<?php if($news_item->modified != '0000-00-00 00:00:00') : ?>
							<td><?=date('d F Y' , strtotime($news_item->modified));?></td>
						<?php else : ?>
							<td>N/A</td>
						<? endif; ?>
						<td><a href="/admin/news/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/news/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a></td>
					</tr>						
				
				<?php endforeach; ?>
			
			</table>
				
		<?php else : ?>
				
			<p>No <em>pending</em> news articles found.</p>
				
		<?php endif; ?>
				
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>