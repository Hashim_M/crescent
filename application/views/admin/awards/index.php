<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		<div id="main">			
			<h1>Awards Page</h1>
			<p>Update the content that is displayed on the awards page. Once you have clicked <strong>save</strong> the changes will be instantly displayed on the website.</p>
			<form action="/admin/awards/" method="post">	
				<?=$this->load->view('assets/admin/forms/awards')?>							
				<p><input type="submit" value="Save" /></p>					
			</form>	
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>