<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/ppg/" class="active">PPG Home page</a></li>
					<li><a href="/admin/ppg_pages/" title="PPG pages">PPG Pages</a></li>
					<li><a href="/admin/ppg/links">Links</a></li>	
					<li><a href="/admin/ppg/minutes">Minutes</a></li>	
					<li><a href="/admin/ppg/members_page">Members page</a></li>		
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		<div id="main">			
			<h1>PPG</h1>
			<p>Update the content that is displayed on the contact pages. Once you have clicked <strong>save</strong> the changes will be instantly displayed on the website.</p>
			
			<form action="/admin/ppg/" method="post">	
				<?=$this->load->view('assets/admin/forms/ppg')?>							
				<p><input type="submit" value="Save" /></p>					
			</form>
				
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>