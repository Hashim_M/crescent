<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/ppg/">PPG Home page</a></li>
					<li><a href="/admin/ppg_pages/" title="PPG pages">PPG Pages</a></li>
					<li><a href="/admin/ppg/links" class="active">Links</a></li>	
					<li><a href="/admin/ppg/minutes">Minutes</a></li>	
					<li><a href="/admin/ppg/members_page">Members page</a></li>		
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
		
			<h1>PPG Links</h1>
			
			<p>Create a new link to be displayed in the new PPG section.</p>	
		
			<form action="/admin/ppg/links/create/" method="post">					
				<p><input type="submit" value="Create a new links" /></p>					
			</form>
				
			<?php if ($links) : ?>
				

					<div class="row heading">
							<div class="col1" style="float: left;width: 280px;">Title</div>
							
							<div class="col2" >
								<span class="manage" style="float: left;width: 292px;">URL</span>
							</div>
							<div class="col2">
								<span class="del" >Remove</span>
							</div>
						</div>

			<ul id="items" class="sortable-list">
						
					<?php foreach ($links as $link) : ?>
					<li id="item_<?=$link->id?>">
						<div class="row">
							<div class="col1" style="float: left;width: 280px;">
								<a href="/admin/ppg/links/update/<?=$link->id;?>"><?=$link->title;?></a>
							</div>
							
							<div class="col2" style="float: left;width: 285px; text-align:left;">
								<span class="manage">
									<?=$link->hyperlink;?>
								</span>
							</div>

							<div class="col2" style="float: left;width: 37px;">
								<span class="del"><a href="/admin/ppg/links/delete/<?=$link->id;?>" title="Delete Link" onclick="return confirmDelete()">Delete</a></span>
							</div>

						</div>
					</li> 
					<?php endforeach; ?>
				</ul>

				
			<?php else : ?>
				
				<p>No <em>links</em> found.</p>
				
			<?php endif; ?>
		
		</div>				
	</div>

<script type="text/javascript">
				
				function updateOrder()
				{
					var options = {
						method : 'post',
						parameters : Sortable.serialize('items')
					};
					
					new Ajax.Request('/admin/ppg/order_links', options);
				}
								
				document.observe("dom:loaded", function() {
				
					Sortable.create('items', { onUpdate : updateOrder });
				});
						</script>	


<?=$this->load->view('assets/admin/footer');?>