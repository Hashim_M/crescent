<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		<div id="main">			
			<h1>Create New Link</h1>
			<form action="/admin/ppg/links/create/" method="post">	
				<?=$this->load->view('assets/admin/forms/ppg_links')?>							
				<p><input type="submit" value="Save" /></p>					
			</form>	
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>