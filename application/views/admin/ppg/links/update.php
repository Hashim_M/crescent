<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/ppg/">PPG Home page</a></li>
					<li><a href="/admin/ppg_pages/" title="PPG pages">PPG Pages</a></li>
					<li><a href="/admin/ppg/links" class="active">Links</a></li>	
					<li><a href="/admin/ppg/minutes">Minutes</a></li>	
					<li><a href="/admin/ppg/members_page">Members page</a></li>		
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
			<h1>Update PPG Link</h1>
			<form action="/admin/ppg/links/update/" method="post">	
				<?=$this->load->view('assets/admin/forms/ppg_links');?>							
				<input type="hidden" name="id" value="<?=$id?>" />
				<p><input type="submit" value="Save changes" /></p>					
			</form>	
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>