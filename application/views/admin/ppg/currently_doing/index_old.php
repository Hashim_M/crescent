<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/ppg/">PPG Home page</a></li>
					<li><a href="/admin/ppg_pages/" title="PPG pages">PPG Pages</a></li>
					<li><a href="/admin/ppg/links">Links</a></li>	
					<li><a href="/admin/ppg/minutes" class="active">Minutes</a></li>	
					<li><a href="/admin/ppg/members_page">Members page</a></li>		
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>					
			</div>
		</div>
		
		<div id="main">			
		
			<h1>Currently Doing</h1>
		
			<form action="/admin/ppg/minutes/create/" method="post">					
				<p><input type="submit" value="Create a new article" /></p>					
			</form>
			
			<!-- Draft Items -->
			
			<h2>Draft Items</h2>
					
			<?php if ($draft_news->num_rows() > 0) : ?>
				<div class="row heading">
							<div class="col1" style="float: left;width: 300px;">Title</div>
							<div class="col2">
								<span class="manage" style="float: left;width: 114px;">
									Published
								</span>
							</div>
							<div class="col2" >
								<span class="manage" style="float: left;width: 158px;">Modified</span>
							</div>
							<div class="col2">
								<span class="del" >Action</span>
							</div>
						</div>

			<ul id="items" class="sortable-list">
						
					<?php foreach($draft_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1" style="float: left;width: 276px;"><a href="/admin/ppg/minutes/update/<?=$news_item->id;?>"><?=$news_item->title;?></a></div>
							<div class="col2" style="float: left;width: 107px;">
								<span class="manage">
									<?=date('d F Y' , strtotime($news_item->published));?>
								</span>
							</div>
							<div class="col2" style="float: left;width: 107px;">
								<span class="manage">
									<?php if($news_item->modified != '0000-00-00 00:00:00') : ?>
										<?=date('d F Y' , strtotime($news_item->modified));?>
									<?php else : ?>
										N/A
									<? endif; ?>
								</span>
							</div>
							<div class="col2" style="float: left;width: 107px;">
								<span class="del"><a href="/admin/ppg/minutes/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/ppg/minutes/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a></span>
							</div>
						</div>
					</li> 
					<?php endforeach; ?>
				</ul>


				
					
			<?php else : ?>
					
				<p>No <em>draft</em> articles found.</p>
					
			<?php endif; ?>
			
			<!-- Published Items -->	
			
			<h2>Published Items</h2>
					
			<?php if ($publish_news->num_rows() > 0) : ?>
				<div class="row heading">
							<div class="col1" style="float: left;width: 300px;">Title</div>
							<div class="col2">
								<span class="manage" style="float: left;width: 114px;">
									Published
								</span>
							</div>
							<div class="col2" >
								<span class="manage" style="float: left;width: 158px;">Modified</span>
							</div>
							<div class="col2">
								<span class="del" >Action</span>
							</div>
						</div>

				<ul id="items" class="sortable-list">

					<?php foreach($publish_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1" style="float: left;width: 280px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>"><?=$news_item->title;?></a>
							</div>
							
							<div class="col2" style="float: left;width: 285px; text-align:left;">
								<span class="manage">
									<?=date('d F Y' , strtotime($news_item->published));?>
								</span>
							</div>

							<div class="col2" style="float: left;width: 37px;">
								<span class="del"><a href="/admin/ppg/minutes/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/ppg/minutes/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a></span>
							</div>

						</div>
					</li> 
					<?php endforeach; ?>


				</ul>

				
				
					
			<?php else : ?>
					
				<p>No <em>published</em> articles found.</p>
					
			<?php endif; ?>
			
			<!-- Pending Items -->	
			
			<h2>Pending Items</h2>
					
			<?php if ($pending_news->num_rows() > 0) : ?>
				<div class="row heading">
							<div class="col1" style="float: left;width: 300px;">Title</div>
							<div class="col2">
								<span class="manage" style="float: left;width: 114px;">
									Published
								</span>
							</div>
							<div class="col2" >
								<span class="manage" style="float: left;width: 158px;">Modified</span>
							</div>
							<div class="col2">
								<span class="del" >Action</span>
							</div>
						</div>

				<ul id="items" class="sortable-list">
					
					<?php foreach($pending_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1"  style="float: left;width: 276px;"><a href="/admin/ppg/minutes/update/<?=$news_item->id;?>"><?=$news_item->title;?></a></div>
							<div class="col2" style="float: left;width: 107px;">
								<span class="manage">
									<?=date('d F Y' , strtotime($news_item->published));?>
								</span>
							</div>
							<div class="col2" style="float: left;width: 107px;">
								<span class="manage">
									<?php if($news_item->modified != '0000-00-00 00:00:00') : ?>
										<?=date('d F Y' , strtotime($news_item->modified));?>
									<?php else : ?>
										N/A
									<? endif; ?>
								</span>
							</div>
							<div class="col2" style="float: left;width: 107px;">
								<span class="del"><a href="/admin/ppg/minutes/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/ppg/minutes/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a></span>
							</div>
						</div>
					</li> 
					<?php endforeach; ?>
				</ul>

					
				
					
			<?php else : ?>
					
				<p>No <em>pending</em> articles found.</p>
					
			<?php endif; ?>
					
			</div>				
		</div>

		

<script type="text/javascript">
				
				function updateOrder()
				{
					console.log('update');
					var options = {
						method : 'post',
						parameters : Sortable.serialize('items')
					};
					
					new Ajax.Request('/admin/ppg/order_currently_doing', options);
				}
								
				document.observe("dom:loaded", function() {
				
						console.log(Sortable);

					Sortable.create('items', { onUpdate : updateOrder });
				});
						</script>	

<?=$this->load->view('assets/admin/footer');?>