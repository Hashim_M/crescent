<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/ppg/">PPG Home page</a></li>
					<li><a href="/admin/ppg_pages/" title="PPG pages">PPG Pages</a></li>
					<li><a href="/admin/ppg/links">Links</a></li>	
					<li><a href="/admin/ppg/minutes" class="active">Minutes</a></li>	
					<li><a href="/admin/ppg/members_page">Members page</a></li>		
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
		
			
			<h1>Minutes</h1>
		
			<form action="/admin/ppg/minutes/create/" method="post">					
				<p><input type="submit" value="Create a new article" /></p>					
			</form>

			<!-- Draft Items -->
			
			<h2>Draft Items</h2>
				
			<?php if ($draft_news->num_rows() > 0) : ?>
				

					<div class="row heading">
							<div class="col1" style="float: left;width: 280px;">Title</div>
							
							<div class="col2" >
								<span class="manage" style="float: left;width: 292px;">Date Published</span>
							</div>
							<div class="col2">
								<span class="del" >Action</span>
							</div>
						</div>

			<ul id="draft-items" class="sortable-list">
						
					<?php foreach ($draft_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1" style="float: left;width: 280px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>"><?=$news_item->title;?></a>
							</div>
							
							<div class="col2" style="float: left;width: 285px; text-align:left;">
								<span class="manage">
									<?=date('d F Y' , strtotime($news_item->published));?>
								</span>
							</div>

							<div class="col2" style="float: left;width: 37px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/ppg/minutes/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a>
							</div>

						</div>
					</li> 
					<?php endforeach; ?>
				</ul>

				
			<?php else : ?>
				
				<p>No <em>published</em> articles found.</p>
				
			<?php endif; ?>


			<!-- Published Items -->	
			
			<h2>Published Items</h2>
				
			<?php if ($publish_news->num_rows() > 0) : ?>
				

					<div class="row heading">
							<div class="col1" style="float: left;width: 280px;">Title</div>
							
							<div class="col2" >
								<span class="manage" style="float: left;width: 292px;">Date Published</span>
							</div>
							<div class="col2">
								<span class="del" >Action</span>
							</div>
						</div>

			<ul id="publish-items" class="sortable-list">
						
					<?php foreach ($publish_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1" style="float: left;width: 280px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>"><?=$news_item->title;?></a>
							</div>
							
							<div class="col2" style="float: left;width: 285px; text-align:left;">
								<span class="manage">
									<?=date('d F Y' , strtotime($news_item->published));?>
								</span>
							</div>

							<div class="col2" style="float: left;width: 37px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/ppg/minutes/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a>
							</div>

						</div>
					</li> 
					<?php endforeach; ?>
				</ul>

				
			<?php else : ?>
				
				<p>No <em>published</em> articles found.</p>
				
			<?php endif; ?>
		
			<!-- Pending Items -->	
			
			<h2>Pending Items</h2>
					
			<?php if ($pending_news->num_rows() > 0) : ?>
				

					<div class="row heading">
							<div class="col1" style="float: left;width: 280px;">Title</div>
							
							<div class="col2" >
								<span class="manage" style="float: left;width: 292px;">Date Published</span>
							</div>
							<div class="col2">
								<span class="del" >Action</span>
							</div>
						</div>

			<ul id="pending-items" class="sortable-list">
						
					<?php foreach ($pending_news->result() as $news_item) : ?>
					<li id="item_<?=$news_item->id?>">
						<div class="row">
							<div class="col1" style="float: left;width: 280px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>"><?=$news_item->title;?></a>
							</div>
							
							<div class="col2" style="float: left;width: 285px; text-align:left;">
								<span class="manage">
									<?=date('d F Y' , strtotime($news_item->published));?>
								</span>
							</div>

							<div class="col2" style="float: left;width: 37px;">
								<a href="/admin/ppg/minutes/update/<?=$news_item->id;?>" title="Edit News Item">Edit</a> | <a href="/admin/ppg/minutes/delete/<?=$news_item->id;?>" title="Delete News Item" onclick="return confirmDelete()">Delete</a>
							</div>

						</div>
					</li> 
					<?php endforeach; ?>
				</ul>

				
			<?php else : ?>
				
				<p>No <em>published</em> articles found.</p>
				
			<?php endif; ?>

		</div>				
	</div>

<script type="text/javascript">
				
				function updateDraftOrder()
				{
					
					var options = {
						method : 'post',
						parameters : Sortable.serialize('draft-items')
					};
					
					new Ajax.Request('/admin/ppg/order_currently_doing', options);
				}

				function updatePubOrder()
				{
					var options = {
						method : 'post',
						parameters : Sortable.serialize('publish-items')
					};
					
					new Ajax.Request('/admin/ppg/order_currently_doing', options);
				}

				function updatePenOrder()
				{
					
					var options = {
						method : 'post',
						parameters : Sortable.serialize('pending-items')
					};
					
					new Ajax.Request('/admin/ppg/order_currently_doing', options);
				}
								
				document.observe("dom:loaded", function() {
				
					
					var draft = document.getElementById('draft-items');
					if(draft != null)
						Sortable.create('draft-items', { onUpdate : updateDraftOrder });
					
					var publish = document.getElementById('publish-items');
					if(publish != null)
						Sortable.create('publish-items', { onUpdate : updatePubOrder });
					
					var pending = document.getElementById('pending-items');
					if(pending != null)
						Sortable.create('pending-items', { onUpdate : updatePenOrder });
				});
						</script>	


<?=$this->load->view('assets/admin/footer');?>