<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/ppg/">PPG Home page</a></li>
					<li><a href="/admin/ppg_pages/" title="PPG pages">PPG Pages</a></li>
					<li><a href="/admin/ppg/links">Links</a></li>	
					<li><a href="/admin/ppg/minutes" class="active">Minutes</a></li>	
					<li><a href="/admin/ppg/members_page">Members page</a></li>		
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		
		<div id="main">			
			<h1>News / Update Minutes Article</h1>
			<form action="/admin/ppg/minutes/update/" method="post">	
				<?=$this->load->view('assets/admin/forms/news');?>							
				<input type="hidden" name="id" value="<?=$id?>" />
				<p><input type="submit" value="Save changes" /></p>					
			</form>	
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>