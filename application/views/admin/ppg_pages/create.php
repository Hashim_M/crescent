<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/ppg/" >PPG Home page</a></li>
						<li><a href="/admin/ppg_pages/" title="PPG pages" class="active">PPG Pages</a></li>
						<li><a href="/admin/ppg/links">Links</a></li>	
						<li><a href="/admin/ppg/minutes">Minutes</a></li>	
						<li><a href="/admin/ppg/members_page">Members page</a></li>		
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>PPG Page / Create page</h1>
				<form action="/admin/ppg_pages/create/" method="post">	
					<?=$this->load->view('assets/admin/forms/ppg_pages')?>							
					<p><input type="submit" value="Save" /></p>					
				</form>	
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>