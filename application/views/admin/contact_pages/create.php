<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/contact/" title="Contact text">Contact Text</a></li>
						<li><a href="/admin/contact_pages/" title="Contact pages" class="active">Contact Pages</a></li>
						<li><a href="/admin/markers/" title="Map Markers">Map Markers</a></li>				
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>Contact Page / Create page</h1>
				<form action="/admin/contact_pages/create/" method="post">	
					<?=$this->load->view('assets/admin/forms/contact_pages')?>							
					<p><input type="submit" value="Save" /></p>					
				</form>	
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>