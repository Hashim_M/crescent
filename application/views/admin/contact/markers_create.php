<?=$this->load->view('assets/admin/header'); ?>
<div id="content">

	<div id="sidebar">		
		<div id="sidemenu">
			<ul>
				<li><a href="/admin/contact/" title="Contact text">Contact Text</a></li>
				<li><a href="/admin/contact_pages/" title="Contact pages">Contact Pages</a></li>
				<li><a href="/admin/markers/" title="Map Markers" class="active">Map Markers</a></li>				
				<li><a href="/admin/logout">Logout?</a></li>
			</ul>				
		</div>
	</div>
	<div id="main">

		<h1>Create marker</h1>

		<form action="/admin/markers/create/" method="post">
			<?php $this->load->view('assets/admin/forms/markers'); ?>
			<p><input type="submit" class="submit" value="Save" /> <a class="cancel" href="/admin/contact/map/">Cancel &amp; return</a></p>
		</form>
				
	</div>

</div>
<?=$this->load->view('assets/footer'); ?>