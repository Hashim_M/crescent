<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/contact/" title="Contact text" class="active">Contact Text</a></li>
					<li><a href="/admin/contact_pages/" title="Contact pages">Contact Pages</a></li>
					<li><a href="/admin/markers" title="Map Markers">Map Markers</a></li>				
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		<div id="main">			
			
			<h1>Contact Text</h1>
			
			<p>Update the content that is displayed on the contact pages. Once you have clicked <strong>save</strong> the changes will be instantly displayed on the website.</p>
			
			<form action="/admin/contact/" method="post">	
				<?=$this->load->view('assets/admin/forms/contact')?>							
				<p><input type="submit" value="Save" /></p>					
			</form>
				
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>