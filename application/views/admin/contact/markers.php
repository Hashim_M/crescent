<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/contact/" title="Contact text">Contact Text</a></li>
				<li><a href="/admin/contact_pages/" title="Contact pages">Contact Pages</a></li>
					<li><a href="/admin/markers/" title="Map Markers" class="active">Map Markers</a></li>				
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		<div id="main">			
			
			<h1>Location Markers</h1>
			
			<form action="/admin/markers/create">
				<p><input type="submit" value="Create marker" /></p>
			</form>	
		
			<?php if($items) : ?>
					
				<table width="100%" cellpadding="0" cellspacing="0" class="items">
					<tr>
						<th>Location</th>
						<th width="80">Remove</th>
					</tr>
				
					<?php foreach($items as $item) : ?>
						<tr>
							<td><a href="/admin/markers/update/<?=$item->id;?>"><?=$item->title;?></a></td>
							<td><span class="del"><a href="/admin/markers/delete/<?=$item->id;?>" onclick="return confirmDelete()">Delete</a></span></td>
						</tr>
					<?php endforeach; ?>
					 
				 </table>
			
			<?php endif; ?>	
				
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>
