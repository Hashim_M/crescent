<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>Practice info / Update page</h1>
				<form action="/admin/practice_info/update/" method="post">	
					<?=$this->load->view('assets/admin/forms/practice_info')?>							
					<p><input type="hidden" name="id" value="<?=$id?>" /><input type="submit" value="Save changes" /></p>					
				</form>	
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>