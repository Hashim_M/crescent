<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>Practice info / Create page</h1>
				<form action="/admin/practice_info/create/" method="post">	
					<?=$this->load->view('assets/admin/forms/practice_info')?>							
					<p><input type="submit" value="Save" /></p>					
				</form>	
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>