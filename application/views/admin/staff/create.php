<?=$this->load->view('assets/admin/header');?>
	<div id="content">	
		<div id="sidebar">		
			<div id="sidemenu">
				<ul>
					<li><a href="/admin/logout">Logout?</a></li>
				</ul>				
			</div>
		</div>
		<div id="main">			
			<h1>Staff / Create Page</h1>
			<form action="/admin/staff/create/" method="post">	
				<?=$this->load->view('assets/admin/forms/staff');?>							
				<p><input type="submit" value="Save" /></p>					
			</form>	
		</div>				
	</div>
<?=$this->load->view('assets/admin/footer');?>