<?=$this->load->view('assets/admin/header')?>
		<div id="content">	
			<div id="sidebar">		
				<div id="sidemenu">
					<ul>
						<li><a href="/admin/logout">Logout?</a></li>
					</ul>				
				</div>
			</div>
			<div id="main">			
				<h1>Staff</h1>
				<form action="/admin/staff/create/" method="post">					
					<p><input type="submit" value="Create a new page" /></p>					
				</form>	
				
				<?php if ($pages->num_rows() > 0) : ?>		
				
				<ul id="items" class="sortable-list">
					<?php foreach($pages->result() as $page) : ?>
					<li id="item_<?=$page->id?>">
						<div class="row">
							<div class="col1"><a href="/admin/staff/update/<?=$page->id?>"><?=$page->title?></a></div>
							<div class="col2">
								<span class="manage"><?=date('d M Y' , strtotime($page->modified))?></span>
								<span class="del"><a href="/admin/staff/delete/<?=$page->id?>" onclick="return confirmDelete()">Delete</a></span>
							</div>
						</div>
					</li> 
					<?php endforeach; ?>
				</ul>
				
				<script type="text/javascript">
				
				function updateOrder() {
					var options = {
						method : 'post',
						parameters : Sortable.serialize('items')
					};
					
					new Ajax.Request('/admin/staff/order', options);
				}
								
				document.observe("dom:loaded", function() {
				
					Sortable.create('items', { onUpdate : updateOrder });
				});
										
				</script>
				
				<?php else : ?>
				
					<p>No pages found.</p>
				
				<?php endif; ?>
				
			</div>				
		</div>
<?=$this->load->view('assets/admin/footer')?>