<?=$this->load->view('assets/header')?>
<div id="content">
	<div id="sidebar">
		<?=$this->load->view('assets/sidemenu/contact')?>
	</div>
	<div id="main">
		<h1>Location</h1>
		<p><?=$contact->location?></p>
		<h3>76 Queens Crescent, London NW5 4EB</h3>
		<?=$this->load->view('assets/large_map')?>
	</div>
</div>
<?=$this->load->view('assets/footer')?>
