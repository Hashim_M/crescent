<?=$this->load->view('assets/header')?>
<div id="content">

	<div id="tabarea">
		<?=$this->load->view('assets/flowpanes')?>
	</div>

	<div class="layout1">
		<div class="col1">
			<div class="updates">
				<div class="left">
					<div class="block">
						<h2>Recent News</h2>
						<div class="items">
							<?php if ($news_items->num_rows() > 0) : ?>
							<?php foreach ($news_items->result() as $news_item) : ?>
							<div class="item">
								<div class="date">
									<h3>
										<span class="day">
											<?=date("jS", strtotime($news_item->published))?>
										</span>
										<br />
										<?=date("M", strtotime($news_item->published))?> '<?=date("y", strtotime($news_item->published))?>
									</h3>
								</div>
								<div class="info">
									<h3>
										<a href="/news/<?=str_replace("-", "/", $news_item->published);?>/<?=$news_item->url_title?>">
											<?=$news_item->title?>
										</a>
									</h3>
									<p>
										<?=word_limiter($news_item->summary, 43)?><br />
										<a href="/news/<?=str_replace("-", "/", $news_item->published);?>/<?=$news_item->url_title?>">
											Read&nbsp;more &gt;
										</a>
									</p>
								</div>
							</div>
							<?php endforeach; ?>

							<?php else : ?>

							<p>
								Sorry, <em>no</em> news items to show
							</p>

							<?php endif; ?>
						</div>

						<div class="further">
							<div class="subscribe">
								<p><a href="/rss/news.xml" class="feed">Subscribe</a></p>
							</div>
							<div class="more">
								<p><a href="/news/">Read more news &gt;</a></p>
							</div>
						</div>
					</div>

				</div>
				<div class="right"></div>
			</div>
		</div>

		<div class="col2">
			<!--<div class="block">-->
			<!--<?=$contact_home->contact_home?>-->
			<!--</div>-->
			<div class="block">
				<h2>How to find us</h2>
				<p><a href="/contact">View location map and address &gt;</a></p>
				<?=$this->load->view('assets/small_map')?>
			</div>
			<div class="block">
				<?php
				/*
				<?=$this->load->view('assets/latest_tweets')?>
				*/
				?>
			</div>
		</div>

		<div class="banners">
			<div class="banner1">
				<a href="/patient_services/register">
					<img src="/static/img/banners/register.jpg" alt="" />
				</a>
			</div>
			<div class="banner2">
				<a href="/patient_services/appointments">
					<img src="/static/img/banners/appointments.jpg" alt="" />
				</a>
			</div>
			<div class="banner3">
				<a href="/practice_info/opening_hours">
					<img src="/static/img/banners/opening.jpg" alt="" />
				</a>
			</div>
			<div class="banner4">
				<a href="/contact/information/">
					<img src="/static/img/banners/contact.jpg" alt="" />
				</a>
			</div>
		</div>
	</div>
</div>

<?=$this->load->view('assets/footer')?>
