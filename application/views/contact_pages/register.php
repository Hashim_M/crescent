<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/patient_services');?>			
			</div>
			<div id="main">
			
				<h1><?=$content->title;?></h1>
				
				<?php if (!empty($submitted)) : ?>
					
				<h2>Thank you for submitting your details. </h2>
				
				<p><a href="/patient_services/register">Click here to submit another form.</a></p>
					
				<?php else : ?>
	
				<?=$content->body;?>
								
				
				<?=$this->load->view('assets/forms/register');?>
				
					<?php endif; ?>
			
			</div>				
		</div>
<?=$this->load->view('assets/footer')?>