<div id="map_canvas" style="width: 620px; height: 420px"></div>		
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(function() {
    var myLatlng = new google.maps.LatLng(51.550657,-0.151779);
    var myOptions = {
      zoom: 16,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    
	var infowindow = new google.maps.InfoWindow({
	    content: '<div><h3>Queens Crescent Practice</h3><p>76 Queens Crescent,<br /> London NW5 4EB</p></div>'
	});
    
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(51.549657,-0.151779), 
        map: map,
        title:"Queens Crescent Practice"
    });   
    

	// open on load
	infowindow.open(map,marker);
	

});
   
</script>