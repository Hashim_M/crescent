<?=$this->load->view('assets/header')?>

<div id="content">
	<div id="sidebar">
		<?=$this->load->view('assets/sidemenu/patient_services')?>
	</div>

	<div id="main">
		<h1>Patient services</h1>

		<div class="twocol">
			<div class="col1">
				<p>
					The Queens Crescent Practice has been in Camden since 1887. The practice only offers NHS services and does not provide private health care.  The tradition is to provide our patients with the benefit of a very extended primary care team.    </p><p> To this end, we have a large number of attached staff and facilities for our patients to benefit from.  This ranges from employment advice,  to finance and debt advice as well as the professional help from a wide range of more than 10 different counselling services.
				</p>
				<p>
					<a href="/patient_services/additional_services">
						Click here to find out more information on our extremely wide range of services we offer at the Queens Crescent Practice.
					</a>
				</p>
			</div>

			<div class="col2">
				<p>
					<img src="/static/img/patient_services.jpg" alt="" />
				</p>
			</div>
		</div>
	</div>
</div>

<?=$this->load->view('assets/footer')?>
