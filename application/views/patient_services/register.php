<?=$this->load->view('assets/header')?>
<div id="content">
	<div id="sidebar">
		<?=$this->load->view('assets/sidemenu/patient_services')?>
	</div>

	<div id="main">
		<h1><?=$content->title?></h1>

		<?php if(isset($validation_failed) && $validation_failed): ?>
		<div class="error" style="display:block;padding:5px;">
			Registration Failed - please correct errors in the form and try again
		</div>
		<?php endif; ?>

		<?php if (!empty($submitted)) : ?>

		<h2>Thank you for submitting your details. </h2>
		<p>
			<a href="/patient_services/register">
				Click here to submit another form.
			</a>
		</p>

		<?php else : ?>

		<?=$content->body?>
		<?=$this->load->view('assets/forms/register')?>

		<?php endif; ?>
	</div>
</div>
<script src="/static/js/enabled-on.js"></script>
<script src="/static/js/display-on.js"></script>
<?=$this->load->view('assets/footer')?>
