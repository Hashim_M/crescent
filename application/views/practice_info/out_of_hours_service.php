<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
			
				<h1>Out of Hours Service</h1>
				<div class="main">				
					<div class="body">			
						<p>The practice uses the services of the local doctors cooperative called CAMIDOC. If you require the service of the practice outside core working hours which are 8.30am to 6.30pm Monday to Thursday; 8.30am to 6pm on Friday, please telephone the CAMIDOC line number on 020-7388 5800 stating clearly your requirements.  
						</p><p>
						It helps to have ready when speaking to the doctor when they phone back, some information concerning temperature, duration of illness, fluid and food intake, and other family members affected.    
						</p><p>
						Please always ensure your phone numbers are correct both on your records and when dealing with the out of hours service. Currently the out of hours service does not have access directly to your medical records and so any important information such as allergies must be communicated.
						</p>
					</div>
					<div class="sidebar">
						
						<div class="sidebanner"><a href="/practice_info/opening_hours"><img src="/static/img/banners/opening.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/contact/information"><img src="/static/img/banners/contact.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/patient_services/results_service"><img src="/static/img/banners/results.jpg" alt="" /></a></div>
						
					</div>
				</div>

			</div>				
		</div>
<?=$this->load->view('assets/footer')?>