<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
			
				<h1>Opening Hours</h1>
				<div class="main">				
					<div class="body">
						<p>Our reception is open at the following times:</p>
						
						<h2>Weekdays:</h2>
						<h3>8.30am to 6.30pm (Friday 6pm)</h3>
						
						<p>Some appointments available up to <strong>8.00pm</strong> or from <strong>7.30am</strong> for working&nbsp;patients.</p> 
						<h3>Friday 1.00pm-2.00pm we are closed for staff training</h3>
						
					</div>
					<div class="sidebar">
						
						<div class="sidebanner"><a href="/contact/information"><img src="/static/img/banners/contact.jpg" alt="" /></a></div>

						
					</div>
				</div>
				
			</div>				
		</div>
<?=$this->load->view('assets/footer')?>