<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
			
				<h1>Under 2's</h1>
				<div class="main">				
					<div class="body">				
						<p>				
						Emergency Service for under 2’s. We are happy to provide an emergency service for under 2’s. When the child is brought to the surgery they can be seen as an extra in clinics if the condition is urgent.  It must be stressed this service is not for an ongoing difficulty or a recurring difficulty.  
						</p><p>
						For example, a child with deteriorating eczema would not qualify for an emergency under 2 fitted in appointment. A routine appointment should be made for this.     A child with a high fever who is acutely unwell would qualify for an under-2 emergency “fitted in” appointment.    
						</p><p>
						If you are bringing your child as an emergency to the surgery, please try and accommodate the timetable of the surgeries by arriving before 11.00 in the morning for a morning appointment and before 3.30 in the afternoon for an afternoon appointment.   
						</p><p>
						There is a duty doctor available both in the morning and the afternoon with whom you can discuss difficulties or the requirement for further assessment.
						</p>
					</div>
					<div class="sidebar">
						
						<div class="sidebanner"><a href="/contact"><img src="/static/img/banners/findus.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/patient_services/appointments"><img src="/static/img/banners/appointments.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/contact/information"><img src="/static/img/banners/contact.jpg" alt="" /></a></div>
						
					</div>
				</div>	
			</div>				
		</div>
<?=$this->load->view('assets/footer')?>