<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
			
				<h1>Ethos</h1>
				
				<div class="ethos">
					<div class="thumb"><img src="/static/img/thumbs/1.jpg" alt="" /></div>
					<div class="stat"><h3>We aim to provide excellent patient care in a welcoming and accessible environment.</h3></div>
				</div>
				
				<div class="ethos">
					<div class="stat"><h3>We will stay committed to the NHS, maintain an open list and provide as wide range of services on-site as possible.</h3></div>

					<div class="thumb"><img src="/static/img/thumbs/2.jpg" alt="" /></div>
				</div>
				
				<div class="ethos">
					<div class="thumb"><img src="/static/img/thumbs/3.jpg" alt="" /></div>
					<div class="stat"><h3>We will have a significant educational role at both under- and post-graduate level and will be adventurous and ‘leading edge’ in our development.</h3></div>
				</div>

				<div class="ethos">
					<div class="stat"><h3>We will work and train in integrated teams with other professionals and will meet most reasonable targets.</h3></div>
					<div class="thumb"><img src="/static/img/thumbs/4.jpg" alt="" /></div>
				</div>

				<div class="ethos">
					<div class="thumb"><img src="/static/img/thumbs/5.jpg" alt="" /></div>
					<div class="stat"><h3>We aim to have a happy and loyal workforce and to maintain financial security.</h3></div>
				</div>
			
			</div>				
		</div>
<?=$this->load->view('assets/footer')?>