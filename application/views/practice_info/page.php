<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
				<h1><?=$content->title?></h1>
				
				<?php if ($content->display_sidebar == 1) : ?>
				
					<div class="main">				
						<div class="body">
							<?=$content->body?>
							<?php if($content->url_title == 'registration_catchment_area'):?>
								<?=$this->load->view('practice_info/map');?>
							<?php endif; ?>		
						</div>
						<div class="sidebar">

							<?php
							if ($banners->num_rows() > 0) :
							foreach ($banners->result() as $banner) :
							?>
								<div class="sidebanner"><a href="<?=$banner->href?>"><img src="/static/img/banners/<?=$banner->file_name?>" alt="<?=$banner->title?>" /></a></div>
							<?php						
							endforeach;	
							endif;
							?>				


						</div>
					</div>
				
				<?php else : ?>
				
					<?=$content->body?>
					<?php if($content->url_title == 'registration_catchment_area'):?>
						<?=$this->load->view('practice_info/map');?>
					<?php endif; ?>				
				<?php endif; ?>				
				
			</div>				
		</div>
<?=$this->load->view('assets/footer')?>