<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
			
				<h1>Arts Programme</h1>
				<div class="main">				
					<div class="body">
				
						<p>Success with an Arts Council England bid has enabled an exciting Arts
						programme, bringing benefits to patients and staff. The funding has been
						matched by the Kentish Town Improvement Fund. This patient resourced
						charity, helps support the four strands of Arts activity:</p>
						
						<ul>
						<li>Rotating three monthly Art exhibitions in a special waiting
						area gallery space has seen local schools, photographers, art
						therapists, and printmakers display their work over the last 12 months.</li>
						
						<li>Workshops for patients led by local artists provide
						opportunities for interaction and group work on 8- 10 week programmes</li>
						
						<li>From a shortlist of 8 from many applicants, a fixed
						commissioned sculpture will enhance the community garden.</li>
						
						<li>An artist residency will allow the interaction of staff with
						an artist on a one year project.</li>
						</ul>
					</div>
					<div class="sidebar">
						
						<div class="sidebanner"><a href="/awards"><img src="/static/img/banners/awards.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/patient_services/register"><img src="/static/img/banners/register.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/patient_services/results_service"><img src="/static/img/banners/results.jpg" alt="" /></a></div>
						
					</div>
				</div>

			</div>				
		</div>
<?=$this->load->view('assets/footer')?>