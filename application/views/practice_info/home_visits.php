<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/practice_info')?>			
			</div>
			<div id="main">
			
				<h1>Home Visits</h1>
				<div class="main">				
					<div class="body">
						<p>The Queens Crescent Practice is only able to provide home visits in cases of emergencies.    If you require a home visit the call should be made at the very earliest opportunity.     A home visit for a deteriorating condition which can be planned for should be telephoned into the practice before 10.30 in the morning so that the visiting doctor can plan his timetable around visiting you. 
						</p><p>
						It is unusual to require a home visit and patients are encouraged to come to the Centre and be seen on all occasions in order to facilitate further investigation and initial tests which can be carried out on site.  Many of these cannot be carried out in the home.    If necessary transport can, on occasion, be provided to help you reach the surgery. 
						</p><p>
						The doctors visit on practice bicycles and in the practice vehicle if required.   
						</p>	
					</div>
					<div class="sidebar">
						
						<div class="sidebanner"><a href="/patient_services/register"><img src="/static/img/banners/register.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/contact"><img src="/static/img/banners/findus.jpg" alt="" /></a></div>
						<div class="sidebanner"><a href="/awards"><img src="/static/img/banners/awards.jpg" alt="" /></a></div>
						
					</div>
				</div>

			</div>				
		</div>
<?=$this->load->view('assets/footer')?>