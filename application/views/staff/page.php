<?=$this->load->view('assets/header')?>
	<div id="content">	
		<div id="sidebar">		
			<?=$this->load->view('assets/sidemenu/staff')?>			
		</div>
		<div id="main">			
			<h1><?=$content->title?></h1>	
			<?=$content->body;?>
		</div>				
	</div>
<?=$this->load->view('assets/footer')?>