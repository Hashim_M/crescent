<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
				<?=$this->load->view('assets/sidemenu/staff')?>			
			</div>
			<div id="main">
				<h1>Doctors</h1>
<p>		
<strong>Dr Jessica Baron</strong>  – Special Interests: Female Health <br />
<strong>Dr Katherine Breckon</strong>  – Teaching<br />
<strong>Dr Simon Brownleader</strong>  – Training and Second Life Lead <br />
<strong>Dr Vanessa Eisman</strong>  – Teaching Lead <br />
<strong>Dr Richard Garlick</strong>  – Education and Prescribing/ Minor surgery <br />
<strong>Dr Elizabeth Goodburn</strong> – Cinical Governance<br />
<strong>Dr Kavitha Gowribalan</strong>  – long term condition clinic lead.<br />
<strong>Dr Zarina Ismail</strong>  – Partner Queens Crescent  Practice<br />
<strong>Dr Marek Koperski</strong>  – Executive Committee NHS Camden<br />
<strong>Dr Karen Ling</strong>  – Teaching/Minor Surgery<br />
<strong>Dr Roy Macgregor</strong>  – Managing Partner<br />
<strong>Dr Philip Posner</strong>  – Quality and Outcome Lead<br />
<strong>Dr Justin Sacks</strong>  – Sexual Health<br />
<strong>Dr Jane Sackville West</strong>  – Training and Teaching<br />
<strong>Dr Penny Sheehan</strong>  – Long Term conditions<br />	
</p>			
				
			</div>
				
		</div>
<?=$this->load->view('assets/footer')?>