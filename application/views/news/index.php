<?=$this->load->view('assets/header');?>
	<div id="content">
		
		<div id="sidebar">		
			<?=$this->load->view('assets/sidemenu/news');?>			
		</div>
		
		<div id="main">
		
			<div class="main2">
				<div class="body">
					<h1>Latest news</h1>
					
					<?php if ($news_items->num_rows() > 0) : ?>
						<?php foreach ($news_items->result() as $news_item) : ?>
							<div class="item">
								<div class="date">
									<h3><span class="day"><?=date("jS", strtotime($news_item->published));?></span><br /> <?=date("M", strtotime($news_item->published));?> '<?=date("y", strtotime($news_item->published));?></h3>
								</div>
								<div class="info">
									<h3><a href="/news/<?=str_replace("-", "/", $news_item->published);?>/<?=$news_item->url_title;?>"><?=$news_item->title;?></a></h3>
									<p><?=$news_item->summary;?></p>
								</div>
							</div>
						<?php endforeach; ?>
						
    				<?php else : ?>
    					
    					<p>Sorry, <em>no</em> news items to show</p>
    				
    				<?php endif; ?>
    				
				</div>
			
				<div class="sidebar">				
					<?=$this->load->view('assets/latest_tweets');?>	
				</div>
		
			</div>
				
		</div>
				
	</div>
<?=$this->load->view('assets/footer');?>