<?=$this->load->view('assets/header')?>
	<div id="content">	
	
	<?php if($links->num_rows > 1) : ?>		
		
		<?php $count = 1; ?>
		
		<?php foreach($links->result() as $link) : ?>
				
				<?php if($count == 1) : ?>				
				
				<div class="sites">				
				<?php endif; ?>
				
				
				<div class="col<?=$count;?>">
					<p><a href="<?=$link->hyperlink;?>" target="_blank"><img src="/storage/links/<?=$link->thumbnail;?>" alt="<?=$link->title;?>" /></a></p>
					<h3><a href="<?=$link->hyperlink;?>" target="_blank"><?=$link->title;?></a></h3>
					<p><?=$link->description;?></p>
				</div>
				
				<?php if($count == 3) : ?>
				
					</div>
				
				<?php endif; ?>
				
				<?php $count++; ?>
				
				<?php 
				if($count == 4) {
					$count = 1;
				} 
				?>
				
		
		<?php endforeach; ?>
				
		<?php if($count == 2 || $count == 3) : ?>
			
			</div>
			
		<?php endif; ?>
				
	<?php else: ?>
		
		<p>Sorry, no links are available.</p>
	
	<?php endif;?>
				
	</div>
<?=$this->load->view('assets/footer')?>