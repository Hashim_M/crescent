<?=$this->load->view('assets/header')?>
		<div id="content">	
			<div id="sidebar">		
		
			</div>
			<div id="main">
			
				<h1>Disclaimer</h1>
<p>The Queens Crescent Practice takes every safeguard to ensure the content of this web site is accurate. We do not however accept responsibility for the comments or views expressed by individuals or organisations.
</p>
<p>Any medical or health information on the site is provided by medically trained and qualified professionals, unless a statement to the contrary is displayed.
</p>
<p>The Queens Crescent Practice is not liable for the contents of any external Internet sites. These links are provided for information and convenience. The Queens Crescent Practice does not endorse any services or commercial products mentioned on these sites.
</p>
<p><strong>Always consult your own GP if you are concerned about your health.</strong></p>

			</div>				
		</div>
<?=$this->load->view('assets/footer')?>