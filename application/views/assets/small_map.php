<div id="map_canvas" style="width:400px; height:200px"></div>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(function() {
  var myLatlng  = new google.maps.LatLng(51.549657,-0.151779);
  var myOptions = {
    zoom: 16,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  var map    = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title:"Queens Crescent Practice"
  });
});
</script>
