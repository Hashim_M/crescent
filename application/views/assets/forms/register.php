 <form action="/patient_services/register" method="post" enctype="multipart/form-data"> 
	<h2>NHS Family doctor services registration</h2>
	<fieldset class="fieldset">
		<legend>Patient's details</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Title</strong>
				</td>
				<td width="400">
					<select name="prefix">
						<option value="" disabled selected>Select a Title</option>
						<option <?=($this->validation->prefix=='Mr')?'selected':''?>>Mr</option>
						<option <?=($this->validation->prefix=='Mrs')?'selected':''?>>Mrs</option>
						<option <?=($this->validation->prefix=='Miss')?'selected':''?>>Miss</option>
						<option <?=($this->validation->prefix=='Ms*')?'selected':''?>>Ms*</option>
					</select>
          <?=$this->validation->prefix_error?>
				</td>
			</tr>

			<tr>
				<td class="field req" width="220">
					<strong>* Surname</strong>
				</td>
				<td width="400">
					<input type="text" name="surname" class="surname" value="<?=$this->validation->surname?>">
					<?=$this->validation->surname_error?>
				</td>
			</tr>

			<tr>
				<td class="field req" width="220">
					<strong>* First Names</strong>
				</td>
				<td width="400">
					<input type="text" name="firstname" class="firstnames" value="<?=$this->validation->firstname?>">
					<?=$this->validation->firstname_error?>
				</td>
			</tr>

			<tr>
				<td class="field opt" width="220">
					<strong>Previous surname</strong>
				</td>
				<td width="400">
					<input name="previoussurname" type="text" class="previoussurname" value="<?=$this->input->post('previoussurname')?>">
				</td>
			</tr>
		</table>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Gender</strong>
				</td>
				<td width="400">
					<label>
						<input type="radio" name="gender" value="Male" <?=($this->validation->gender=="Male")?'checked="checked"':''?>>
						Male
					</label>
					<label>
						<input type="radio" name="gender" value="Female" <?=($this->validation->gender=="Female")?'checked="checked"':''?>>
						Female
					</label>
          <?=$this->validation->gender_error?>
				</td>
			</tr>
		</table>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Date of birth</strong>
				</td>
				<td width="400">
					<select name="dob_day">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 31; $i++): ?>
						<option <?=($this->validation->dob_day==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="dob_month">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 12; $i++): ?>
						<option <?=($this->validation->dob_month==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="dob_year">
						<option value="" disabled="disabled" selected></option>
						<?php
						$current_year = date('Y');
						$start_year   = $current_year - 100; // 100 years old max

						for($i = $current_year; $i >= $start_year; $i--):
						?>
						<option <?=($this->validation->dob_year==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<?=$this->validation->dob_day_error?>
					<?=$this->validation->dob_month_error?>
					<?=$this->validation->dob_year_error?>
				</td>
			</tr>

			<tr>
				<td class="field req" width="220">
					<strong>* Town and Country of Birth</strong>
				</td>
				<td width="400">
					<input type="text" name="country_of_birth" class="town" value="<?=$this->validation->country_of_birth?>">
					<?=$this->validation->country_of_birth_error?>
				</td>
			</tr>

			<tr>
				<td class="field req" width="220">
					<strong>* Were you born in London</strong>
				</td>
				<td width="400">
					<label>
						<input type="radio" name="born_in_london" value="Yes" <?=($this->validation->born_in_london=="Yes")?'checked="checked"':''?>>
						Yes
					</label>
					<label>
						<input type="radio" name="born_in_london" value="No" <?=($this->validation->born_in_london=="No")?'checked="checked"':''?>>
						No
					</label>
					<?=$this->validation->born_in_london_error?>

					<p class="opt display-on" data-display-on="born_in_london" data-display-value="Yes">
						<select name="london_borough">
							<option value="" disabled selected>Select a Borough</option>
							<option <?=($this->input->post('london_borough')=='Barking and Dagenham')?'selected':''?>>Barking and Dagenham</option>
							<option <?=($this->input->post('london_borough')=='Barnet')?'selected':''?>>Barnet</option>
							<option <?=($this->input->post('london_borough')=='Bexley')?'selected':''?>>Bexley</option>
							<option <?=($this->input->post('london_borough')=='Brent')?'selected':''?>>Brent</option>
							<option <?=($this->input->post('london_borough')=='Bromley')?'selected':''?>>Bromley</option>
							<option <?=($this->input->post('london_borough')=='Camden')?'selected':''?>>Camden</option>
							<option <?=($this->input->post('london_borough')=='City of London')?'selected':''?>>City of London</option>
							<option <?=($this->input->post('london_borough')=='Croydon')?'selected':''?>>Croydon</option>
							<option <?=($this->input->post('london_borough')=='Ealing')?'selected':''?>>Ealing</option>
							<option <?=($this->input->post('london_borough')=='Enfield')?'selected':''?>>Enfield</option>
							<option <?=($this->input->post('london_borough')=='Greenwich')?'selected':''?>>Greenwich</option>
							<option <?=($this->input->post('london_borough')=='Hackney')?'selected':''?>>Hackney</option>
							<option <?=($this->input->post('london_borough')=='Hammersmith and Fulham')?'selected':''?>>Hammersmith and Fulham</option>
							<option <?=($this->input->post('london_borough')=='Haringey')?'selected':''?>>Haringey</option>
							<option <?=($this->input->post('london_borough')=='Harrow')?'selected':''?>>Harrow</option>
							<option <?=($this->input->post('london_borough')=='Havering')?'selected':''?>>Havering</option>
							<option <?=($this->input->post('london_borough')=='Hillingdon')?'selected':''?>>Hillingdon</option>
							<option <?=($this->input->post('london_borough')=='Hounslow')?'selected':''?>>Hounslow</option>
							<option <?=($this->input->post('london_borough')=='Islington')?'selected':''?>>Islington</option>
							<option <?=($this->input->post('london_borough')=='Kensington and Chelsea')?'selected':''?>>Kensington and Chelsea</option>
							<option <?=($this->input->post('london_borough')=='Kingston upon Thames')?'selected':''?>>Kingston upon Thames</option>
							<option <?=($this->input->post('london_borough')=='Lambeth')?'selected':''?>>Lambeth</option>
							<option <?=($this->input->post('london_borough')=='Lewisham')?'selected':''?>>Lewisham</option>
							<option <?=($this->input->post('london_borough')=='Merton')?'selected':''?>>Merton</option>
							<option <?=($this->input->post('london_borough')=='Newham')?'selected':''?>>Newham</option>
							<option <?=($this->input->post('london_borough')=='Redbridge')?'selected':''?>>Redbridge</option>
							<option <?=($this->input->post('london_borough')=='Richmond upon Thames')?'selected':''?>>Richmond upon Thames</option>
							<option <?=($this->input->post('london_borough')=='Southwark')?'selected':''?>>Southwark</option>
							<option <?=($this->input->post('london_borough')=='Sutton')?'selected':''?>>Sutton</option>
							<option <?=($this->input->post('london_borough')=='Tower Hamlets')?'selected':''?>>Tower Hamlets</option>
							<option <?=($this->input->post('london_borough')=='Waltham Forest')?'selected':''?>>Waltham Forest</option>
							<option <?=($this->input->post('london_borough')=='Wandsworth')?'selected':''?>>Wandsworth</option>
							<option <?=($this->input->post('london_borough')=='Westminster')?'selected':''?>>Westminster</option>
						</select>
						<?=$this->validation->london_borough_error?>
					</p>
				</td>
			</tr>
		</table>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Current home address</strong>
				</td>
				<td width="400">
					<textarea cols="5" rows="3" name="address" class="address"><?=$this->validation->address?></textarea>
					<?=$this->validation->address_error?>
				</td>
			</tr>

			<tr>
				<td class="field req" width="220">
					<strong>* Postcode</strong>
				</td>
				<td width="400">
					<input type="text" name="postcode" class="postcode" value="<?=$this->validation->postcode?>">
					<?=$this->validation->postcode_error?>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Telephone number</strong>
				</td>
				<td width="400">
					<input type="text" name="number" class="number" value="<?=$this->validation->number?>">
					<?=$this->validation->number_error?>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Email ID</strong>
				</td>
				<td width="400">
					<input type="email" name="email" class="email" value="<?=$this->validation->email?>">
					<?=$this->validation->email_error?>
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>I consent to receiving personal data via email</strong>
				</td>
				<td width="400">
					<label>
						<input type="radio" name="email_consent" value="Yes" <?=($this->input->post('email_consent')=="Yes")?'checked="checked"':''?>>
						Yes
					</label>
					<label>
						<input type="radio" name="email_consent" value="No" <?=($this->input->post('email_consent')=="No")?'checked="checked"':''?>>
						No
					</label>
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>NHS Number</strong>
				</td>
				<td width="400">
					<input type="text" name="nhsnumber1" class="nhsnumber1" value="<?=$this->input->post('nhsnumber1')?>">
					<input type="text" name="nhsnumber2" class="nhsnumber2" value="<?=$this->input->post('nhsnumber2')?>">
					<input type="text" name="nhsnumber3" class="nhsnumber3" value="<?=$this->input->post('nhsnumber3')?>">
				</td>
			</tr>
		</table>
		<!--
	</fieldset>

	<fieldset class="fieldset">
		<legend>If you are from abroad</legend>
		-->
		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Are you from abroad</strong>
				</td>
				<td width="400">
					<label>
						<input type="radio" name="abroad" value="Yes" <?=($this->validation->abroad=="Yes")?'checked="checked"':''?>>
						Yes
					</label>
					<label>
						<input type="radio" name="abroad" value="No" <?=($this->validation->abroad=="No")?'checked="checked"':''?>>
						No
					</label>
					<?=$this->validation->abroad_error?>
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>Your first UK address where registered with a GP</strong>
				</td>
				<td width="400">
					<textarea class="address enabled-on" data-enabled-on="abroad" data-enabled-value="Yes" cols="5" rows="3" name="abroadfirstaddress"><?=$this->input->post('abroadfirstaddress')?></textarea>
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>If previously resident in UK, <br />date of leaving</strong>
				</td>
				<td width="400">
					<!-- <input type="text" class="leavingdate enabled-on" data-enabled-on="abroad" data-enabled-value="Yes"> -->
					<select name="leavingdate_day" class="enabled-on" data-enabled-on="abroad" data-enabled-value="Yes">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 31; $i++): ?>
						<option <?=($this->input->post('leavingdate_day')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="leavingdate_month" class="enabled-on" data-enabled-on="abroad" data-enabled-value="Yes">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 12; $i++): ?>
						<option <?=($this->input->post('leavingdate_month')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="leavingdate_year" class="enabled-on" data-enabled-on="abroad" data-enabled-value="Yes">
						<option value="" disabled="disabled" selected></option>
						<?php
						$current_year = date('Y');
						$start_year   = $current_year - 100; // 100 years old max

						for($i = $current_year; $i >= $start_year; $i--):
						?>
						<option <?=($this->input->post('leavingdate_year')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Date you first came to live in UK</strong>
				</td>
				<td width="400">
					<!-- <input type="text" name="datearrived" class="datearrived enabled-on" data-enabled-on="abroad" data-enabled-value="Yes"> -->
					<select name="datearrived_day" class="enabled-on" data-enabled-on="abroad" data-enabled-value="Yes">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 31; $i++): ?>
						<option <?=($this->input->post('datearrived_day')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="datearrived_month" class="enabled-on" data-enabled-on="abroad" data-enabled-value="Yes">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 12; $i++): ?>
						<option <?=($this->input->post('datearrived_month')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="datearrived_year" class="enabled-on" data-enabled-on="abroad" data-enabled-value="Yes">
						<option value="" disabled="disabled" selected></option>
						<?php
						$current_year = date('Y');
						$start_year   = $current_year - 100; // 100 years old max

						for($i = $current_year; $i >= $start_year; $i--):
						?>
						<option <?=($this->input->post('datearrived_year')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<?=$this->validation->datearrived_day_error?>
					<?=$this->validation->datearrived_month_error?>
					<?=$this->validation->datearrived_year_error?>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Medical records</legend>
		<p>
			If you have been registered with a GP before, please help us trace your
			previous medical records by providing the following information
		</p>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Your previous address in UK</strong>
				</td>
				<td width="400">
					<textarea cols="5" rows="3" name="previousaddress" class="address"><?=$this->validation->previousaddress;?></textarea>
					<?=$this->validation->previousaddress_error?>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Name of previous doctor while at that address</strong>
				</td>
				<td width="400">
					<input type="text" name="previousdoctor" class="previousdoctor" value="<?=$this->validation->previousdoctor?>">
					<?=$this->validation->previousdoctor_error?>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Address of previous doctor</strong>
				</td>
				<td width="400">
					<textarea cols="5" rows="3" name="previousdoctoraddress" class="address"><?=$this->validation->previousdoctoraddress;?></textarea>
					<?=$this->validation->previousdoctoraddress_error?>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>If you are returning from the Armed Forces</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field opt" width="220">
					<strong>Address before enlisting</strong>
				</td>
				<td width="400">
					<textarea cols="5" rows="3" name="address_before_enlisting" class="address"><?=$this->input->post('address_before_enlisting')?></textarea>
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>Service number</strong>
				</td>
				<td width="400">
					<input type="text" name="service_number" class="service-number" value="<?=$this->input->post('service_number')?>">
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>Enlistment Date</strong>
				</td>
				<td width="400">
					<!-- <input type="text" name="date_enlistment" class="date-enlistment"> -->
					<select name="date_enlistment_day">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 31; $i++): ?>
						<option <?=($this->input->post('date_enlistment_day')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="date_enlistment_month">
						<option value="" disabled="disabled" selected></option>
						<?php for($i = 1; $i <= 12; $i++): ?>
						<option <?=($this->input->post('date_enlistment_month')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
					<select name="date_enlistment_year">
						<option value="" disabled="disabled" selected></option>
						<?php
						$current_year = date('Y');
						$start_year   = $current_year - 100; // 100 years old max

						for($i = $current_year; $i >= $start_year; $i--):
						?>
						<option <?=($this->input->post('date_enlistment_year')==$i)?'selected':''?> value="<?=$i?>"><?=$i?></option>
						<?php endfor; ?>
					</select>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>If you are registering a child under 5</legend>
		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right" width="40">
					<input type="checkbox" name="childregistered" <?=$this->input->post('childregistered') ?'checked="checked"':''?>>&nbsp;
				</td>
				<td class="opt" width="580">
					<strong>
						I wish the child above to be registered with a Doctor from the James
						Wigg Practice for Child Health Surveillance.
					</strong>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>If you need doctor to dispense medicines and appliances</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right" width="40">
					<input type="checkbox" name="chemist_more_than_mile" <?=$this->input->post('chemist_more_than_mile') ?'checked="checked"':''?>>&nbsp;
				</td>
				<td class="opt" width="580">
					<p>
						I live more than 1 mile in a straight line from the nearest chemist.
					</p>
				</td>
			</tr>
			<tr>
				<td align="right" width="20">
					<input type="checkbox" name="chemist_difficulty" <?=$this->input->post('chemist_difficulty') ?'checked="checked"':''?>>&nbsp;
				</td>
				<td class="opt" width="600">
					<p>
						I would have serious difficulty in getting them from a chemist.
					</p>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>NHS Organ Donor registration</legend>

		<p>
			I want to register my details on NHS Organ Donor Register as someone whose
			organs/tissue may be used for transplantation after my death. Please tick
			the boxes that apply.
		</p>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td width="25"></td>
				<td class="opt" width="595">
					<p>
						<label>
							<input type="checkbox" name="organ_donor_any_organs" <?=$this->input->post('organ_donor_any_organs') ?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_kidneys,organ_donor_heart,organ_donor_liver,organ_donor_corneas,organ_donor_lungs,organ_donor_pancreas,organ_donor_any_part" data-enabled-value="">&nbsp;
							Any of my organs and tissue or
						</label>
					</p>
					<p>
						<label>
							<input type="checkbox" name="organ_donor_kidneys" <?=$this->input->post('organ_donor_kidneys')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Kidneys
						</label><br>
						<label>
							<input type="checkbox" name="organ_donor_heart" <?=$this->input->post('organ_donor_heart')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Heart
						</label><br>
						<label>
							<input type="checkbox" name="organ_donor_liver" <?=$this->input->post('organ_donor_liver')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Liver
						</label><br>
						<label>
							<input type="checkbox" name="organ_donor_corneas" <?=$this->input->post('organ_donor_corneas')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Corneas
						</label><br>
						<label>
							<input type="checkbox" name="organ_donor_lungs" <?=$this->input->post('organ_donor_lungs')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Lungs
						</label><br>
						<label>
							<input type="checkbox" name="organ_donor_pancreas" <?=$this->input->post('organ_donor_pancreas')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Pancreas
						</label><br>
						<label>
							<input type="checkbox" name="organ_donor_any_part" <?=$this->input->post('organ_donor_any_part')?'checked="checked"':''?>
								 class="enabled-off" data-enabled-off="organ_donor_any_organs" data-enabled-value="">
							Any part of my body
						</label>
					</p>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>NHS Blood Donor registration</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right" width="40">
					<input type="checkbox" name="blood_donor_confirm" <?=$this->input->post('blood_donor_confirm')?'checked="checked"':''?>>&nbsp;
				</td>
				<td class="opt" width="580">
					<p>
						I would like to join the NHS Blood Donor Register as someone who may
						be contacted and would be prepared to donate blood.
					</p>
				</td>
			</tr>
			<tr>
				<td align="right" width="40">
					<input type="checkbox" name="blood_donor_three_years" <?=$this->input->post('blood_donor_three_years')?'checked="checked"':''?>>&nbsp;
				</td>
				<td class="opt" width="580">
					<p>
						Tick here if you have given blood in the last 3 years
					</p>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Next of Kin</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field req" width="220">
					<strong>* Name of Next of Kin</strong>
				</td>
				<td width="400">
					<input type="text" name="next_kin_name" class="next-kin-name" value="<?=$this->validation->next_kin_name?>">
					<?=$this->validation->next_kin_name_error?>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Relationship to you</strong>
				</td>
				<td width="400">
					<input type="text" name="next_kin_relationship" class="next-kin-relationship" value="<?=$this->validation->next_kin_relationship?>">
					<?=$this->validation->next_kin_relationship_error?>
				</td>
			</tr>
			<tr>
				<td class="field req" width="220">
					<strong>* Phone number</strong>
				</td>
				<td width="400">
					<input type="text" name="next_kin_phone" class="next-kin-phone" value="<?=$this->validation->next_kin_phone?>">
					<?=$this->validation->next_kin_phone_error?>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Children Details</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<?php for($i = 0; $i < 5; $i++): ?>
			<tr>
				<td class="field opt" width="180">
					<strong>Name</strong>
					<input type="text" name="child_name_<?=$i?>" value="<?=$this->input->post('child_name_'.$i)?>" class="child-name" style="width:70%">
				</td>
				<td class="field opt" width="110">
					<strong>Sex</strong>
					<label>
						<input <?=($this->input->post('child_gender_'.$i)=='Male')?'checked="checked"':''?> type="radio" name="child_gender_<?=$i?>" value="Male">
						Male
					</label>
					<label>
						<input <?=($this->input->post('child_gender_'.$i)=='Female')?'checked="checked"':''?> type="radio" name="child_gender_<?=$i?>" value="Female">
						Female
					</label>
				</td>
				<td class="field opt" width="200">
					<strong>Date of Birth</strong>
					<!-- <input type="text" name="child_dob[]" class="child-dob" style="width:60%"> -->
					<select name="child_dob_day_<?=$i?>">
						<option value="" disabled="disabled" selected></option>
						<?php for($j = 1; $j <= 31; $j++): ?>
						<option value="<?=$j?>" <?=($this->input->post('child_dob_day_'.$i)==$j)?'selected':''?>>
							<?=$j?>
						</option>
						<?php endfor; ?>
					</select>
					<select name="child_dob_month_<?=$i?>">
						<option value="" disabled="disabled" selected></option>
						<?php for($j = 1; $j <= 12; $j++): ?>
						<option value="<?=$j?>" <?=($this->input->post('child_dob_month_'.$i)==$j)?'selected':''?>>
							<?=$j?>
						</option>
						<?php endfor; ?>
					</select>
					<select name="child_dob_year_<?=$i?>">
						<option value="" disabled="disabled" selected></option>
						<?php
						$current_year = date('Y');
						$start_year   = $current_year - 100; // 100 years old max

						for($j = $current_year; $j >= $start_year; $j--):
						?>
						<option value="<?=$j?>" <?=($this->input->post('child_dob_year_'.$i)==$j)?'selected':''?>>
							<?=$j?>
						</option>
						<?php endfor; ?>
					</select>
				</td>
			</tr>
			<?php endfor; ?>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>For Children under 16</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td class="field opt" width="220">
					<strong>
						Who looks after you?
					</strong>
				</td>
				<td width="400">
					<p>
						<label>
							<input type="radio" name="under16_guardian" value="Mother" <?=($this->input->post('under16_guardian')=="Mother")?'checked="checked"':''?>>
							Mother
						</label><br>
						<label>
							<input type="radio" name="under16_guardian" value="Father" <?=($this->input->post('under16_guardian')=="Father")?'checked="checked"':''?>>
							Father
						</label><br>
						<label>
							<input type="radio" name="under16_guardian" value="Both Parents" <?=($this->input->post('under16_guardian')=="Both Parents")?'checked="checked"':''?>>
							Both Parents
						</label><br>
						<label>
							<input type="radio" name="under16_guardian" value="Carer" <?=($this->input->post('under16_guardian')=="Carer")?'checked="checked"':''?>>
							Carer
						</label>
					</p>
				</td>
			</tr>
			<tr>
				<td class="field opt" width="220">
					<strong>Which school do you go to?</strong>
				</td>
				<td width="400">
					<input type="text" name="under16_school" class="under16-school" value="<?=$this->input->post('under16_school')?>">
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Type of Accommodation*</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td width="620">
					<p>
						<label>
							<input type="radio" name="accommodation" value="Owner occupier" <?=($this->validation->accommodation=="Owner occupier")?'checked="checked"':''?>>
							Owner occupier
						</label>&nbsp;
						<label>
							<input type="radio" name="accommodation" value="Council rented" <?=($this->validation->accommodation=="Council rented")?'checked="checked"':''?>>
							Council rented
						</label>&nbsp;
						<label>
							<input type="radio" name="accommodation" value="Private rental" <?=($this->validation->accommodation=="Private rental")?'checked="checked"':''?>>
							Private rental
						</label>&nbsp;
						<label>
							<input type="radio" name="accommodation" value="Hostel" <?=($this->validation->accommodation=="Hostel")?'checked="checked"':''?>>
							Hostel
						</label>&nbsp;
						<label>
							<input type="radio" name="accommodation" value="Temporary" <?=($this->validation->accommodation=="Temporary")?'checked="checked"':''?>>
							Temporary
						</label>&nbsp;
						<label>
							<input type="radio" name="accommodation" value="Homeless" <?=($this->validation->accommodation=="Homeless")?'checked="checked"':''?>>
							Homeless
						</label><br>
						<label>
							<input type="radio" name="accommodation" value="Care Home" <?=($this->validation->accommodation=="Care Home")?'checked="checked"':''?>>
							Care Home
						</label>
					</p>
					<?=$this->validation->accommodation_error?>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Occupation*</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tr>
				<td width="620">
					<p>
						<label>
							<input type="radio" name="occupation" value="Employed" <?=($this->validation->occupation=="Employed")?'checked="checked"':''?>>
							Employed
						</label>&nbsp;
						<label>
							<input type="radio" name="occupation" value="Unemployed" <?=($this->validation->occupation=="Unemployed")?'checked="checked"':''?>>
							Unemployed
						</label>&nbsp;
						<label>
							<input type="radio" name="occupation" value="Retired" <?=($this->validation->occupation=="Retired")?'checked="checked"':''?>>
							Retired
						</label>&nbsp;
						<label>
							<input type="radio" name="occupation" value="Long term sick benefit" <?=($this->validation->occupation=="Long term sick benefit")?'checked="checked"':''?>>
							Long term sick benefit
						</label>&nbsp;
						<label>
							<input type="radio" name="occupation" value="Student" <?=($this->validation->occupation=="Student")?'checked="checked"':''?>>
							Student
						</label>
					</p>
					<?=$this->validation->occupation_error?>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Height &amp; Weight</legend>
		<table width="620" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td class="field req" width="220">
						<strong>How tall are you*</strong>
					</td>
					<td width="600">
						<input type="text" name="height" value="<?=$this->validation->height?>">
						<select name="height_units">
							<option <?=($this->input->post('height_units')=='metres')?'selected':''?> value="metres">Metres</option>
							<option <?=($this->input->post('height_units')=='feet')?'selected':''?> value="feet">Feet</option>
						</select>
						<?=$this->validation->height_error?>
					</td>
				</tr>
				<tr>
					<td class="field req" width="220">
						<strong>How much do you weigh*</strong>
					</td>
					<td width="600">
						<input type="text" name="weight" value="<?=$this->validation->weight?>">
						<select name="weight_units">
							<option <?=($this->input->post('weight_units')=='kilograms')?'selected':''?> value="kilograms">Kilograms</option>
							<option <?=($this->input->post('weight_units')=='pounds')?'selected':''?> value="pounds">Pounds</option>
						</select>
						<?=$this->validation->weight_error?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Carer Details</legend>
		<table width="620" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td class="field req" width="220">
						<strong>Are you a carer? *</strong>
					</td>
					<td width="600">
            <label>
  						<input name="is_carer" type="radio" value="Yes" <?=($this->validation->is_carer=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
  						<input name="is_carer" type="radio" value="No" <?=($this->validation->is_carer=="No")?'checked="checked"':''?>>
              No
            </label>
						<?=$this->validation->is_carer_error?>

						<p class="opt">
							If Yes, who do you care for?<br/>
							<input name="carer_for" type="text" value="<?=$this->input->post('carer_for')?>" class="enabled-on" data-enabled-on="is_carer" data-enabled-value="Yes">
							<?=$this->validation->carer_for_error?>
						</p>
					</td>
				</tr>
				<tr>
					<td class="field req" width="220">
						<strong>Do you have a carer? *</strong>
					</td>
					<td width="600">
            <label>
  						<input name="has_carer" type="radio" value="Yes" <?=($this->validation->has_carer=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
  						<input name="has_carer" type="radio" value="No" <?=($this->validation->has_carer=="No")?'checked="checked"':''?>>
              No
            </label>
						<?=$this->validation->has_carer_error?>

						<p class="opt">
							If Yes, Carer's Name:<br/>
							<input name="carer_name" type="text" value="<?=$this->input->post('carer_name')?>" class="enabled-on" data-enabled-on="has_carer" data-enabled-value="Yes">
							<?=$this->validation->carer_name_error?>
						</p>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>Ethnic Group</legend>

		<table width="620" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td class="field req" width="220">
						<strong>What best describes you?*</strong>
					</td>
					<td width="400">
						<p>
							<select name="ethnicity">
								<option value="" disabled selected>Select an option</option>
								<option <?=($this->validation->ethnicity=='White British')?'selected':''?>>White British</option>
								<option <?=($this->validation->ethnicity=='White Irish')?'selected':''?>>White Irish</option>
								<option <?=($this->validation->ethnicity=='Any other white background')?'selected':''?>>Any other white background</option>
								<option <?=($this->validation->ethnicity=='Black African')?'selected':''?>>Black African</option>
								<option <?=($this->validation->ethnicity=='Black Caribbean')?'selected':''?>>Black Caribbean</option>
								<option <?=($this->validation->ethnicity=='Mixed white and Black Caribbean')?'selected':''?>>Mixed white and Black Caribbean</option>
								<option <?=($this->validation->ethnicity=='Mixed white and Black African')?'selected':''?>>Mixed white and Black African</option>
								<option <?=($this->validation->ethnicity=='Mixed white and Asian')?'selected':''?>>Mixed white and Asian</option>
								<option <?=($this->validation->ethnicity=='Any other mixed background')?'selected':''?>>Any other mixed background</option>
								<option <?=($this->validation->ethnicity=='Indian or British Indian')?'selected':''?>>Indian or British Indian</option>
								<option <?=($this->validation->ethnicity=='Pakistani or British Pakistani')?'selected':''?>>Pakistani or British Pakistani</option>
								<option <?=($this->validation->ethnicity=='Bangladeshi or British Bangladeshi')?'selected':''?>>Bangladeshi or British Bangladeshi</option>
								<option <?=($this->validation->ethnicity=='Any other Asian background')?'selected':''?>>Any other Asian background</option>
								<option <?=($this->validation->ethnicity=='Chinese')?'selected':''?>>Chinese</option>
								<option <?=($this->validation->ethnicity=='other')?'selected':''?> value="other">Any other ethnic group</option>
							</select>
						</p>
						<p class="opt display-on" data-display-on="ethnicity" data-display-value="other">
							Please provide a description<br/>
							<input type="text" name="ethnicity_other_description" value="<?=$this->input->post('ethnicity_other_description')?>">
						</p>
						<?=$this->validation->ethnicity_error?>
						<?=$this->validation->ethnicity_other_description_error?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>

	<fieldset class="fieldset">
		<legend>General Questions</legend>
		<table width="620" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td class="field req" width="220">
						<strong>What is the first or main language that you speak*</strong>
					</td>
					<td width="400">
						<input type="text" name="first_language" value="<?=$this->validation->first_language?>">
						<?=$this->validation->first_language_error?>
					</td>
				</tr>
				<tr>
					<td class="field req" width="220">
						<strong>Do you ever need an interpreter *</strong>
					</td>
					<td width="400">
            <label>
              <input type="radio" name="interpreter_required" value="Yes" <?=($this->validation->interpreter_required=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
              <input type="radio" name="interpreter_required" value="No" <?=($this->validation->interpreter_required=="No")?'checked="checked"':''?>>
              No
            </label>
						<?=$this->validation->interpreter_required_error?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>

  <fieldset class="fieldset">
    <legend>Medical History</legend>
    <table width="620" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td class="field opt" style="text-align:left" colspan="4">
            <strong>
              Have you had any other Long Term Condition, Illness, Accident or
              Operation
            </strong>
          </td>
        </tr>

        <?php for($i = 0; $i < 4; $i++): ?>
        <tr>
          <td width="20"><?=($i + 1)?>.</td>
          <td width="200">
            Condition/Operation<br/>
            <input type="text" name="history_condition_<?=$i?>" value="<?=$this->input->post('history_condition_'.$i)?>">
          </td>
          <td width="200">
            Date/Year<br/>
						<select name="history_date_day_<?=$i?>">
							<option value="" disabled="disabled" selected></option>
							<?php for($j = 1; $j <= 31; $j++): ?>
							<option value="<?=$j?>" <?=($this->input->post('history_date_day_'.$i)==$j)?'selected':''?>>
								<?=$j?>
							</option>
							<?php endfor; ?>
						</select>
						<select name="history_date_month_<?=$i?>">
							<option value="" disabled="disabled" selected></option>
							<?php for($j = 1; $j <= 12; $j++): ?>
							<option value="<?=$j?>" <?=($this->input->post('history_date_month_'.$i)==$j)?'selected':''?>>
								<?=$j?>
							</option>
							<?php endfor; ?>
						</select>
						<select name="history_date_year_<?=$i?>">
							<option value="" disabled="disabled" selected></option>
							<?php
							$current_year = date('Y');
							$start_year   = $current_year - 100; // 100 years old max

							for($j = $current_year; $j >= $start_year; $j--):
							?>
							<option value="<?=$j?>" <?=($this->input->post('history_date_year_'.$i)==$j)?'selected':''?>>
								<?=$j?>
							</option>
							<?php endfor; ?>
						</select>
          </td>
          <td width="200">
            Hospital<br/>
            <input type="text" name="history_hospital_<?=$i?>" value="<?=$this->input->post('history_hospital_'.$i)?>">
          </td>
        </tr>
        <?php endfor; ?>

      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Allergies</legend>
    <table width="620" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td class="field req" width="220">
            <strong>
              * Are you allergic to any medication<br/>(such as Penicillin or Aspirin)
            </strong>
          </td>
          <td width="400">
            <label>
              <input type="radio" name="known_allergies" value="Yes" <?=($this->validation->known_allergies=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
              <input type="radio" name="known_allergies" value="No" <?=($this->validation->known_allergies=="No")?'checked="checked"':''?>>
              No
            </label>
            <label>
              <input type="radio" name="known_allergies" value="Not Sure" <?=($this->validation->known_allergies=="Not Sure")?'checked="checked"':''?>>
              Not Sure
            </label>

						<?=$this->validation->known_allergies_error?>

	          <p class="opt">
	            If Yes, please give details<br/>
	            <textarea cols="50" name="allergy_details" class="enabled-on" data-enabled-on="known_allergies" data-enabled-value="Yes"><?=$this->input->post('allergy_details')?></textarea>
	          	<?=$this->validation->allergy_details_error?>
	          </p>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Smoking</legend>
    <table width="620" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td class="field req" width="220">
            <strong>
              * Do you smoke now
            </strong>
          </td>
          <td width="400">
            <label>
              <input type="radio" name="current_smoker" value="Yes" <?=($this->validation->current_smoker=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
              <input type="radio" name="current_smoker" value="No" <?=($this->validation->current_smoker=="No")?'checked="checked"':''?>>
              No
            </label>

						<?=$this->validation->current_smoker_error?>

						<p class="opt">
	          	If Yes, how many per day and what kind?<br/>
	            <textarea cols="50" name="current_smoker_details" class="enabled-on" data-enabled-on="current_smoker" data-enabled-value="Yes"><?=$this->input->post('current_smoker_details')?></textarea>
            </p>
						<?=$this->validation->current_smoker_details_error?>
          </td>
        </tr>
        <tr>
          <td class="field req" width="220">
            <strong>
              * Did you smoke in the past
            </strong>
          </td>
          <td width="400">
            <label>
              <input type="radio" name="past_smoker" value="Yes" <?=($this->validation->past_smoker=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
              <input type="radio" name="past_smoker" value="No" <?=($this->validation->past_smoker=="No")?'checked="checked"':''?>>
              No
            </label>
						<?=$this->validation->past_smoker_error?>

	          <p class="opt">
	          	If Yes, how long?<br/>
	            <textarea cols="50" name="past_smoker_details" class="enabled-on" data-enabled-on="past_smoker" data-enabled-value="Yes"><?=$this->input->post('past_smoker_details')?></textarea>
							<?=$this->validation->past_smoker_details_error?>
	          </p>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Alcohol</legend>
    <table width="620" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td class="field req" width="220">
            <strong>* How often do you have an alcoholic drink?</strong>
          </td>
          <td width="400">
          	<p>
	            <label>
	              <input type="radio" name="alcohol_frequency" value="Never" <?=($this->validation->alcohol_frequency=="Never")?'checked="checked"':''?>>
	              Never
	            </label><br/>
	            <label>
	              <input type="radio" name="alcohol_frequency" value="Monthly or less" <?=($this->validation->alcohol_frequency=="Monthly or less")?'checked="checked"':''?>>
	              Monthly or less
	            </label><br/>
	            <label>
	              <input type="radio" name="alcohol_frequency" value="2-4 times a month" <?=($this->validation->alcohol_frequency=="2-4 times a month")?'checked="checked"':''?>>
	              2-4 times a month
	            </label><br/>
	            <label>
	              <input type="radio" name="alcohol_frequency" value="2-3 times a week" <?=($this->validation->alcohol_frequency=="2-3 times a week")?'checked="checked"':''?>>
	              2-3 times a week
	            </label><br/>
	            <label>
	              <input type="radio" name="alcohol_frequency" value="4 or more times a week" <?=($this->validation->alcohol_frequency=="4 or more times a week")?'checked="checked"':''?>>
	              4 or more times a week
	            </label>

							<?=$this->validation->alcohol_frequency_error?>
	          </p>
          </td>
        </tr>
        <tr>
          <td class="field req" width="220">
            <strong>* How many drinks/units per session?</strong>
          </td>
          <td width="400">
          	<p>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never" type="radio" name="alcohol_per_session" value="1 or 2" <?=($this->validation->alcohol_per_session=="1 or 2")?'checked="checked"':''?>>
	              1 or 2
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_per_session" value="3 or 4" <?=($this->validation->alcohol_per_session=="3 or 4")?'checked="checked"':''?>>
	              3 or 4
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_per_session" value="5 or 6" <?=($this->validation->alcohol_per_session=="5 or 6")?'checked="checked"':''?>>
	              5 or 6
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_per_session" value="7 or 9" <?=($this->validation->alcohol_per_session=="7 or 9")?'checked="checked"':''?>>
	              7 or 9
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_per_session" value="10 or more" <?=($this->validation->alcohol_per_session=="10 or more")?'checked="checked"':''?>>
	              10 or more
	            </label>

							<?=$this->validation->alcohol_per_session_error?>
            </p>
          </td>
        </tr>
        <tr>
          <td class="field req" width="220">
            <strong>
              * How often do you have 6 or more standard drinks on one occasions?
            </strong>
          </td>
          <td width="400">
          	<p>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_max_per_session" value="Never" <?=($this->validation->alcohol_max_per_session=="Never")?'checked="checked"':''?>>
	              Never
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_max_per_session" value="Less than Monthly" <?=($this->validation->alcohol_max_per_session=="Less than Monthly")?'checked="checked"':''?>>
	              Less than Monthly
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_max_per_session" value="Weekly" <?=($this->validation->alcohol_max_per_session=="Weekly")?'checked="checked"':''?>>
	              Weekly
	            </label><br/>
	            <label>
	              <input class="enabled-off" data-enabled-off="alcohol_frequency" data-enabled-value="Never"  type="radio" name="alcohol_max_per_session" value="Daily or almost daily" <?=($this->validation->alcohol_max_per_session=="Daily or almost daily")?'checked="checked"':''?>>
	              Daily or almost daily
	            </label>

							<?=$this->validation->alcohol_max_per_session_error?>
	           </p>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Your Family's Health</legend>
    <table width="620" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td class="field req" width="220">
            <strong>
              Has anyone in your family had a heart attack or angina*
            </strong>
          </td>
          <td width="400">
            <label>
              <input type="radio" name="family_heart_attack_or_angina" value="Yes" <?=($this->validation->family_heart_attack_or_angina=="Yes")?'checked="checked"':''?>>
              Yes
            </label>
            <label>
              <input type="radio" name="family_heart_attack_or_angina" value="No" <?=($this->validation->family_heart_attack_or_angina=="No")?'checked="checked"':''?>>
              No
            </label>
            <?=$this->validation->family_heart_attack_or_angina_error?>
          </td>
        </tr>
        <tr>
          <td class="field opt" width="220">
            <strong>
              If Yes, Please Tick*
            </strong>
          </td>
          <td width="400">
            <p>
              <label>
                <input type="checkbox" name="family_heart_attack_or_angina_mother" class="enabled-on" data-enabled-on="family_heart_attack_or_angina" data-enabled-value="Yes" <?=$this->input->post('family_heart_attack_or_angina_mother')?'checked="checked"':''?>>
                Mother
              </label>
              <label>
                <input type="checkbox" name="family_heart_attack_or_angina_father" class="enabled-on" data-enabled-on="family_heart_attack_or_angina" data-enabled-value="Yes" <?=$this->input->post('family_heart_attack_or_angina_father')?'checked="checked"':''?>>
                Father
              </label>
              <label>
                <input type="checkbox" name="family_heart_attack_or_angina_sister" class="enabled-on" data-enabled-on="family_heart_attack_or_angina" data-enabled-value="Yes" <?=$this->input->post('family_heart_attack_or_angina_sister')?'checked="checked"':''?>>
                Sister
              </label>
              <label>
                <input type="checkbox" name="family_heart_attack_or_angina_brother" class="enabled-on" data-enabled-on="family_heart_attack_or_angina" data-enabled-value="Yes" <?=$this->input->post('family_heart_attack_or_angina_brother')?'checked="checked"':''?>>
                Brother
              </label>
              <label>
                <input type="checkbox" name="family_heart_attack_or_angina_other" class="enabled-on" value="Other" data-enabled-on="family_heart_attack_or_angina" data-enabled-value="Yes" <?=$this->input->post('family_heart_attack_or_angina_other')?'checked="checked"':''?>>
                Other
              </label>
              <?php
              if($this->validation->family_heart_attack_or_angina_mother_error
              	&& $this->validation->family_heart_attack_or_angina_father_error
              	&& $this->validation->family_heart_attack_or_angina_brother_error
              	&& $this->validation->family_heart_attack_or_angina_sister_error
              	&& $this->validation->family_heart_attack_or_angina_other_error): ?>
              	<div class="error">The 'Please Tick*' field must have a value.</div>
              <?php endif; ?>
            </p>
            <p class="opt">
              If Other, please specify<br/>
              <input type="text" name="family_heart_attack_or_angina_other_description" class="enabled-on" data-enabled-on="family_heart_attack_or_angina_other" data-enabled-value="" value="<?=$this->input->post('family_heart_attack_or_angina_other_description')?>">
            	<?=$this->validation->family_heart_attack_or_angina_other_description_error?>
            </p>
          </td>
        </tr>
        <tr>
          <td class="field req" width="220">
            <strong>
              Have members of your family suffered from any other health problems*
            </strong>
          </td>
          <td width="400">
            <p>
              <label>
                <input type="radio" name="family_other_health_problems" value="Yes" <?=($this->validation->family_other_health_problems=="Yes")?'checked="checked"':''?>>
                Yes
              </label>
              <label>
                <input type="radio" name="family_other_health_problems" value="No" <?=($this->validation->family_other_health_problems=="No")?'checked="checked"':''?>>
                No
              </label>
            </p>
            <?=$this->validation->family_other_health_problems_error?>
            <p class="opt">
              If Yes, please specify<br/>
              <input type="text" name="family_other_health_problems_member" class="enabled-on" data-enabled-on="family_other_health_problems" data-enabled-value="Yes" value="<?=$this->input->post('family_other_health_problems_member')?>">
            	<?=$this->validation->family_other_health_problems_member_error?>
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Proof of ID &amp; Address</legend>
    <table width="620" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td class="field opt" width="220">
            <strong>
              Please attach a scanned copy of your Proof of Address
            </strong>
          </td>
          <td width="400">
            <input type="file" name="proof_of_address">
            <p>
              <small>
                Valid proof of address include Bank &amp; Utility Statement,
                Tenancy Agreement, Council Letters, Pensions and Benefit letters
                from DWP. Mobile Phone bills are not valid
              </small>
            </p>
          </td>
        </tr>
        <tr>
          <td class="field opt" width="220">
            <strong>
              Please attach a scanned copy of your Proof of ID
            </strong>
          </td>
          <td width="400">
            <input type="file" name="proof_of_id">
            <p>
              <small>
                Valid proof of address include Passport, Driving licence, ID Card
              </small>
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Where did you hear about us</legend>
    <table width="620" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td width="620">
            <textarea cols="75" rows="7" name="hear_about"><?=$this->input->post('hear_about')?></textarea>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

  <fieldset class="fieldset">
    <legend>Final Declaration*</legend>
    <table width="620" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td align="right" width="40">
            <input type="checkbox" name="confirm_accuracy" <?=$this->input->post('confirm_accuracy')?'checked="checked"':''?>>
            &nbsp;
          </td>
          <td width="580">
						<?=$this->validation->confirm_accuracy_error?>
            <p>
	            I acknowledge that all the information provided is accurate to the
	            best of my knowledge. By ticking this option, I take responsibility
	            and consent to the practice registering me as a regular patient
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  </fieldset>

	<table width="620" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td width="220"></td>
				<td>
					<button type="submit">Submit form</button>
				</td>
			</tr>
		</tbody>
	</table>
</form>

