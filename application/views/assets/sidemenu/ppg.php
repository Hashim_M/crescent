<?php
$sections = array(
	array('href' => '',				 'name' => 'Home'),
	array('href' => 'links',	 'name' => 'Links'),
	array('href' => 'minutes', 'name' => 'Minutes'),
	array('href' => 'members', 'name' => 'Members page')
);

foreach($sidenav as $menu) {
	$sections[] = array(
    'href' => $menu->url_title,
    'name' => $menu->title
  );
}
?>
<div id="sidemenu">
  <ul>
    <?php display_sidenav($sections); ?>
  </ul>
</div>
<?php
