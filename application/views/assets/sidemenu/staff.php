<?php
$sections = array();

if ($sidenav->num_rows() > 0) {
	foreach($sidenav->result() as $menu) {
		$sections[] = array(
      'href' => $menu->url_title,
      'name' => $menu->title
    );
	}
  ?>
  <div id="sidemenu">
    <ul>
      <?php display_sidenav($sections); ?>
    </ul>
  </div>
  <?php
}
