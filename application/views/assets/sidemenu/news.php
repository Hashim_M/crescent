<?php
// Site navigation
$sections = array(
	array('href' => '',	'name' => 'Latest news'),
);

// foreach($nav_items->result() as $nav_item){
//	array_push($sections, array('href' => 'news/' . $nav_item->year. '/' . $nav_item->month . '/', 'name' => $nav_item->monthname . ' ' . $nav_item->year));
// }
?>
<div id="sidemenu">
	<ul>
		<?php display_sidenav($sections); ?>

		<?php if ($nav_items->num_rows() > 0) : ?>

		<?php foreach($nav_items->result() as $nav_item) : ?>
		<li>
			<a href="/news/<?=$nav_item->year?>/<?=$nav_item->month?>/" <?php if(isset($year) && isset($month) && $nav_item->year == $year && $nav_item->month == $month) : ?> class="active"<?php endif; ?>>
				<?=$nav_item->monthname?> <?=$nav_item->year?>
			</a>
		</li>
		<?php endforeach; ?>

		<?php endif; ?>
	</ul>
</div>
