<?php
// Site navigation
$sections = array(
	array('href' => '',						 'name' => 'Location'),
	array('href' => 'information', 'name' => 'Email and contact numbers'),
);
?>
<div id="sidemenu">
  <ul>
    <?php display_sidenav($sections); ?>
  </ul>
</div>
