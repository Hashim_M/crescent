<?php
$sections = array();

if ($sidenav->num_rows() > 0) {
	foreach($sidenav->result() as $menu) {
		$sections[] = array(
			'href' => $menu->url_title,
			'name' => $menu->title
		);
	}
	?>
	<div id="sidemenu">
		<ul>
			<?php if (sizeof($sections) > 0) : ?>
			<?php foreach($sections as $nav_item) : ?>
			<li>
				<a href="/patient_services/<?=$nav_item['href'];?>" <?php if(isset($page_name) && $nav_item['name'] == $page_name) : ?> class="active" <?php endif; ?> >
					<?=$nav_item['name']?>
				</a>
			</li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
<?php
}
