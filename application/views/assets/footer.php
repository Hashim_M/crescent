  		<div id="footer">
  			<div class="col1">
  				<p>
            &copy; <?=date('Y')?> Queens Crescent Practice.<br />
  				  All rights Reserved<br />
  				  76 Queens Crescent,<br />
  				  London NW5 4EB
          </p>
  			</div>
  			<div class="col2">
  				<ul>
  					<li><a href="/contact">Contact Us</a></li>
  					<li><a href="/disclaimer">Disclaimer</a></li>
  				</ul>
  			</div>
  			<div class="col3">
  				<p>
            Photography by Timothy Soar and Rob Parrish
          </p>
  				<p>
            Design by <a href="http://boilerhouse.co.uk">Boilerhouse</a>
          </p>
  			</div>
  		</div>
  	</div>

    <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-23689795-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s  = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
  </body>
</html>
