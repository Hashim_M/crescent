<div id="flowpanes">
	<div class="items">
		<?php foreach($slides  as $slide):?>
			<div class="slide fp-<?=($slide->slide_position != 1) ? 'left': 'right' ?>" style="background-image: url(/static/img/sections/<?=$slide->slide_image;?>)">
				<div class="section slider-<?=($slide->slide_position == 1) ? 'left': 'right' ?>">
					<div class="summary">
						<h1><?=$slide->slide_title;?></h1>
						<p><?=$slide->slide_content;?></p>
					</div>
				</div>
			</div>
		<?php endforeach;?>
	</div>
</div>

<ul id="flowtabs">
	<li><a id="t1" href="#introduction">Introduction</a></li>
	<li><a id="t2" href="#register">Register</a></li>
	<li><a id="t3" href="#prescriptions">Prescriptions</a></li>
	<li><a id="t4" href="#results">Results</a></li>
	<li><a id="t5" href="#appointments">Appointments</a></li>
</ul>

<script type="text/javascript">
$(function() {

	$("#flowpanes").scrollable({
			size: 1
		})
		.circular()
		.navigator({
			navi: "#flowtabs",
			naviItem: 'a',
			activeClass: 'current'
		});
});
</script>
