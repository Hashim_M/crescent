<?php
// Site navigation
$sections = array(
	array('href' => 'home',		'name' => 'Home'),
	array('href' => 'practice_info',	'name' => 'Practice info'),
	array('href' => 'patient_services',	'name' => 'Patient services'),
	array('href' => 'ppg',				'name' => 'PPG'),
	array('href' => 'staff',			'name' => 'Staff'),
	array('href' => 'news',				'name' => 'News'),
	array('href' => 'awards',			'name' => 'Awards'),
	array('href' => 'links',			'name' => 'Links'),
	array('href' => 'contact',			'name' => 'Contact')
);

// Work out selected menu item
$href = ($this->uri->segment(2) ? $this->uri->segment(2) : 'dashboard');
?>
<div id="nav">
<ul>
<?php foreach($sections as $section) : ?>
	<li><a href="/admin/<?=$section['href']?>"<?php if ($section['href'] == $href) : ?> class="active"<?php endif; ?>><?=$section['name']?></a></li>
<?php endforeach; ?>
</ul>
</div>