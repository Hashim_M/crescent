<?=$this->validation->title_error;?>
<p><label>Title:<br /><input type="text" class="text" size="88" name="title" value="<?=$this->validation->title;?>" /></label></p>

<?=$this->validation->description_error;?>
<p><label>Description:<br /><input type="text" class="text" size="88" name="description" value="<?=$this->validation->description;?>" /></label></p>

<?=$this->validation->hyperlink_error;?>
<p><label>Hyperlink:<br /><input type="text" class="text" size="88" name="hyperlink" value="<?=$this->validation->hyperlink;?>" /></label></p>


<p><a class="thickbox" href="/admin/links/upload/?keepThis=true&TB_iframe=true&height=550&width=980&modal=false">Thumbnail</a>:</p>

		
<?php if (empty($this->validation->thumbnail_filepath)) : ?>

<div id="preview"><img src="/static/img/blank.gif" /></div>

<?php else : ?>

<div id="preview"><img src="<?=$this->validation->thumbnail_filepath?>" /> <span class="del"><a href="#" onclick="deleteValue(); return false;">Delete</a></span></div>

<?php endif; ?>
	
	
<p><input type="hidden" name="thumbnail_file" value="<?=$this->validation->thumbnail_file?>" /></p>
<p><input type="hidden" name="thumbnail_filepath" value="<?=$this->validation->thumbnail_filepath?>" /></p>
	
<script>

function thumbnail(filename)
{
	document.forms[0].thumbnail_file.value = filename;
	document.forms[0].thumbnail_filepath.value = '/tmp/' + filename;
	
	jQuery("#preview").html('<img src="/tmp/' + filename + '" /> <span class="del"><a href="#" onclick="deleteValue(); return false;">Delete</a></span>');
}

function deleteValue()
{
	document.forms[0].thumbnail_file.value = '';
	document.forms[0].thumbnail_filepath.value = '';
	
	jQuery("#preview").html('<img src="/static/img/blank.gif" />');	
}
</script>	



