<?=($this->uri->segment(4)? '<h3>Slider updated</h3>' : '');?>

<?=$this->validation->slide_title_error;?>
<p>Slider title:<br /><input type="text" style="width: 100%;" class="text" id="slide_title" name="slide_title" value="<?=$this->validation->slide_title;?>" /></p>

<?=$this->validation->slide_position_error;?>
<p>
	Text position:<br />
	<label><input type="radio" id="slide_position" name="slide_position" value="1" <?=set_checkbox('slide_position', 1, true);?> /> Left</label><br />
	<label><input type="radio" id="slide_position" name="slide_position" value="2" <?=set_checkbox('slide_position', 2, true);?> /> Right</label>
</p>

<?=$this->validation->slide_content_error;?>
<p>Slider text:<br /><textarea cols="70" rows="15" class="xinha" style="width: 620px;" id="slide_content" name="slide_content"><?=$this->validation->slide_content;?></textarea></p>

<?=!empty($error)? $error : '';?>
<p>
	Current slider Image:<br />
	<img src="/static/img/sections/<?=$slide->slide_image;?>" style="max-width: 100%;"/>
</p>
<p>
	Change image <input type="file" name="slide_image" /> (max dimensions: 900x277)
</p>
