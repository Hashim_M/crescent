<?=$this->validation->title_error;?>
<p><label>Title:<br /><input type="text" class="text" size="88" name="title" value="<?=$this->validation->title?>" /></label></p>

<?=$this->validation->summary_error?>
<p>Summary: <br /><textarea cols="70" rows="5" style="width: 620px;" id="summary" name="summary"><?=$this->validation->summary;?></textarea></p>

<?=$this->validation->body_error?>
<p>Body:<br /><textarea cols="70" rows="30" class="xinha" style="width: 620px; display: none;" id="body" name="body"><?=$this->validation->body?></textarea></p>

<p>Publish status: <br />
<select name="post_status" class="poststatus">
<option value="draft" <?=$this->validation->set_select('post_status', 'draft');?>>Draft</option>
<option value="pending" <?=$this->validation->set_select('post_status', 'pending');?>>Pending</option>
<option value="publish" <?=$this->validation->set_select('post_status', 'publish');?>>Publish</option>
</select></p>

<p><a href="#" class="advanced" onclick="Effect.toggle('url_title_input','blind'); return false;">SEO URL</a></p>

<div class="advanced_input" id="url_title_input"<?php if (empty($this->validation->url_title_error)) : ?> style="display: none;"<?php endif; ?>>
	
	<?=$this->validation->url_title_error?>
	<p><label>URL Title: (Generated automatically if empty)<br /><input type="text" class="text" size="84" name="url_title" value="<?=$this->validation->url_title?>" /></label></p>

</div>