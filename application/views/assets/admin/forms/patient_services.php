<?=$this->validation->title_error?>
<p><label>Title:<br /><input type="text" class="text" size="88" name="title" value="<?=$this->validation->title?>" /></label></p>

<?=$this->validation->body_error?>
<p>Body:<br />
<textarea cols="70" rows="30" class="xinha" style="width: 620px; display: none;" id="body" name="body"><?=$this->validation->body?></textarea></p>

<p><a href="#" class="advanced" onclick="Effect.toggle('url_title_input','blind'); return false;">SEO URL</a></p>
<div class="advanced_input" id="url_title_input"<?php if (empty($this->validation->url_title_error)) : ?> style="display: none;"<?php endif; ?>>
	
	<?=$this->validation->url_title_error?>
	<p><label>URL Title: (Generated automatically if empty)<br /><input type="text" class="text" size="84" name="url_title" value="<?=$this->validation->url_title?>" /></label></p>

</div>

<p><a href="#" class="advanced" onclick="Effect.toggle('position_input','blind'); return false;">Menu Position</a></p>
<div class="advanced_input" id="position_input"<?php if (empty($this->validation->position_error)) : ?> style="display: none;"<?php endif; ?>>
	
	<?=$this->validation->position_error?>
	<p><label>Index:<br /><input type="text" class="text" size="5" name="position" value="<?=$this->validation->position?>" /></label></p>

</div> 

<p><a href="#" class="advanced" onclick="Effect.toggle('display_sidebar_input','blind'); return false;">Show sidebar</a></p>
<div class="advanced_input" id="display_sidebar_input"<?php if (empty($this->validation->display_sidebar_error)) : ?> style="display: none;"<?php endif; ?>>
	
	<?=$this->validation->display_sidebar_error?>
	<p><label>Display: <select name="display_sidebar">
		<option value="1" <?=$this->validation->set_select('display_sidebar', '1')?>>Yes</option>
		<option value="0" <?=$this->validation->set_select('display_sidebar', '0')?>>No</option>
	</select></label></p>

</div> 


<p><a href="#" class="advanced" onclick="Effect.toggle('banners_input','blind'); return false;">Banners displayed</a></p>
<div class="advanced_input" id="banners_input" style="display: none;">
	
<p>	
<?php foreach($banners->result() as $banner) : ?>
	<label><input type="checkbox" name="banners[]" value="<?=$banner->id?>" <?=$this->validation->set_checkbox('banners', $banner->id)?> /> <?=$banner->title?></label><br />
<?php endforeach; ?>
</p>

</div> 