<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function create_xinha($textareas = array())
{
	ob_start(); // Turn on output buffering
// print_r('expression');exit;
?>
<script type="text/javascript">
_editor_url  = "/xinha/";
_editor_lang = "en";
</script>
<script type="text/javascript" src="/xinha/XinhaCore.js"></script>
<script type="text/javascript">
xinha_editors = null;
xinha_init    = null;
xinha_config  = null;
xinha_plugins = null;

xinha_init = xinha_init ? xinha_init : function()
{
  
	xinha_editors = xinha_editors ? xinha_editors :
	[
	<?php echo "'" . join("','", $textareas) . "'"; ?>
	];
  
	xinha_plugins = xinha_plugins ? xinha_plugins :
	[
	'DoubleClick',
	'ExtendedFileManager',
	'CSS',
	'PasteText'
	];
  
	 // THIS BIT OF JAVASCRIPT LOADS THE PLUGINS, NO TOUCHING  :)
	 if(!Xinha.loadPlugins(xinha_plugins, xinha_init)) return;


	xinha_config = xinha_config ? xinha_config() : new Xinha.Config();
   
	xinha_config.toolbar =
	[
		// "popupeditor"
		["htmlmode","separator","formatblock","bold","italic","separator",
		"insertorderedlist","insertunorderedlist","separator","inserttable",
		"toggleborders", "separator","createlink","insertimage"], 
		(Xinha.is_gecko ? ["pastetext","separator"] : ["copy","paste","pastetext","separator"]),["undo"]
	];
	  
	xinha_config.formatblock =
	{
	"&mdash; format &mdash;": "",
	"Heading 1": "h1",
	"Heading 2": "h2",
	"Heading 3": "h3",
	"Normal"   : "p"
	};
      
	xinha_config.pageStyleSheets = [ "/xinha/textarea.css" ];
    
	xinha_config.cssPluginConfig =
	{
	combos : [
		{ label: "CSS",
		options: { 
					"None"           		: "",
					"Float right"     		: "right",
					"Float left"     		: "left"
				 }
		}
	]
	};

	xinha_config.browserQuirksMode = true;
	xinha_config.showLoading = false;
	xinha_config.statusBar = false;
	xinha_config.stripBaseHref = true;
	xinha_config.baseHref = "<?php echo 'http://'. $_SERVER['HTTP_HOST'] ?>";
	
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/xinha/contrib/php-xinha.php'; ?>
   
	xinha_config.ExtendedFileManager.use_linker = true;
	  
	if (xinha_config.ExtendedFileManager) 
	{
		with (xinha_config.ExtendedFileManager)
		{
		<?php
		
		// define backend configuration for the plugin
		$IMConfig = array();
		
		// the directories have to be writeable for php (that means 777 under linux)
		$IMConfig['max_foldersize_mb'] = 100;
		$IMConfig['files_dir'] = $_SERVER['DOCUMENT_ROOT'] . '/storage/files';
		$IMConfig['images_dir'] = $_SERVER['DOCUMENT_ROOT'] . '/storage/images';
		$IMConfig['files_url'] = '/storage/files/';
		$IMConfig['images_url'] = '/storage/images/';
		$IMConfig['images_enable_styling'] = false;
		$IMConfig['max_filesize_kb_image'] = 2000;
		
		// we can use the value 'max' to allow the maximium upload size defined in php_ini
		$IMConfig['max_filesize_kb_link'] = 'max';
		$IMConfig['allowed_link_extensions'] = array("doc","jpg","gif","js","pdf","zip","txt","psd","png","html","swf","xml","xls","ppt");
		$IMConfig['images_enable_title'] = true;
		$IMConfig['images_enable_alt'] = true;
		xinha_pass_to_php_backend($IMConfig);
				
		?>
		}
	}
	

	xinha_editors   = Xinha.makeEditors(xinha_editors, xinha_config, xinha_plugins);
	
	
	Xinha.startEditors(xinha_editors);
}

// Xinha._addEvent(window,'load', xinha_init);
document.observe("dom:loaded", function() {
	xinha_init();
});
</script>
<?php
	
	$output = ob_get_contents(); // Return the contents of the output buffer
	ob_end_clean(); // Erase the output buffer
	return $output;
}
?>