<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cropcanvas {

	var $original_img;
	var $final_img;
	var $type;
	var $error;
	
	function load($filepath)
	{
		// Check file exists
		if (!is_file($filepath))
		{
			$this->error = "File $filepath doesn't exist!";
			return false;
		}
		
		list($width, $height, $this->type, $attr) = @getimagesize($filepath);	
		
		switch($this->type)
		{
			case '2' : 
			
				$this->original_img = @imagecreatefromjpeg($filepath);				
				
			break;
			case '3' : 
			
				$this->original_img = @imagecreatefrompng($filepath); 
				// Default white bg
				$white = @imagecolorallocate($this->original_img, 255, 255, 255);
				@imagefill($this->original_img, 0, 0, $white);
				
			break;
			default :
				$this->error = 'Only jpg and png file formats supported';
				return false;
			break;
		}
		
		// Check image has been created
		if ($this->original_img == null)
		{
			$this->error = 'Image could not be loaded';
			return false;
		}
		
		return true;
	}
	
	function crop($sx,$sy,$ex,$ey)
	{
		// Check image has been created
		if ($this->original_img == null)
		{
			$this->error = 'Image could not be loaded';
			return false;
		}
	
		$nx = abs($ex - $sx); // crop width - left pos
		$ny = abs($ey - $sy); // crop height - top pos
	
	
		// Check crop size is valid
		if (($nx <= 0) || ($ny <= 0))
		{
			$this->error = 'The image could not be cropped because the size given is not valid.';
			return false;
		}

		// Create canvas
		$this->final_img = @imagecreatetruecolor($nx, $ny);

		// sets background to white
		$white = imagecolorallocate($this->final_img, 255, 255, 255);
		imagefill($this->final_img, 0, 0, $white);
		
		// Copy and resize part of an image with resampling
		@imagecopyresampled($this->final_img, $this->original_img, 0, 0, $sx, $sy, $nx, $ny, $nx, $ny);
		
		return true;
	}
		
	function cropCenter()
	{	
		// Get dimensions
		$width = @imagesx($this->original_img);
		$height = @imagesy($this->original_img);
			
	
		if ($width > $height)
		{
			// height		
			$sx = abs($width / 2) - abs($height / 2) ; // left - half width take half height
			$sy = 0; // top
			$ex = $height + $sx; // width
			$ey = $height; // height
			
		}
		else
		{
			//use width
			
		}
		
		$this->crop($sx,$sy,$ex,$ey);
	}
	
	function scaleTo($scale_width = 100, $scale_height = 100)
	{
		$this->scale($scale_width,$scale_height);
		$this->crop(0,0,$scale_width,$scale_height);
	}
	
	function scale($scale_width = 100, $scale_height = 100)
	{
		// Check image has been created
		if ($this->final_img == null)
		{
			$this->error = 'There is no image to scale.';
			return false;
		}
		
		// Reset
		$this->original_img = $this->final_img;
		
		// Get dimensions
		$width = @imagesx($this->original_img);
		$height = @imagesy($this->original_img);
			
		$width_ratio = ($scale_width / $width);
		$height_ratio = ($scale_height / $height);
		
				
		if ($width_ratio > $height_ratio)
		{
			$scale_height = round(($scale_width / $width) * $height);	
		}
		else
		{
			$scale_width = round(($scale_height / $height) * $width);
		}		
				
		// Create canvas
		$this->final_img = @imagecreatetruecolor($scale_width, $scale_height);
		
		// sets background to white
		$white = imagecolorallocate($this->final_img, 255, 255, 255);
		imagefill($this->final_img, 0, 0, $white);
		
		// Copy and resize part of an image with resampling
		@imagecopyresampled($this->final_img, $this->original_img, 0, 0, 0, 0, $scale_width, $scale_height, $width, $height);	
	
		$this->original_img = $this->final_img;
		
		//		
		return true;
	}
	
	function save($filepath, $quality = 100)
	{
		// Check image has been created
		if ($this->final_img == null)
		{
			$this->error = 'There is no processed image to save.';
			return false;
		}		
		
		// Saved status
		$saved = false;
		
		switch($this->type)
		{
			case '2' : 
				$saved = @imagejpeg($this->final_img, $filepath, $quality);
			break;
			case '3' : 
				$saved = @imagepng($this->final_img, $filepath); 
			break;
			default :
				$this->error = 'Only jpg and png file formats supported';
				return false;
			break;
		}

		// Did the image save?
		if (!$saved) 
		{
			$this->error = "Could not save the output file to $filepath.";
			return false;
		}
		
		return true;
	}
	
}
