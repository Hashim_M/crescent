<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Validation extends CI_Validation {

       function My_Validation()
       {
           parent::CI_Validation();
       }


       function set_default_values($data, $value = null)
       {
           if (is_array($data) == TRUE)
          {
               foreach($data as $field => $value)
               {
                  $this->$field   = $value;
                   $_POST[$field]  = $value;
               }
           }
           else
          {
              $this->$data    = $value;
               $_POST[$data]   = $value;
           }
       }
       
       
    // http://codeigniter.com/forums/viewthread/73012/   
    function set_checkbox($field = '', $value = '')
    {
        if ($field == '' OR $value == '' OR  ! isset($_POST[$field]))
        {
            return '';
        }

        if(is_array($_POST[$field])) {
            if(in_array($value,$_POST[$field])) {
                return ' checked="checked"';
            }
        } elseif ($_POST[$field] == $value) {
            return ' checked="checked"';
        }
    }
       
 
       
    // http://codeigniter.com/forums/viewthread/73012/
	function set_select($field = '', $value = '')
    {    
        if ($field == '' OR $value == '' OR  ! isset($_POST[$field]))
        {
            return '';
        }
        
        if(is_array($_POST[$field])) {
            if(in_array($value,$_POST[$field])) {
                return ' selected="selected"';
            }
        } elseif ($_POST[$field] == $value) {
            return ' selected="selected"';
        }
        
    } 
	
       
       
       
   }
?>
